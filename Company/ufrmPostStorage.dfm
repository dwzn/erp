object frmPostStorage: TfrmPostStorage
  Left = 0
  Top = 48
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = #26032#22686#25968#25454
  ClientHeight = 550
  ClientWidth = 828
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 11
    Top = 493
    Width = 803
    Height = 2
  end
  object Bevel2: TBevel
    Left = 0
    Top = 34
    Width = 828
    Height = 2
    Align = alTop
    ExplicitTop = 36
    ExplicitWidth = 809
  end
  object RzPanel1: TRzPanel
    Left = 0
    Top = 0
    Width = 828
    Height = 34
    Align = alTop
    BorderOuter = fsNone
    Caption = #20837#24211#21333
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object cxLabel2: TcxLabel
    Left = 20
    Top = 399
    Caption = #32467#31639#29366#24577#65306
  end
  object cxLabel4: TcxLabel
    Left = 287
    Top = 399
    Caption = #32463#25163#20154#65306
  end
  object cxLabel7: TcxLabel
    Left = 20
    Top = 70
    Caption = #25910#36135#21333#20301#65306
  end
  object cxLabel1: TcxLabel
    Left = 20
    Top = 44
    Caption = #21457#36135#21333#20301#65306
  end
  object cxLabel3: TcxLabel
    Left = 275
    Top = 44
    Caption = #21517#12288#12288#31216#65306
  end
  object RzPanel4: TRzPanel
    Left = 14
    Top = 100
    Width = 801
    Height = 292
    BorderInner = fsFlat
    BorderOuter = fsNone
    TabOrder = 6
    object RzToolbar13: TRzToolbar
      Left = 1
      Top = 1
      Width = 799
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdBottom]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer30
        RzToolButton3
        RzSpacer5
        RzToolButton6
        RzSpacer6
        RzToolButton90
        RzSpacer120
        RzToolButton85
        RzSpacer1
        RzToolButton1
        RzSpacer2)
      object RzSpacer30: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzToolButton85: TRzToolButton
        Left = 223
        Top = 2
        Width = 62
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 51
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton85Click
      end
      object RzToolButton90: TRzToolButton
        Left = 153
        Top = 2
        Width = 62
        SelectionColorStop = 16119543
        SelectionFrameColor = clBtnShadow
        ImageIndex = 3
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20445#23384
        Visible = False
        OnClick = RzToolButton90Click
      end
      object RzSpacer120: TRzSpacer
        Left = 215
        Top = 2
      end
      object RzToolButton3: TRzToolButton
        Left = 12
        Top = 2
        Width = 63
        ImageIndex = 37
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #26032#22686
        OnClick = RzToolButton3Click
      end
      object RzSpacer5: TRzSpacer
        Left = 75
        Top = 2
      end
      object RzToolButton6: TRzToolButton
        Left = 83
        Top = 2
        Width = 62
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
        OnClick = RzToolButton6Click
      end
      object RzSpacer6: TRzSpacer
        Left = 145
        Top = 2
        Visible = False
      end
      object RzSpacer1: TRzSpacer
        Left = 285
        Top = 2
      end
      object RzToolButton1: TRzToolButton
        Left = 293
        Top = 2
        Width = 75
        ImageIndex = 4
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #34920#26684#35774#32622
        OnClick = RzToolButton1Click
      end
      object RzSpacer2: TRzSpacer
        Left = 368
        Top = 2
      end
    end
    object Grid: TcxGrid
      Left = 1
      Top = 30
      Width = 799
      Height = 238
      Align = alClient
      TabOrder = 1
      object tView: TcxGridDBTableView
        PopupMenu = pm
        OnDblClick = tViewDblClick
        Navigator.Buttons.CustomButtons = <>
        OnEditing = tViewEditing
        OnEditKeyDown = tViewEditKeyDown
        DataController.DataSource = DataDetailed
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #165',0.00;'#165'-,0.00'
            Kind = skSum
            OnGetText = tViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
            Column = tViewCol10
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CellMultiSelect = True
        OptionsView.DataRowHeight = 23
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 23
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 14
        Styles.OnGetContentStyle = tViewStylesGetContentStyle
        OnColumnSizeChanged = tViewColumnSizeChanged
        object tViewCol1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.ReadOnly = True
          OnGetDisplayText = tViewCol1GetDisplayText
          Width = 40
        end
        object tViewCol2: TcxGridDBColumn
          Caption = #29366#24577
          DataBinding.FieldName = 'status'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Visible = False
          Width = 40
        end
        object tViewColumn11: TcxGridDBColumn
          Caption = #24320#31080#26085#26399
          DataBinding.FieldName = 'BillingDate'
          Visible = False
        end
        object tViewCol3: TcxGridDBColumn
          Caption = #32534#21495
          DataBinding.FieldName = 'BillNumber'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Visible = False
          Width = 160
        end
        object tViewColumn8: TcxGridDBColumn
          Caption = #21457#36135#21333#20301
          DataBinding.FieldName = 'DeliverCompany'
          Visible = False
          Width = 70
        end
        object tViewColumn9: TcxGridDBColumn
          Caption = #25910#36135#21333#20301
          DataBinding.FieldName = 'ReceiveCompany'
          Visible = False
          Width = 70
        end
        object tViewCol4: TcxGridDBColumn
          Caption = #21830#21697#21517#31216
          DataBinding.FieldName = 'GoodsName'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.DropDownListStyle = lsEditList
          Properties.DropDownWidth = 360
          Properties.ImmediateDropDownWhenActivated = True
          Properties.KeyFieldNames = 'MakingCaption'
          Properties.ListColumns = <
            item
              Caption = #21830#21697#21517#31216
              Width = 120
              FieldName = 'MakingCaption'
            end
            item
              Caption = #22411#21495
              Width = 100
              FieldName = 'ModelIndex'
            end
            item
              Caption = #35268#26684
              Width = 40
              FieldName = 'spec'
            end>
          Properties.ListSource = DataMakings
          Properties.OnCloseUp = tViewCol4PropertiesCloseUp
          Width = 153
        end
        object tViewCol5: TcxGridDBColumn
          Caption = #22411#21495
          DataBinding.FieldName = 'Model'
          Width = 60
        end
        object tViewCol6: TcxGridDBColumn
          Caption = #35268#26684
          DataBinding.FieldName = 'Spec'
          Width = 60
        end
        object tViewColumn12: TcxGridDBColumn
          Caption = #35745#31639#20844#24335
          DataBinding.FieldName = 'CalculatingFormula'
          Visible = False
          Width = 100
        end
        object tViewColumn13: TcxGridDBColumn
          Caption = #21464#37327#25968
          DataBinding.FieldName = 'VariableCount'
          PropertiesClassName = 'TcxSpinEditProperties'
          Properties.DisplayFormat = '0.#######;-0.#######'
          Properties.EditFormat = '0.#######;-0.#######'
          Properties.ValueType = vtFloat
          Visible = False
        end
        object tViewCol7: TcxGridDBColumn
          Caption = #25968#37327
          DataBinding.FieldName = 'CountTotal'
          PropertiesClassName = 'TcxPopupEditProperties'
          Properties.PopupControl = RzPanel3
          Properties.OnCloseUp = tViewCol7PropertiesCloseUp
          Properties.OnInitPopup = tViewCol7PropertiesInitPopup
          Properties.OnPopup = tViewCol7PropertiesPopup
          Width = 70
        end
        object tViewColumn10: TcxGridDBColumn
          Caption = #37325#37327
          DataBinding.FieldName = 'weight'
          PropertiesClassName = 'TcxSpinEditProperties'
          Properties.DisplayFormat = '0.#######;-0.#######'
          Properties.EditFormat = '0.#######;-0.#######'
          Properties.ValueType = vtFloat
          Properties.OnValidate = tViewColumn10PropertiesValidate
          Visible = False
        end
        object tViewCol17: TcxGridDBColumn
          Caption = #26465#30446#31867#22411
          DataBinding.FieldName = 'EntryType'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.ImmediateDropDownWhenActivated = True
          Properties.Items.Strings = (
            #22806#20511#20986#24211
            #31199#36161#20986#24211
            #35843#23384#20986#24211
            #28040#32791#20986#24211
            #29616#37329#20837#24211
            #35843#23384#20837#24211
            #24080#30446#20837#24211
            #31199#36161#20837#24211
            #36864#36824#22238#24211
            #20511#36824#22238#24211
            #31199#36161#22238#24211
            #35843#23384#22238#24211)
          Visible = False
          Width = 60
        end
        object tViewCol9: TcxGridDBColumn
          Caption = #21333#20301
          DataBinding.FieldName = 'Company'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.ImmediateDropDownWhenActivated = True
          OnGetPropertiesForEdit = tViewCol9GetPropertiesForEdit
          Width = 60
        end
        object tViewCol8: TcxGridDBColumn
          Caption = #21333#20215
          DataBinding.FieldName = 'UnitPrice'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.OnValidate = tViewCol8PropertiesValidate
          Width = 70
        end
        object tViewCol15: TcxGridDBColumn
          Caption = #25240#25187
          DataBinding.FieldName = 'DisCount'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = '0.00;0.00'
          Properties.OnValidate = tViewCol15PropertiesValidate
          Visible = False
          Width = 60
        end
        object tViewCol16: TcxGridDBColumn
          Caption = #25240#25187#21518#21333#20215
          DataBinding.FieldName = 'DisPrice'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.OnValidate = tViewCol16PropertiesValidate
          Visible = False
          Width = 75
        end
        object tViewCol10: TcxGridDBColumn
          Caption = #37329#39069
          DataBinding.FieldName = 'SumMonery'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.OnValidate = tViewCol10PropertiesValidate
          Width = 120
        end
        object tViewColumn14: TcxGridDBColumn
          Caption = #22823#20889
          DataBinding.FieldName = 'LargeMonery'
          Width = 117
        end
        object tViewColumn1: TcxGridDBColumn
          Caption = #36135#21495#20998#31867
          DataBinding.FieldName = 'NoType'
          Visible = False
          Width = 60
        end
        object tViewColumn2: TcxGridDBColumn
          Caption = #36135#21495#32534#21495
          DataBinding.FieldName = 'NoNumber'
          Visible = False
          Width = 60
        end
        object tViewColumn3: TcxGridDBColumn
          Caption = #21378#23478
          DataBinding.FieldName = 'Manufactor'
          Visible = False
          Width = 60
        end
        object tViewColumn4: TcxGridDBColumn
          Caption = #21697#29260
          DataBinding.FieldName = 'brand'
          Visible = False
          Width = 60
        end
        object tViewColumn5: TcxGridDBColumn
          Caption = #20998#39033#33539#22260
          DataBinding.FieldName = 'ItemRange'
          Visible = False
          Width = 60
        end
        object tViewColumn6: TcxGridDBColumn
          Caption = #39068#33394
          DataBinding.FieldName = 'NoColour'
          Visible = False
          Width = 60
        end
        object tViewColumn7: TcxGridDBColumn
          Caption = #26448#36136
          DataBinding.FieldName = 'Texture'
          Visible = False
          Width = 60
        end
        object tViewCol11: TcxGridDBColumn
          Caption = #22791#27880
          DataBinding.FieldName = 'Remarks'
          Width = 118
        end
        object tViewCol12: TcxGridDBColumn
          Caption = #22791#27880'1'
          DataBinding.FieldName = 'Remarks1'
          Visible = False
          Width = 126
        end
        object tViewCol13: TcxGridDBColumn
          Caption = #22791#27880'2'
          DataBinding.FieldName = 'Remarks2'
          Visible = False
          Width = 100
        end
        object tViewCol14: TcxGridDBColumn
          Caption = #22791#27880'3'
          DataBinding.FieldName = 'Remarks3'
          Visible = False
          Width = 92
        end
      end
      object Lv: TcxGridLevel
        GridView = tView
      end
    end
    object RzPanel5: TRzPanel
      Left = 1
      Top = 268
      Width = 799
      Height = 23
      Align = alBottom
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdLeft, sdRight, sdBottom]
      TabOrder = 2
      object RzPanel6: TRzPanel
        Left = 58
        Top = 0
        Width = 472
        Height = 22
        Align = alClient
        Alignment = taLeftJustify
        BorderOuter = fsNone
        BorderSides = [sdBottom]
        Caption = #12288#22823#20889':'
        Color = clWhite
        TabOrder = 0
        object cxDBTextEdit7: TcxDBTextEdit
          Left = -4
          Top = 3
          Properties.Alignment.Horz = taCenter
          Style.BorderStyle = ebsNone
          Style.Color = clWhite
          TabOrder = 0
          Width = 470
        end
      end
      object RzPanel7: TRzPanel
        Left = 1
        Top = 0
        Width = 57
        Height = 22
        Align = alLeft
        BorderInner = fsFlat
        BorderOuter = fsNone
        BorderSides = [sdRight]
        Caption = #21512#35745#65306
        Color = clWhite
        TabOrder = 1
      end
      object RzPanel8: TRzPanel
        Left = 530
        Top = 0
        Width = 268
        Height = 22
        Align = alRight
        Alignment = taLeftJustify
        BorderInner = fsFlat
        BorderOuter = fsNone
        BorderSides = [sdLeft]
        Caption = #12288#65509':'
        Color = clWhite
        TabOrder = 2
        object cxDBCurrencyEdit1: TcxDBCurrencyEdit
          Left = 0
          Top = 0
          Properties.Alignment.Horz = taCenter
          Style.BorderStyle = ebsNone
          Style.Color = clWhite
          TabOrder = 0
          Width = 265
        end
      end
    end
  end
  object cxLabel8: TcxLabel
    Left = 533
    Top = 70
    Caption = #24320#31080#26085#26399#65306
  end
  object cxLabel5: TcxLabel
    Left = 275
    Top = 70
    Caption = #31080#25454#31867#21035#65306
  end
  object cxLabel6: TcxLabel
    Left = 533
    Top = 399
    Caption = #23457#26680#31614#23383#65306
  end
  object cxLabel10: TcxLabel
    Left = 533
    Top = 428
    Caption = #31614#25910#26085#26399#65306
  end
  object cxButton2: TcxButton
    Left = 568
    Top = 506
    Width = 83
    Height = 28
    Caption = '&'#20445#23384
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = 'Office2013White'
    OptionsImage.ImageIndex = 17
    OptionsImage.Images = DM.cxImageList1
    TabOrder = 11
    OnClick = cxButton2Click
  end
  object cxButton3: TcxButton
    Left = 701
    Top = 506
    Width = 82
    Height = 28
    Caption = '&'#36864#20986
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = 'Office2013White'
    OptionsImage.Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000000000090000
      000E000000100000001000000010000000100000001000000011000000110000
      0011000000110000001100000011000000100000000B000000037B5A4FC1AA7C
      6EFFAA7C6EFFAA7C6DFFAA7B6DFFA97A6CFFA97A6BFFA97A6BFFA9796BFFA87A
      6BFFA7786AFFB77848FFB77746FFB77746FF845632C30000000AAC7E71FFF8F1
      EEFFF8F0EDFFF8F1ECFFF8F0ECFFF8F0ECFFF8F0ECFFF8F0ECFFF7EFEBFFF8EF
      EBFFF7EFEBFFD6AE84FFDFBC94FFDFBC93FFC58C5CFF0000000FAD8172FFF8F2
      EFFFF6EDE8FFF7EDE8FFF6ECE8FFF6EDE8FFF6ECE8FFF6EDE8FFF6ECE8FFF7ED
      E8FFF6EDE7FFD3A779FFDBB385FFE1C098FFC68D5DFF00000010AF8375FFF9F2
      EFFFF6ECE8FFF6EDE9FFF6ECE7FFF3EAE5FFF0E7E3FFF4EBE6FFF6EDE8FFF6EC
      E8FFF7EDE8FFD4AA7CFFDCB689FFE3C49EFFC78F5FFF0000000FB18577FFF9F3
      F0FFF7EDE9FFF6ECE7FFF2E9E4FFE4D7D1FFC2967AFFEFE6E2FFF7EDE8FFF7ED
      E9FFF6EDE9FFD6AC7FFFDEB98EFFE5C8A4FFC99061FF0000000EB2887AFFF9F4
      F0FFF5ECE7FFEFE6E2FFD0BAAEFFA7704FFFB47950FFE3D9D6FFEAE1DDFFEDE4
      E0FFF3EAE6FFD7AF82FFDFBD92FFE7CCA8FFCA9363FF0000000EB48A7CFFF7F2
      EFFFEBE1DCFFB9937FFFB27A4EFFF1C47FFFB67D54FFAC7049FFAC6F48FFB77F
      57FFEDE4E0FFD8B185FFE1C196FFE9CFADFFCA9464FF0000000DB68D7FFFF7F1
      EFFFC19980FFD3A97EFFF5D8A8FFFCD38DFFFCD18DFFFCD38DFFF5D8A8FFBB84
      5DFFECE3DFFFDAB488FFE3C399FFEBD3B1FFCC9666FF0000000CB88F82FFF9F4
      F1FFEEE4E1FFC8A691FFC28E62FFEDCC9EFFBF8C64FFBF8B63FFBF8A63FFBE89
      61FFEFE7E3FFDAB68BFFE4C79DFFECD6B7FFCD9968FF0000000CB99184FFFAF5
      F3FFF7EEEAFFF3EBE6FFDDCBBEFFBC8B69FFC28F68FFEBE2DFFFEFE8E4FFF1E7
      E5FFF5EDE8FFDCB88DFFE6C9A1FFEED9BBFFCE9B6CFF0000000BBB9387FFFAF6
      F4FFF7EFEBFFF7EEEBFFF6EDE9FFEDE1DDFFD2B195FFF3EBE7FFF7EFEBFFF7EE
      EBFFF7EFEBFFDDBB90FFE8CCA3FFEFDCBFFFCF9C6EFF0000000ABD968AFFFAF6
      F4FFF8EFECFFF7EFECFFF7EFECFFF7EEEBFFF6EDEAFFF6EEEBFFF7EFECFFF7EF
      ECFFF7EFEBFFDEBC92FFE9CFA6FFF1DFC2FFD09E70FF00000009BE998BFFFAF7
      F4FFF7F0ECFFF8F0ECFFF8F0EDFFF8EFECFFF8EFECFFF8EFECFFF8F0ECFFF7F0
      ECFFF8EFECFFDFBE94FFEAD1A9FFF1E1C6FFD0A071FF00000009C09B8EFFFAF8
      F6FFFBF8F6FFFBF8F6FFFAF8F5FFFAF8F6FFFAF8F5FFFAF8F6FFFAF8F5FFFAF8
      F5FFFAF8F5FFE7D0B2FFF5E8D1FFF4E5CDFFD1A273FF000000088F756CC0C19D
      90FFC19C8FFFC09B8EFFC09B8EFFC09A8EFFBF9A8DFFBF998CFFBE988CFFBE98
      8BFFBD978AFFD3A375FFD3A375FFD2A375FF9C7956C200000005}
    OptionsImage.ImageIndex = 8
    OptionsImage.Images = DM.cxImageList1
    TabOrder = 12
    OnClick = cxButton3Click
  end
  object cxLabel9: TcxLabel
    Left = 533
    Top = 44
    Caption = #21333#25454#32534#21495#65306
  end
  object cxLabel11: TcxLabel
    Left = 32
    Top = 428
    Caption = #31614#25910#20154#65306
  end
  object cxLabel12: TcxLabel
    Left = 287
    Top = 428
    Caption = #21457#36135#20154#65306
  end
  object cxLabel16: TcxLabel
    Left = 20
    Top = 457
    Caption = #31080#25454#29366#24577#65306
  end
  object cxCheckBox1: TcxCheckBox
    Left = 158
    Top = 509
    Caption = #36830#32493#24320#21333
    TabOrder = 17
  end
  object cxButton6: TcxButton
    Left = 308
    Top = 506
    Width = 85
    Height = 28
    Caption = '&'#38468#20214
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = 'Office2013White'
    OptionsImage.ImageIndex = 55
    OptionsImage.Images = DM.cxImageList1
    TabOrder = 18
    Visible = False
    OnClick = cxButton6Click
  end
  object cxButton7: TcxButton
    Left = 443
    Top = 506
    Width = 75
    Height = 28
    Caption = #25171#21360
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = 'Office2013White'
    OptionsImage.ImageIndex = 6
    OptionsImage.Images = DM.cxImageList1
    TabOrder = 19
    OnClick = cxButton7Click
  end
  object cxDBLookupComboBox1: TcxDBLookupComboBox
    Left = 78
    Top = 42
    DataBinding.DataField = 'DeliverCompany'
    Properties.ImmediateDropDownWhenActivated = True
    Properties.KeyFieldNames = 'SignName'
    Properties.ListColumns = <
      item
        Caption = #24448#26469#21333#20301
        Width = 80
        FieldName = 'SignName'
      end
      item
        Caption = #31616#30721
        Width = 60
        FieldName = 'PYCode'
      end>
    Properties.ListSource = DM.DataSignName
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsSimple
    Style.ButtonTransparency = ebtNone
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 20
    OnEnter = cxDBLookupComboBox1Enter
    Width = 178
  end
  object cxDBLookupComboBox3: TcxDBLookupComboBox
    Left = 78
    Top = 69
    DataBinding.DataField = 'ReceiveCompany'
    Properties.DropDownListStyle = lsEditList
    Properties.ImmediateDropDownWhenActivated = True
    Properties.KeyFieldNames = 'SignName'
    Properties.ListColumns = <
      item
        Caption = #24448#26469#21333#20301
        Width = 80
        FieldName = 'SignName'
      end
      item
        Caption = #31616#30721
        Width = 60
        FieldName = 'PYCode'
      end>
    Properties.ListSource = DM.DataSignName
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.Kind = lfFlat
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsSimple
    Style.ButtonTransparency = ebtNone
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 21
    OnEnter = cxDBLookupComboBox1Enter
    Width = 178
  end
  object cxDBTextEdit1: TcxDBTextEdit
    Left = 335
    Top = 42
    DataBinding.DataField = 'CompanyName'
    Properties.ReadOnly = True
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 22
    Width = 178
  end
  object cxDBComboBox1: TcxDBComboBox
    Left = 335
    Top = 69
    DataBinding.DataField = 'BillCategory'
    Properties.Items.Strings = (
      #36134#30446#37319#36141
      #36827#24211#31080#25454
      #20986#24211#31080#25454
      #36864#36824#31080#25454
      #31199#36161#20837#24211
      #31199#36161#20986#24211)
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsSimple
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 23
    OnEnter = cxDBComboBox2Enter
    Width = 178
  end
  object BillNumber: TcxDBTextEdit
    Left = 591
    Top = 42
    DataBinding.DataField = 'BillNumber'
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 24
    Width = 178
  end
  object cxDBDateEdit1: TcxDBDateEdit
    Left = 591
    Top = 69
    DataBinding.DataField = 'BillingDate'
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsSimple
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 25
    OnEnter = cxDBDateEdit2Enter
    Width = 178
  end
  object cxDBComboBox2: TcxDBComboBox
    Left = 78
    Top = 398
    DataBinding.DataField = 'AccountsStatus'
    Properties.ImmediateDropDownWhenActivated = True
    Properties.Items.Strings = (
      #25346#36134
      #31199#36161
      #24050#25910#27454
      #24050#20184#27454)
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsSimple
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 26
    OnEnter = cxDBComboBox2Enter
    Width = 178
  end
  object cxDBTextEdit3: TcxDBTextEdit
    Left = 78
    Top = 426
    DataBinding.DataField = 'Recipient'
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 27
    Width = 178
  end
  object cxDBComboBox3: TcxDBComboBox
    Left = 78
    Top = 456
    DataBinding.DataField = 'BillStatus'
    Properties.ImmediateDropDownWhenActivated = True
    Properties.Items.Strings = (
      #24050#24320#21457#31080
      #26410#24320#21457#31080)
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsSimple
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 28
    OnEnter = cxDBComboBox2Enter
    Width = 178
  end
  object cxDBTextEdit5: TcxDBTextEdit
    Left = 335
    Top = 398
    DataBinding.DataField = 'Handman'
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 29
    Width = 178
  end
  object cxDBTextEdit4: TcxDBTextEdit
    Left = 335
    Top = 426
    DataBinding.DataField = 'Consignor'
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 30
    Width = 178
  end
  object cxDBTextEdit6: TcxDBTextEdit
    Left = 591
    Top = 398
    DataBinding.DataField = 'AuditSignature'
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 31
    Width = 178
  end
  object cxDBDateEdit2: TcxDBDateEdit
    Left = 591
    Top = 426
    DataBinding.DataField = 'DateSigning'
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsSimple
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 32
    OnEnter = cxDBDateEdit2Enter
    Width = 178
  end
  object cxLabel18: TcxLabel
    Left = 275
    Top = 457
    Caption = #20132#26131#26041#24335#65306
  end
  object cxLabel15: TcxLabel
    Left = 533
    Top = 457
    Caption = #36153#29992#31185#30446#65306
  end
  object cxDBLookupComboBox4: TcxDBLookupComboBox
    Left = 335
    Top = 456
    RepositoryItem = DM.MarketTypeBox
    DataBinding.DataField = 'Exacct'
    Properties.ImmediateDropDownWhenActivated = True
    Properties.ListColumns = <>
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsSimple
    Style.ButtonTransparency = ebtNone
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 35
    OnEnter = cxDBLookupComboBox1Enter
    Width = 178
  end
  object cxDBLookupComboBox5: TcxDBLookupComboBox
    Left = 591
    Top = 456
    RepositoryItem = DM.CostComboBox
    DataBinding.DataField = 'Exacct'
    Properties.ImmediateDropDownWhenActivated = True
    Properties.ListColumns = <>
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Edges = [bBottom]
    Style.HotTrack = False
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsSimple
    Style.ButtonTransparency = ebtNone
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 36
    OnEnter = cxDBLookupComboBox1Enter
    Width = 178
  end
  object RzPanel3: TRzPanel
    Left = 383
    Top = 167
    Width = 342
    Height = 320
    BorderShadow = clBlack
    TabOrder = 37
    Visible = False
    object Bevel3: TBevel
      Left = 10
      Top = 40
      Width = 319
      Height = 2
    end
    object cxGroupBox4: TcxGroupBox
      Left = 10
      Top = 48
      Caption = #38050#31563#35745#31639#22120
      ParentBackground = False
      TabOrder = 1
      Height = 81
      Width = 319
      object Label23: TLabel
        Left = 163
        Top = 25
        Width = 52
        Height = 13
        Caption = #20214#12288#12288#25968':'
      end
      object Label24: TLabel
        Left = 24
        Top = 51
        Width = 47
        Height = 13
        Caption = ' '#21333#26681'/'#31859':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label26: TLabel
        Left = 171
        Top = 51
        Width = 44
        Height = 13
        Caption = #21333#20214'/'#26681':'
      end
      object Label3: TLabel
        Left = 19
        Top = 25
        Width = 52
        Height = 13
        Caption = #38050#31563#35268#26684':'
      end
      object cxSpinEdit1: TcxSpinEdit
        Left = 221
        Top = 22
        Properties.EditFormat = '0.##;-0.##'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxSpinEdit1PropertiesChange
        TabOrder = 1
        Width = 80
      end
      object cxSpinEdit2: TcxSpinEdit
        Left = 77
        Top = 49
        Properties.DisplayFormat = '0.##;-0.##'
        Properties.EditFormat = '0.##;-0.##'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxSpinEdit1PropertiesChange
        TabOrder = 2
        Value = 9.000000000000000000
        Width = 76
      end
      object cxSpinEdit5: TcxSpinEdit
        Left = 221
        Top = 49
        Properties.DisplayFormat = '0.##;-0.##'
        Properties.EditFormat = '0.##;-0.##'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxSpinEdit1PropertiesChange
        TabOrder = 3
        Width = 80
      end
      object cxComboBox1: TcxComboBox
        Left = 77
        Top = 22
        Properties.OnCloseUp = cxComboBox1PropertiesCloseUp
        Style.BorderStyle = ebs3D
        TabOrder = 0
        Width = 72
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 10
      Top = 135
      Caption = #25968#37327#35745#31639#22120
      TabOrder = 2
      Height = 122
      Width = 319
      object Label4: TLabel
        Left = 18
        Top = 30
        Width = 52
        Height = 13
        Caption = #35745#31639#20844#24335':'
      end
      object Label2: TLabel
        Left = 30
        Top = 82
        Width = 40
        Height = 13
        Caption = #21464#37327#25968':'
      end
      object Label6: TLabel
        Left = 170
        Top = 82
        Width = 52
        Height = 13
        Caption = #35745#31639#25968#20540':'
      end
      object cxDBMemo2: TcxDBMemo
        Left = 76
        Top = 30
        DataBinding.DataField = 'CalculatingFormula'
        DataBinding.DataSource = DataDetailed
        Properties.OnChange = cxDBMemo2PropertiesChange
        TabOrder = 2
        Height = 43
        Width = 229
      end
      object cxDBSpinEdit2: TcxDBSpinEdit
        Left = 76
        Top = 79
        DataBinding.DataField = 'VariableCount'
        DataBinding.DataSource = DataDetailed
        Properties.DisplayFormat = '0.#######;-0.#######'
        Properties.EditFormat = '0.#######;-0.#######'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxDBSpinEdit2PropertiesChange
        TabOrder = 0
        Width = 80
      end
      object cxDBSpinEdit3: TcxDBSpinEdit
        Left = 228
        Top = 79
        DataBinding.DataField = 'NumericalValue'
        DataBinding.DataSource = DataDetailed
        Properties.DisplayFormat = '0.#######;-0.#######'
        Properties.EditFormat = '0.#######;-0.#######'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxDBSpinEdit3PropertiesChange
        TabOrder = 1
        Width = 77
      end
    end
    object cxLabel13: TcxLabel
      Left = 12
      Top = 13
      Caption = #25968#37327':'
    end
    object cxDBSpinEdit1: TcxDBSpinEdit
      Left = 50
      Top = 13
      DataBinding.DataField = 'CountTotal'
      DataBinding.DataSource = DataDetailed
      Properties.DisplayFormat = '0.#######;-0.#######'
      Properties.EditFormat = '0.#######;-0.#######'
      Properties.ValueType = vtFloat
      Properties.OnValidate = cxDBSpinEdit1PropertiesValidate
      TabOrder = 0
      Width = 103
    end
    object cxLabel14: TcxLabel
      Left = 159
      Top = 13
      Caption = #23567#25968#20445#30041#20301#25968':'
    end
    object cxComboBox2: TcxComboBox
      Left = 245
      Top = 13
      Properties.Items.Strings = (
        '1'#20301
        '2'#20301
        '3'#20301
        '4'#20301
        '5'#20301
        '6'#20301
        '7'#20301)
      Properties.OnCloseUp = cxComboBox2PropertiesCloseUp
      Style.BorderStyle = ebs3D
      TabOrder = 5
      Text = '3'#20301
      Width = 66
    end
    object cxButton4: TcxButton
      Left = 77
      Top = 270
      Width = 67
      Height = 25
      Caption = #30830#23450
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 17
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 6
      OnClick = cxButton4Click
    end
    object cxButton5: TcxButton
      Left = 222
      Top = 270
      Width = 67
      Height = 25
      Caption = #21462#28040
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 51
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 7
      OnClick = cxButton5Click
    end
  end
  object DataDetailed: TDataSource
    DataSet = Detailed
    Left = 288
    Top = 247
  end
  object Detailed: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = DetailedAfterInsert
    Left = 288
    Top = 200
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 208
    Top = 200
  end
  object ClientMakings: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 80
    Top = 200
  end
  object DataMakings: TDataSource
    DataSet = ClientMakings
    Left = 80
    Top = 264
  end
  object pm: TPopupMenu
    Images = DM.cxImageList1
    Left = 152
    Top = 200
    object N1: TMenuItem
      Caption = #26032#22686
      ImageIndex = 37
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #32534#36753
      ImageIndex = 0
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #21024#38500
      ImageIndex = 51
      OnClick = N3Click
    end
  end
  object Report: TPopupMenu
    Left = 152
    Top = 264
    object N4: TMenuItem
      Caption = #25171#21360#39044#35272
    end
    object N5: TMenuItem
      Caption = #25253#34920#35774#35745
    end
    object N6: TMenuItem
      Caption = #30452#25509#25171#21360
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 288
    Top = 304
  end
  object ADOContract: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 80
    Top = 152
  end
  object ContractSource: TDataSource
    DataSet = ADOContract
    Left = 152
    Top = 152
  end
end
