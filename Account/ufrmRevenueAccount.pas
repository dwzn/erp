unit ufrmRevenueAccount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RzButton, RzPanel, Vcl.ExtCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Datasnap.DBClient, cxTextEdit, cxCalendar, cxDBLookupComboBox, cxDropDownEdit,ufrmBaseController,
  Data.Win.ADODB, cxCheckBox, cxSpinEdit, cxCurrencyEdit, cxMemo, cxContainer,
  Vcl.ComCtrls, cxSplitter, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxLabel,
  dxLayoutContainer, cxGridViewLayoutContainer, cxGridLayoutView,UtilsTree,FillThrdTree,
  cxGridDBLayoutView, cxGridCustomLayoutView, Vcl.Menus, frxClass, frxDBSet,
  Vcl.StdCtrls , Generics.Collections, RzTabs, VclTee.TeeGDIPlus,
  VCLTee.TeEngine, VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, VCLTee.DBChart,
  dxmdaset, dxCore, cxDateUtils;

type
  TSum = record
    dwSumMoney : Currency;
    dwPrize    : Currency;
    dwbuckle   : Currency;
    dwPaidMoney: Currency;
    dwDetBranchMoney : Currency;
    dwDetCollectMoney: Currency;
  end;
type
  TSumType = record
     dwPrjectName:string;
     dwIndex : Integer;
     dwSumMoney : Currency;
  end;
type
  TfrmRevenueAccount = class(TfrmBaseController)
    Top: TRzPanel;
    Client: TRzPanel;
    Left: TRzPanel;
    RzPanel5: TRzPanel;
    SignNameGrid: TcxGrid;
    tvSignName: TcxGridDBTableView;
    tvSignNameColumn1: TcxGridDBColumn;
    tvSignNameColumn2: TcxGridDBColumn;
    SignNameLv: TcxGridLevel;
    RzToolbar3: TRzToolbar;
    RzSpacer7: TRzSpacer;
    RzSpacer13: TRzSpacer;
    RzToolButton4: TRzToolButton;
    cxLabel3: TcxLabel;
    Search: TcxLookupComboBox;
    cxSplitter1: TcxSplitter;
    Tv: TTreeView;
    Splitter2: TSplitter;
    RzPanel1: TRzPanel;
    DetGrid: TcxGrid;
    tvDet: TcxGridDBTableView;
    DetCol1: TcxGridDBColumn;
    DetCol2: TcxGridDBColumn;
    DetCol3: TcxGridDBColumn;
    DetCol4: TcxGridDBColumn;
    LayoutView: TcxGridDBLayoutView;
    LayoutView1: TcxGridDBLayoutViewItem;
    LayoutView2: TcxGridDBLayoutViewItem;
    LayoutView3: TcxGridDBLayoutViewItem;
    LayoutView4: TcxGridDBLayoutViewItem;
    LayoutViewGroup_Root: TdxLayoutGroup;
    cxGridLayoutItem1: TcxGridLayoutItem;
    LayoutViewLayoutItem2: TcxGridLayoutItem;
    LayoutViewLayoutItem3: TcxGridLayoutItem;
    LayoutViewLayoutItem4: TcxGridLayoutItem;
    DetLv: TcxGridLevel;
    RzPanel2: TRzPanel;
    Splitter3: TSplitter;
    RzPanel6: TRzPanel;
    RzPanel7: TRzPanel;
    RzPanel8: TRzPanel;
    RzPanel9: TRzPanel;
    RzPanel10: TRzPanel;
    RzPanel11: TRzPanel;
    RzPanel12: TRzPanel;
    Splitter4: TSplitter;
    ADOSumInfo: TADODataSet;
    ADOSumInfoSignName: TStringField;
    ADOSumInfoInduceType: TStringField;
    ADOSumInfoClearBalance: TCurrencyField;
    ADOSumInfoClearBalanceCapital: TStringField;
    ADOSumInfoInputDate: TDateTimeField;
    ADOSumInfoPaidMoney: TCurrencyField;
    ADOSumInfoDeductMoney: TCurrencyField;
    ADOSumInfoRewardMoney: TCurrencyField;
    ADOSumInfoSumMoney: TCurrencyField;
    ADOSumInfoSign_Id: TIntegerField;
    DataSumInfo: TDataSource;
    frxDBDataset: TfrxDBDataset;
    ADOSign: TADOQuery;
    DataSign: TDataSource;
    ADOCompany: TADOQuery;
    DataCompany: TDataSource;
    ADOSumLogInfo: TADOQuery;
    DataSumLogInfo: TDataSource;
    ADODet: TADODataSet;
    ADODetSignName: TStringField;
    ADODetSign_Id: TIntegerField;
    ADODetdetDate: TDateField;
    ADODetdetSumMoney: TCurrencyField;
    ADODetdetType: TStringField;
    ADODetModuleIndex: TIntegerField;
    DataDet: TDataSource;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzPanel13: TRzPanel;
    RzPanel14: TRzPanel;
    RzPanel15: TRzPanel;
    RzPanel16: TRzPanel;
    RzPanel17: TRzPanel;
    RzPanel18: TRzPanel;
    RzPanel19: TRzPanel;
    RzPanel20: TRzPanel;
    RzPanel21: TRzPanel;
    RzPageControl2: TRzPageControl;
    Tab1: TRzTabSheet;
    Tab2: TRzTabSheet;
    TabSheet1: TRzTabSheet;
    TabSheet2: TRzTabSheet;
    RzToolbar1: TRzToolbar;
    RzSpacer4: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzToolButton6: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    Grid: TcxGrid;
    tvGrid: TcxGridDBTableView;
    tvGridColumn1: TcxGridDBColumn;
    tvGridColumn2: TcxGridDBColumn;
    tvGridColumn3: TcxGridDBColumn;
    tvGridColumn4: TcxGridDBColumn;
    tvGridColumn5: TcxGridDBColumn;
    tvGridColumn6: TcxGridDBColumn;
    tvGridColumn9: TcxGridDBColumn;
    tvGridColumn10: TcxGridDBColumn;
    tvGridColumn11: TcxGridDBColumn;
    tvGridColumn12: TcxGridDBColumn;
    tvGridColumn13: TcxGridDBColumn;
    tvGridColumn14: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzToolbar2: TRzToolbar;
    RzSpacer11: TRzSpacer;
    RzToolButton14: TRzToolButton;
    RzSpacer16: TRzSpacer;
    RzSpacer5: TRzSpacer;
    cxLabel2: TcxLabel;
    LogInfoGrid: TcxGrid;
    tvLogInfoView: TcxGridDBTableView;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridDBColumn16: TcxGridDBColumn;
    cxGridDBColumn17: TcxGridDBColumn;
    cxGridDBColumn18: TcxGridDBColumn;
    cxGridDBColumn19: TcxGridDBColumn;
    cxGridDBColumn20: TcxGridDBColumn;
    cxGridDBColumn21: TcxGridDBColumn;
    cxGridDBColumn22: TcxGridDBColumn;
    LogInfoLv: TcxGridLevel;
    DBChart1: TDBChart;
    Series2: TPieSeries;
    pmRigth: TPopupMenu;
    N5: TMenuItem;
    N7: TMenuItem;
    N6: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    ChartData: TdxMemData;
    ChartProjectName: TStringField;
    ChartSumMoney: TCurrencyField;
    PaidData: TdxMemData;
    PaidDataPrjectName: TStringField;
    PaidDataPaidMoney: TCurrencyField;
    SUMData: TdxMemData;
    SUMDataPrjectName: TStringField;
    SUMDataSumMoney: TCurrencyField;
    RzToolbar4: TRzToolbar;
    RzSpacer17: TRzSpacer;
    RzSpacer18: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    cxLabel5: TcxLabel;
    cxComboBox1: TcxComboBox;
    pmSumReport: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    pmDetReport: TPopupMenu;
    N11: TMenuItem;
    Exel1: TMenuItem;
    pmDet: TPopupMenu;
    N3: TMenuItem;
    N4: TMenuItem;
    DetCol5: TcxGridDBColumn;
    tvGridColumn7: TcxGridDBColumn;
    ADOSumInforatio: TFloatField;
    RzSpacer8: TRzSpacer;
    RzToolButton7: TRzToolButton;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLabel6: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel8: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    RzToolButton10: TRzToolButton;
    pmLog: TPopupMenu;
    N2: TMenuItem;
    RzSpacer9: TRzSpacer;
    RzToolButton11: TRzToolButton;
    pmLogReport: TPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    RzSpacer12: TRzSpacer;
    RzSpacer14: TRzSpacer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure TvDblClick(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure SearchPropertiesCloseUp(Sender: TObject);
    procedure tvGridColumn5CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvGridColumn13CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvSignNameDblClick(Sender: TObject);
    procedure RzPanel6Resize(Sender: TObject);
    procedure tvGridColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton9Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure ADOSumInfoAfterInsert(DataSet: TDataSet);
    procedure tvDetStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure RzToolButton2Click(Sender: TObject);
    procedure tvGridColumn3CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure N5Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure Exel1Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure RzToolButton10Click(Sender: TObject);
    procedure RzToolButton7Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure RzPanel13Resize(Sender: TObject);
  private
    { Private declarations }
    dwSumMoney : Currency;
    g_TreeView : TNewUtilsTree;
    dwIsSumDetailed : Boolean;
    IsDateQuery : Boolean;
   // g_TableName: string;
    function GetSUMMoney(lpIndex : Byte ;lpSignName : string):Boolean;
    function GetDetInfo(lpIndex : Byte ;lpSignName : string):Currency;
    function SearchLog(lpIndex : Byte;lpSignName : string):Boolean;
  public
    { Public declarations }
    dwSum : TObjectDictionary<string,TSum>;
    dwProjectSum : TObjectDictionary<string,Currency>;
    ProjectName : array[0..7] of string;
  end;

var
  frmRevenueAccount: TfrmRevenueAccount;
  g_TableName : string = 'sk_ReconciliationTable';
  g_ReconciliationTable  : string = 'ReconciliationTable';

implementation

uses
   uDataModule,global,ufrmSpareMoney,ufrmSumDetailed,System.Math;

{$R *.dfm}

procedure TfrmRevenueAccount.ADOSumInfoAfterInsert(DataSet: TDataSet);
begin
  inherited;
  Self.tvGridColumn4.EditValue := '营收';
end;

procedure TfrmRevenueAccount.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := (Sender as TcxComboBox).Text;
  GetDetInfo(3,s);
end;

procedure TfrmRevenueAccount.cxLookupComboBox1PropertiesCloseUp(
  Sender: TObject);
var
  s : string;

begin
  inherited;
  s := (Sender as TcxLookupComboBox).Text;
  SearchLog(0,s);
end;

procedure TfrmRevenueAccount.Exel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'债务明细表');
end;

procedure TfrmRevenueAccount.FormActivate(Sender: TObject);
begin
  inherited;
  dwSum := TObjectDictionary<string,TSum>.Create();
  dwProjectSum := TObjectDictionary<string,Currency>.Create();
  g_TreeView := TNewUtilsTree.Create(Self.tv,DM.ADOconn,g_Table_CompanyTree,'往来单位');
  g_TreeView.GetTreeTable(0);

  with Self.ADOSign  do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select *from ' + g_Table_CompanyManage;
    Open;
  end;

  ProjectName[0] := '项目营收-合同总价';
  ProjectName[1] := '项目营收-交易记录';
  ProjectName[2] := '交易流水帐';
  ProjectName[3] := '公司仓库-营收挂账';
  ProjectName[4] := '债务-收款';
  ProjectName[5] := '债务-支款';
  ProjectName[6] := '营收对账-债收款';
  ProjectName[7] := '营收对账-债支款';
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  RzPageControl2.ActivePage := Self.Tab2;
end;

function TfrmRevenueAccount.SearchLog(lpIndex : Byte; lpSignName : string):Boolean;
var
  SqlText , s  : string;
begin
  case lpIndex of
    0: s := ' AND A.SignName Like"%' + lpSignName + '%"';
    2:
    begin
      if Length(lpSignName) <> 0 then
        s := ' AND A.SignName Like"%' + lpSignName + '%" and InputDate between :t1 and :t2'
      else
        s := ' AND InputDate between :t1 and :t2';
    end;

  end;


  SqlText :=  'select A.Id,'+
                     'A.InputDate,'  +
                     'A.Sign_Id,'    +
                     'A.InduceType,' +
                     'A.SumMoney,'   +
                     'A.DeductMoney,'+
                     'A.RewardMoney,'+
                     'A.SignName,'   +
                     'A.ModuleType'  +
                     //'B.SignName'    +
              ' from '+ g_TableName + ' AS A'+
              //' right join ' + g_Table_CompanyManage +' AS B on A.Sign_Id=B.ID'+
              ' WHERE ModuleType=1' + s;

  Self.Client.Caption := lpSignName;

  with Self.ADOSumLogInfo do
  begin
    Close;
    SQL.Clear;
    SQL.Text := SqlText;
    if lpIndex = 2 then
    begin
      Parameters.ParamByName('t1').Value := Self.cxDateEdit1.Text;  // StrToDate('2016-4-17 00:00:00');
      Parameters.ParamByName('t2').Value := Self.cxDateEdit2.Text;
    end;
    Open;

  end;

end;

function TfrmRevenueAccount.GetSUMMoney(lpIndex : Byte ;lpSignName : string):Boolean;

  function GetGroupSQL(lpSelect , lpTableName , lpWhereName , lpGroupName : string):string;
  begin
    Result := 'SELECT ' + lpSelect + ' FROM ' + lpTableName + ' WHERE ' + lpWhereName + '  GROUP BY  ' + lpGroupName;
  end;

  function GetSumMoney(lpIndex : Byte ;SQLTEXT : string; lpPrjectName : string) : Currency;
  var
     szName: string;
     szSum : TSum;
     szIsExist : Boolean;
     t : Currency;
  begin
     Result := 0;
     if Length(SQLTEXT) <> 0 then
     begin
        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := SQLTEXT;
          Open;

          if RecordCount <> 0 then
          begin
            while not Eof do
            begin
              szName := FieldByName('c').AsString;
              t := FieldByName('t').AsCurrency;
              if dwIsSumDetailed then
              begin
                with dwProjectSum do
                begin
                  if ContainsKey(lpPrjectName) then
                  begin
                    Items[lpPrjectName] := t;
                  end;

                end;
              end else
              begin
                with dwSum do
                begin
                  ZeroMemory(@szSum,SizeOf(Tsum));
                  if ContainsKey(szName) then
                  begin
                    szIsExist := True;
                    szSum := Items[szName];
                    case lpIndex of
                      0: szSum.dwSumMoney := szSum.dwSumMoney  + t;  //帐目总价
                      1: szSum.dwPaidMoney:= szSum.dwPaidMoney + t;  //支款统计
                      2: szSum.dwDetBranchMoney := szSum.dwDetBranchMoney + t; //债务支款
                      3: szSum.dwDetCollectMoney:= szSum.dwDetCollectMoney+ t; //债务收款
                    end;
                    Items[szName] := szSum;
                  end else
                  begin
                    case lpIndex of
                      0: szSum.dwSumMoney := t;  //帐目总价
                      1: szSum.dwPaidMoney:= t;  //支款统计
                      2: szSum.dwDetBranchMoney := t; //债务支款
                      3: szSum.dwDetCollectMoney:= t; //债务收款
                    end;

                    Add(szName,szSum);

                  end;

                end;
              //  ShowMessage( IntToStr(lpIndex) + ' - ' + lpPrjectName  + ' - ' + szName + ' t:' + CurrToStr(t));
              end;
              Next;
            end;

          end;  
        end; 
     end;  

  end;

  function UpdateSumMoney(lpIndex : Byte; lpSignName : string;
                          lpSumMoney,
                          lpPaidMoney,
                          lpClearMoney: Currency;
                          lpRatio : Double
                          ):Boolean;
  var
    a , b , c , d : Currency;

  begin

    with Self.ADOSumInfo do
    begin

      if Self.ADOSumInfo.Locate(Self.tvGridColumn3.DataBinding.FieldName,lpSignName,[loCaseInsensitive, loPartialKey]) then
        Edit
      else
        Append;
        {
          FieldByName(Self.ADOSumInfoSignName.FieldName).Value     := lpSignName;
          FieldByName(Self.ADOSumInfoSumMoney.FieldName).Value     := lpSumMoney;
          FieldByName(Self.ADOSumInfoPaidMoney.FieldName).Value    := lpPaidMoney;
          FieldByName(Self.ADOSumInfoClearBalance.FieldName).Value := lpClearMoney; //清算余额
          FieldByName(Self.ADOSumInfoClearBalanceCapital.FieldName).Value:= MoneyConvert( lpClearMoney );
          FieldByName(Self.ADOSumInfoInputDate.FieldName).Value    := Date; //对帐日期
          FieldByName(Self.ADOSumInforatio.FieldName).Value        := lpRatio;
        Post;
        }
      FieldByName(Self.ADOSumInfoSignName.FieldName).Value     := lpSignName;
      FieldByName(Self.ADOSumInfoSumMoney.FieldName).Value     := lpSumMoney;
      FieldByName(Self.ADOSumInfoPaidMoney.FieldName).Value    := lpPaidMoney;
      FieldByName(Self.ADOSumInfoClearBalance.FieldName).Value := lpClearMoney; //清算余额
      FieldByName(Self.ADOSumInfoClearBalanceCapital.FieldName).Value:= MoneyConvert( lpClearMoney );
      FieldByName(Self.ADOSumInfoInputDate.FieldName).Value    := Date; //对帐日期
      FieldByName(Self.ADOSumInforatio.FieldName).Value        := lpRatio;
      Post;
    end;

  end;

var
  s : string;
  szCaption : string;
  szSumReceivables : Currency; //合同总价
  szSumIncome : Currency;  //交易记录
  szSumRuningAccount : Currency; //流水汇总
  szSumDebtmoney : Currency;  //营收债
  szSumDebtType  : Integer;   //营收债类型
  szSQLText : string;
  szSumMoney:Currency;
  szPaidMoney : Currency;
  szClearMoney: Currency;
  szDetBranchMoney : Currency;
  szDetCollectMoney: Currency;
  szCompanyStorage : Currency;
  szStatus : string;
  szSelect , szWhere , szGroup: string;
  t:Integer;
  ratio : Double;
begin
  // 0 = 个人对帐  1 = 全部汇总
  szStatus := ' status=yes ';
  if (lpIndex = 0) and (Length(lpSignName) = 0) then
  begin
    //个人查询反回0
    Exit;
  end;
  if lpIndex = 0 then  SearchLog(0,lpSignName);
  dwSum.Clear;

  with Self.tvGrid.DataController do
  begin
    if lpIndex <> 2 then
    begin
      SelectAll;
      DeleteSelection;
    end else
    begin
      dwProjectSum.Clear;
      for t := 0 to 7 do dwProjectSum.Add(ProjectName[t],0);
    end;
  end;
  szSumMoney := 0;
  szPaidMoney:= 0;

  GetDetInfo(lpIndex,lpSignName); //取出个人债务
  //**************************************************************************
  //项目营收-合同总价
  szCaption := 'BuildCompany';
  if (lpIndex = 0) or (lpIndex = 2) then s := ' AND '+ szCaption +'="' + lpSignName + '"';
  szSelect := 'SUM(totalsum) AS t,' + szCaption + ' as c';
  szWhere  := szStatus + s ;
  szGroup  := szCaption;
  szSQLText := GetGroupSQL(szSelect, g_Table_Collect_Receivables , szWhere , szGroup);
  szSumReceivables := GetSumMoney(0,szSQLText,ProjectName[0]);  //项目营收-合同总价

  //**************************************************************************
  //项目营收-交易记录
  szCaption := 'PaymentCompany';
  if (lpIndex = 0) or (lpIndex = 2) then s := ' AND '+ szCaption +'="' + lpSignName + '"';
  szSelect := 'SUM(SettleAccountsMoney) AS t,'+ szCaption +' as c';
  szWhere  := szStatus + s;
  szGroup  := szCaption;
  szSQLText:= GetGroupSQL(szSelect , g_Table_Income , szWhere , szGroup);
  szSumIncome := GetSumMoney(1,szSQLText,ProjectName[1]);   //项目营收-交易记录

  //**************************************************************************
  //交易流水帐
  szCaption := 'PaymentType';
  if (lpIndex = 0) or (lpIndex = 2) then s := ' AND '+ szCaption +'="'+ lpSignName +'"';
  szSelect  := 'SUM(SumMoney) as t,'+ szCaption + ' as c';
  szWhere   := 'fromType=0 AND ' + szStatus + s; //
  szGroup   := szCaption;
  {
  szSQLText := 'SELECT SUM(SumMoney) as t,'+ szCaption + ' as c FROM ' + g_Table_RuningAccount +
               ' where fromType=0 AND ' + szStatus + s;
  }
  szSQLText := GetGroupSQL(szSelect,g_Table_RuningAccount,szWhere,szGroup);
  szSumRuningAccount := GetSumMoney(1,szSQLText,ProjectName[2]); //交易流水

  //**************************************************************************
  //营收债务
  szSumMoney := szSumReceivables;  //合同总价
  szPaidMoney:= szSumIncome + szSumRuningAccount + szCompanyStorage;  //交易记录 + 流水帐

  ////////////////////////////////////////////////////////////////////////////
  //公司仓库-营收挂账
  if (lpIndex = 0) or (lpIndex = 2) then s := ' AND ReceiveCompany="' + lpSignName + '"';
  szCaption := 'ReceiveCompany';
  szSelect  := 'SUM(SumMonery) AS t,' + szCaption + ' AS c';
  szWhere   := 'AccountsStatus="营收挂账" and DataType=1 AND ' + szStatus + s;
  szGroup   := szCaption;
  {
  szSQLText := 'Select SUM(SumMonery) AS t from '+ g_Table_Company_StorageDetailed  +
                 ' where AccountsStatus="挂账" and DataType=1 AND ' + szStatus + s;
  }
  szSQLText := GetGroupSQL(szSelect,g_Table_Company_StorageDetailed ,szWhere,szGroup );
  GetSumMoney(0,szSQLText,ProjectName[3]);   //公司仓库-营收挂账
  ////////////////////////////////////////////////////////////////////////////
  {
  szSQLText := 'Select SUM(SumMoney) AS t from '+ g_Table_pactDebtmoney +
                 ' where MoneyType=1 AND ' + szStatus + s;  //收
  }
  //债务-收款
  if (lpIndex = 0) or (lpIndex = 2) then s := ' AND ParentName="'+ lpSignName +'"';
  szCaption := 'ParentName';
  szSelect  := 'SUM(SumMoney) AS t,'+ szCaption + ' AS c';
  szWhere   := 'MoneyType=1 AND '   + szStatus + s;  //收
  szGroup   := szCaption;
  szSQLText := GetGroupSQL(szSelect,g_Table_pactDebtmoney,szWhere , szGroup);
  GetSumMoney(3,szSQLText,ProjectName[4]); //债务-收款
  {
  szSQLText := 'Select SUM(SumMoney) AS t from '+ g_Table_pactDebtmoney +
                 ' where MoneyType=0 AND ' + szStatus + s;  //支
  }
  //债务-支款
  if (lpIndex = 0) or (lpIndex = 2) then  s := ' AND ParentName="'+ lpSignName +'"';
  szCaption := 'ParentName';
  szSelect  := 'SUM(SumMoney) AS t,' + szCaption + ' AS c';
  szWhere   := 'MoneyType=0 AND ' + szStatus + s;
  szGroup   := szCaption;
  szSQLText := GetGroupSQL(szSelect,g_Table_pactDebtmoney,szWhere,szGroup);
  GetSumMoney(2,szSQLText,ProjectName[5]); //债务-支款
  ////////////////////////////////////////////////////////////////////////////

  if(lpIndex = 0) or (lpIndex = 2) then s := ' AND SignName="'+ lpSignName +'"';
  {
  szSQLText := 'Select SUM(detSumMoney) AS t from '+ g_Table_DetList +
                 ' where ModuleIndex=1 AND detType="收"' + s;
  }
  //营收对账-收款
  szCaption:= 'SignName';
  szSelect := 'SUM(detSumMoney) AS t,' + szCaption + ' AS c';
  szWhere  := 'ModuleIndex=1 AND detType="收"' + s;
  szGroup  := szCaption;
  szSQLText:= GetGroupSQL(szSelect,g_Table_DetList,szWhere,szGroup);  //债务
  GetSumMoney(3,szSQLText,ProjectName[6]); //营收对账-收款

  //营收对账-支款
  if lpIndex = 0 then s := ' AND SignName="'+ lpSignName +'"';
  szCaption:= 'SignName';
  szSelect := 'SUM(detSumMoney) AS t,' + szCaption + ' AS c';
  szWhere  := 'ModuleIndex=1 AND detType="支"' + s;
  szGroup  := szCaption;
  szSQLText:= GetGroupSQL(szSelect,g_Table_DetList,szWhere,szGroup);  //债务
  GetSumMoney(2,szSQLText,ProjectName[7]); //营收对账-支款
  {
  szSQLText := 'Select SUM(detSumMoney) AS t from '+ g_Table_DetList +
                 ' where ModuleIndex=1 AND detType="支"' + s;
  }
  ////////////////////////////////////////////////////////////////////////////
   {
  with DM.Qry do
  begin
    if lpIndex = 0 then s := ' AND ReceiveCompany="' + lpSignName + '"';

    szSQLText := 'Select SUM(SumMonery) AS t from '+ g_Table_Company_StorageDetailed  +
                 ' where AccountsStatus="营收挂账" and DataType=1 AND ' + szStatus + s;
    Close;
    SQL.Clear;
    SQL.Text := szSQLText;
    Open;
    if RecordCount <> 0 then szSumMoney  := szSumMoney +  FieldByName('t').AsCurrency;

    //////////////////////////////////////////////////////////////////////////
    if lpIndex = 0 then s := ' AND ParentName="'+ lpSignName +'"';

    szSQLText := 'Select SUM(SumMoney) AS t from '+ g_Table_pactDebtmoney +
                 ' where MoneyType=1 AND ' + szStatus + s;  //收
    Close;
    SQL.Clear;
    SQL.Text := szSQLText;
    Open;
    if RecordCount <> 0 then szSumMoney  := szSumMoney  + FieldByName('t').AsCurrency;
    //////////////////////////////////////////////////////////////////////////
    if lpIndex = 0 then  s := ' AND ParentName="'+ lpSignName +'"';

    szSQLText := 'Select SUM(SumMoney) AS t from '+ g_Table_pactDebtmoney +
                 ' where MoneyType=0 AND ' + szStatus + s;  //支
    Close;
    SQL.Clear;
    SQL.Text := szSQLText;
    Open;
    if RecordCount <> 0 then szPaidMoney  := szPaidMoney + FieldByName('t').AsCurrency;
    //////////////////////////////////////////////////////////////////////////
    //ModuleIndex  营收债务 1
    //流水帐收款债
    if lpIndex = 0 then s := ' AND SignName="'+ lpSignName +'"';
    szSQLText := 'Select SUM(detSumMoney) AS t from '+ g_Table_DetList +
                 ' where ModuleIndex=1 AND detType="收"' + s;
    Close;
    SQL.Clear;
    SQL.Text := szSQLText;
    Open;
    if RecordCount <> 0 then szSumMoney := szSumMoney +  FieldByName('t').AsCurrency;

    if lpIndex = 0 then s := ' AND SignName="'+ lpSignName +'"';

    szSQLText := 'Select SUM(detSumMoney) AS t from '+ g_Table_DetList +
                 ' where ModuleIndex=1 AND detType="支"' + s;
    Close;
    SQL.Clear;
    SQL.Text := szSQLText;
    Open;
    if RecordCount <> 0 then szPaidMoney := szPaidMoney +  FieldByName('t').AsCurrency;
  end;
   }
  if lpIndex <> 2 then
  begin
    with dwSum do
    begin
      if Count <= 0 then
      begin
        //一个也没找到
        ShowMessage('没有找到符合条件的对帐信息！');
      end else
      begin
        //汇总额
        for s in Keys do
        begin
          if ContainsKey(s) then
          begin
            Application.ProcessMessages;
            szSumMoney  := szSumMoney + Items[s].dwSumMoney;
            szPaidMoney := szPaidMoney+ Items[s].dwPaidMoney;
            szDetBranchMoney := szDetBranchMoney + Items[s].dwDetBranchMoney;
            szDetCollectMoney:= szDetCollectMoney+ Items[s].dwDetCollectMoney;
          end;
        end;

        Self.RzPanel15.Caption :=  Format('%2.2m',[szSumMoney]);
        Self.RzPanel17.Caption :=  Format('%2.2m',[szPaidMoney]);
        Self.RzPanel19.Caption :=  Format('%2.2m',[szDetBranchMoney]);
        Self.RzPanel21.Caption :=  Format('%2.2m',[szDetCollectMoney]);
        //-----------------------------------------------------------//
        case lpIndex of
          0:
          begin
            //个人查询

            for s in Keys do
            begin

              if ContainsKey(s) then
              begin
                Application.ProcessMessages;
                dwSumMoney := Items[s].dwSumMoney;
                szSumMoney := Items[s].dwSumMoney + Items[s].dwDetCollectMoney;
                szPaidMoney:= Items[s].dwPaidMoney+ Items[s].dwDetBranchMoney;

                szClearMoney := szSumMoney - szPaidMoney;

                UpdateSumMoney(0,s,szSumMoney,szPaidMoney,szClearMoney,0);
              end;

            end;

            Self.RzPanel12.Caption:= MoneyConvert( szSumMoney );
            Self.RzPanel10.Caption:= MoneyConvert( szPaidMoney );
            Self.RzPanel7.Caption := MoneyConvert( szClearMoney );

            if (szSumMoney <> 0) or (szPaidMoney <> 0) then
              Self.RzToolButton9.Enabled := True
            else
              Self.RzToolButton9.Enabled := False;
            Self.Client.Caption := lpSignName;
            tvGridColumn7.Visible := False;
          end;
          1:
          begin
            //全部查询
            tvGridColumn7.Visible := True;
            Self.RzToolButton9.Enabled := False;
            for s in Keys do
            begin

              if ContainsKey(s) then
              begin
                Application.ProcessMessages;
                szSumMoney := Items[s].dwSumMoney;
                szPaidMoney:= Items[s].dwPaidMoney;
                if (szSumMoney <> 0) or (szPaidMoney <> 0) then
                begin
                  try
                    if szSumMoney = 0 then
                      ratio := 0
                    else
                      ratio := RoundTo( (Items[s].dwSumMoney / szSumMoney) * 100 , -2 );
                  except
                    ratio := 0;
                  end;

                  szClearMoney := szSumMoney - szPaidMoney;
                  UpdateSumMoney(0,s,szSumMoney,szPaidMoney,szClearMoney,ratio);
                end;
              end;

            end;

          end;
        end;

      end;

    end;
  end else
  begin
    szSumMoney  := 0;
    szPaidMoney := 0;
    with dwProjectSum do
    begin

      for s in Keys do
      begin

        if ContainsKey(s) then
        begin
          if items[s]<>0 then
          begin
            {
            ProjectName[0]
            ProjectName[3]
            ProjectName[4]
            ProjectName[6]
            }
            if (s = ProjectName[0] )  or
               (s = ProjectName[3] )  or
               (s = ProjectName[4] )  or
               (s = ProjectName[6] )
                then
            begin
              with Self.SUMData do
              begin
                if not Active then Active := True;
                Append;
                FieldByName(Self.SUMDataPrjectName.FieldName).Value := s;
                FieldByName(Self.SUMDataSumMoney.FieldName).Value   := Items[s];
                //合同总价
                szSumMoney := szSumMoney + Items[s];
              //  ShowMessage(s + ' SumMoney:' +  Format('%2.2m',[szSumMoney]) );
              end;
            end;

            if (s = ProjectName[1]) or
               (s = ProjectName[2]) or
               (s = ProjectName[5]) or
               (s = ProjectName[7])
               then
            begin
              with Self.PaidData do
              begin
                if not Active then Active := True;
                Append;
                FieldByName(Self.PaidDataPrjectName.FieldName).Value := s;
                FieldByName(Self.PaidDataPaidMoney.FieldName).Value  := Items[s];
                //已付
                szPaidMoney := szPaidMoney + Items[s];
              //  ShowMessage(s + ' SumMoney:' +  Format('%2.2m',[szSumMoney]) );
              end;
            end;
            {
            if (s = ProjectName[10]) or
               (s = ProjectName[11]) or
               (s = ProjectName[14]) or
               (s = ProjectName[15]) then
            begin

              if  (s = ProjectName[10]) or
                  (s = ProjectName[14]) then
              begin
                //奖励
                szSumMoney := szSumMoney + Items[s];
              end else
              if (s = ProjectName[11]) or
                 (s = ProjectName[15]) then
              begin
                //扣罚
                szPaidMoney := szPaidMoney + Items[s];
              end;
            end;
            }
            //一个合同总价丶奖的在合同总价
            //一个己付金额丶罚款的在己付
            //两个
          end;
        end;

      end;

      with Self.ChartData do
      begin
        if not Active then Active := True;
        Append;
        FieldByName(Self.ChartProjectName.FieldName).Value := '未付金额';
        FieldByName(Self.ChartSumMoney.FieldName).Value    := szSumMoney;
        Append;
        FieldByName(Self.ChartProjectName.FieldName).Value := '已收金额';
        FieldByName(Self.ChartSumMoney.FieldName).Value    := szPaidMoney;
      end;

    end;

  end;

end;

procedure TfrmRevenueAccount.MenuItem1Click(Sender: TObject);
begin
  inherited;
//  frxReport.DesignReport();
  with DM do
  begin
    BasePrinterLink1.Component := Self.Grid;
  //  BasePrinterLink1.RealPrinterPage.DMPaper := 241 * 280;
  //  DM.BasePrinterLink1.ReportTitleText := '公司名称：' + Self.tvCompanyColumn2.EditValue;
   // DM.BasePrinter.CurrentLink.PrinterPage.PageSize.Create(241,279.4);
    BasePrinter.Preview(True, nil);
  end;

end;

procedure TfrmRevenueAccount.MenuItem2Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'汇总明细表');
end;


procedure TfrmRevenueAccount.MenuItem3Click(Sender: TObject);
begin
  inherited;
//  frxReport.DesignReport();
  with DM do
  begin
    BasePrinterLink1.Component := Self.tvLogInfoView;
  //  BasePrinterLink1.RealPrinterPage.DMPaper := 241 * 280;
  //  DM.BasePrinterLink1.ReportTitleText := '公司名称：' + Self.tvCompanyColumn2.EditValue;
   // DM.BasePrinter.CurrentLink.PrinterPage.PageSize.Create(241,279.4);
    BasePrinter.Preview(True, nil);
  end;

end;

procedure TfrmRevenueAccount.MenuItem4Click(Sender: TObject);
begin
  inherited;
    CxGridToExcel(Self.Grid,'日志明细表');
end;

procedure DeleteData(lpGrid : TcxGridDBTableView ; lpDir  , lpTableName , lpFieldName : string );
var
  Id : string;
  szRecordCount : Integer;
  str , dir :string;
  I: Integer;
  adoTmp : TADOQuery;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s : Variant ;

begin
  szCount := lpGrid.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount > 0 then
  begin
      if  Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin

        with lpGrid  do
        begin
          if not DataController.DataSource.DataSet.IsEmpty then
          begin
            str := '';
            for I := szCount -1 downto 0 do
            begin
              szIndex := GetColumnByFieldName(lpFieldName).Index;

              s := Controller.SelectedRows[i].Values[szIndex];
              Id := VarToStr(s);

              if Length(id) <> 0 then
              begin

                if Length(lpDir) <> 0 then
                begin
                  dir := ConcatEnclosure(lpDir, id);
                  if DirectoryExists(dir) then
                  begin
                    DeleteDirectory(dir);
                  end;

                end;

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;

              end else
              begin
                Controller.DeleteSelection;
              end;

            end;

            if (str <> ',') and (Length(str) <> 0) then
            begin

              with DM.Qry do
              begin

                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  lpTableName +' where '+ lpFieldName +' in("' + str + '") AND ModuleIndex=1';

                ExecSQL;

              end;

            end;

          end;

        end;

      end;
  end;

end;

procedure TfrmRevenueAccount.N11Click(Sender: TObject);
begin
  inherited;
//  frxReport.DesignReport();
  with DM do
  begin
    BasePrinterLink1.Component := Self.Grid;
  //  BasePrinterLink1.RealPrinterPage.DMPaper := 241 * 280;
  //  DM.BasePrinterLink1.ReportTitleText := '公司名称：' + Self.tvCompanyColumn2.EditValue;
   // DM.BasePrinter.CurrentLink.PrinterPage.PageSize.Create(241,279.4);
    BasePrinter.Preview(True, nil);
  end;
end;

procedure TfrmRevenueAccount.N2Click(Sender: TObject);
begin
  inherited;
  IsDeleteData(Self.tvLogInfoView,g_ReconciliationTable,g_TableName,Self.cxGridDBColumn5.DataBinding.FieldName);
  Self.ADOSumLogInfo.Requery(); //刷新
end;

procedure TfrmRevenueAccount.N3Click(Sender: TObject);
begin
  inherited;
  if Self.tvDet.Controller.FocusedRowIndex >= 0 then
  begin
    //债务删除
    //'Select * from '+ g_Table_DetList +' WHERE ModuleIndex=0'
    DeleteData(Self.tvDet,
                 g_Resources + '\det\',
                 g_Table_DetList,
                 Self.DetCol5.DataBinding.FieldName);
    Self.ADODet.Requery();

  end;
end;

procedure TfrmRevenueAccount.N4Click(Sender: TObject);
var
  s : string;
  FileDir : string;

begin
  inherited;
  if Self.tvDet.Controller.FocusedRowIndex >= 0 then
  begin
    s := Self.ADODetSignName.AsString;
    FileDir := Concat(g_Resources,'\det\' + s);
    CreateOpenDir(Handle,FileDir,True);
  end;
end;

procedure TfrmRevenueAccount.N5Click(Sender: TObject);
var
  s : Variant;

begin
  inherited;
  if tvGrid.Controller.FocusedRowIndex >= 0 then
  begin
    s := tvGridColumn3.EditValue;
    if s <> null then
    begin
      GetSUMMoney(0,VarToStr(s));
    end;
  end;


end;

procedure TfrmRevenueAccount.N8Click(Sender: TObject);
var
  szName : Variant;
  Child : TfrmSumDetailed;

begin
  inherited;

  Self.PaidData.Active := False;
  Self.SUMData.Active  := False;
  Self.ChartData.Active:= False;

  if Self.tvGrid.Controller.FocusedRowIndex >= 0 then
  begin
    szName := Self.tvGridColumn3.EditValue;
    if szName <> null then
    begin
      dwIsSumDetailed := True;
      //汇总价
      GetSUMMoney(2, VarToStr(szName) );
      dwIsSumDetailed := False;
      Child := TfrmSumDetailed.Create(Nil);
      szName := Child.Caption + ' - ' + szName;
      Child.Caption := szName;
      Child.Lv2.Caption := '已收金额';
      try
        if Sender = Self.N8 then
        begin
          Child.Grid.ActiveLevel := Child.Lv1;
        end else
        if Sender = Self.N9 then
        begin
          Child.Grid.ActiveLevel := Child.Lv2;
        end;
        Child.SumMem.CopyFromDataSet(Self.SUMData);
        Child.PaidMem.CopyFromDataSet(Self.PaidData);
        Child.ChartData.CopyFromDataSet(Self.ChartData);

        Child.ShowModal;
      finally
        Child.Free;
      end;

    end;
  end;
end;

procedure TfrmRevenueAccount.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmRevenueAccount.FormCreate(Sender: TObject);
begin
  inherited;
  Self.ADOSumInfo.CreateDataSet;
  Self.ADODet.CreateDataSet;

end;

procedure TfrmRevenueAccount.RzPanel13Resize(Sender: TObject);
var
  sumWidth : Integer;
  Width : Integer;
begin
  inherited;
  sumWidth := Self.RzPanel14.Width + Self.RzPanel16.Width + Self.RzPanel18.Width + Self.RzPanel20.Width;
  Width := Trunc( (Self.RzPanel13.Width - sumWidth) / 4 );

  Self.RzPanel15.Width := Width;
  Self.RzPanel17.Width := Width;
  Self.RzPanel19.Width := Width;
  Self.RzPanel21.Width := Width;
end;

procedure TfrmRevenueAccount.RzPanel6Resize(Sender: TObject);
var
  szSumWidth : Double;
  d : Double;

begin
  inherited;
   d := Self.RzPanel6.Width  -
        Self.RzPanel11.Width -
        Self.RzPanel8.Width  -
        Self.RzPanel9.Width;

  szSumWidth := d  / 3;

  Self.RzPanel12.Width := Round(szSumwidth);
  Self.RzPanel7.Width  := Round(szSumwidth);
  Self.RzPanel10.Width := Round(szSumwidth);
end;

procedure TfrmRevenueAccount.RzToolButton10Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := Self.cxLookupComboBox1.Text;
  SearchLog(2,s);
end;

procedure TfrmRevenueAccount.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  GetMaintainInfo;
  ShowMessage('信息刷新完成!');
end;

procedure TfrmRevenueAccount.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  GetSUMMoney(1,'');
end;

procedure TfrmRevenueAccount.RzToolButton4Click(Sender: TObject);
begin
  inherited;
  GetSUMMoney(0,Self.Search.Text);
end;

procedure TfrmRevenueAccount.RzToolButton6Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmRevenueAccount.RzToolButton7Click(Sender: TObject);
begin
  inherited;
  SearchLog(1,'');
end;

function TfrmRevenueAccount.GetDetInfo(lpIndex : Byte ; lpSignName : string):Currency;
var
  s : string;
begin
  //债务表
  {
  case lpIndex of
    0: s :=  ' AND ' + Self.ADODetSignName.FieldName +'="' + lpSignName +'"';
  end;

  with Self.ADODet do
  begin
    Close;
    Self.ADODet.CommandText := 'Select * from '+ g_Table_DetList +' WHERE '+ 'ModuleIndex=1' +  s;
    Open;
  end;
  }
  //债务表
  //0=个人对帐 1=对帐汇总
  if lpIndex <> 2 then
  begin
    with Self.ADODet do
    begin
      Close;

      case lpIndex of
        0:
        begin
          s := 'Select * from '+ g_Table_DetList +' WHERE '+ Self.ADODetSignName.FieldName +'="' + lpSignName +'" AND ModuleIndex=1' ;
          Self.ADODet.CommandText := s;
          Open;
        end;
        1:
        begin
          s := 'Select * from '+ g_Table_DetList +' WHERE ModuleIndex=1' ;
          Self.ADODet.CommandText := s;
          Open;
        end;
        3:
        begin
          s := 'Select * from ' + g_Table_DetList +' WHERE ModuleIndex=1 and ' + Self.DetCol4.DataBinding.FieldName + '="' + lpSignName  +'"' ;
          Self.ADODet.CommandText := s;
          Open;
        end;
      end;

    end;

  end;

end;


function GetSignNameId(lpSignName : string):Integer;
begin
  Result := 0;
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select Id from ' + g_Table_CompanyManage + ' Where SignName="'  + lpSignName + '"';
    Open;
    if RecordCount <> 0 then
    begin
      Result := FieldByName('Id').AsInteger;
    end;
  end;
end;

procedure TfrmRevenueAccount.RzToolButton9Click(Sender: TObject);
var
  szSumMoney  : Currency;
  szInduceType: string;
  szSignId : Integer;
  szInputDate : TDate;
  szPaidMoney : Currency;
  SpareMoney  : TfrmSpareMoney;
  IsSpareStatus : Boolean;
  szSpareMoney , szClearBalance: Currency;
  szDetType : Integer;
  err : string;
  szSignName : string;
  szFieldName: string;
  szFileDir: string;

begin
  inherited;

  if MessageBox(handle, PChar('是否清算?'),'提示',
       MB_ICONQUESTION + MB_YESNO) = IDYES then
  begin

    szClearBalance :=Self.ADOSumInfoClearBalance.AsCurrency;
    if szClearBalance > 0 then
    begin
      szSpareMoney := szClearBalance;
      szDetType := 0;//收
    end else
    begin
      szSpareMoney := 0- szClearBalance;
      szDetType := 1;//支
    end;

    IsSpareStatus := False;
    DM.ADOconn.BeginTrans; //开始事务
    try
      szSignName := Self.ADOSumInfoSignName.AsString;
      szFileDir  := Concat(g_Resources,'\det\' + szSignName);
      //ADOQuery 代码
        if szSpareMoney <> 0 then
        begin
          //弹框写入附件
          //检测欠据
          SpareMoney := TfrmSpareMoney.Create(Application);
          try

            SpareMoney.FieldSignName := Self.ADODetSignName.FieldName;
            SpareMoney.FieldSign_Id  := Self.ADODetSign_Id.FieldName;
            SpareMoney.FielddetDate  := Self.ADODetdetDate.FieldName;
            SpareMoney.FielddetSumMoney := Self.ADODetdetSumMoney.FieldName;
            SpareMoney.FielddetType     := Self.ADODetdetType.FieldName;
            SpareMoney.FildModuleIndex  := Self.ADODetModuleIndex.FieldName;

            SpareMoney.FileDir := szFileDir;
            SpareMoney.ModuleIndex := 1;
            SpareMoney.DetType:=szDetType;
            SpareMoney.cxCurrencyEdit1.Value := szSpareMoney;
            SpareMoney.SignName := Self.ADOSumInfoSignName.AsString;
            SpareMoney.TableName:= g_Table_DetList;
            SpareMoney.ShowModal;

            IsSpareStatus := SpareMoney.IsSpareStatus;
            Self.ADODet.Requery();

          finally
            SpareMoney.Free;
          end;

        end else
        begin
          //删除债务
          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select * from '+ g_Table_DetList +
                        ' WHERE '+ Self.ADODetSignName.FieldName +'="' + szSignName + '" AND ModuleIndex=1' ;
            Open;

            if RecordCount <> 0 then
            begin
              //检测欠据
              Delete;
              IsSpareStatus := True;

            end else
            begin
              IsSpareStatus := True;
            end;
          end;

        end;

        if (IsSpareStatus) then
        begin
            //******************更新状态**********************

           with DM.Qry do
           begin
             Close;
             SQL.Clear;  //合同总价
             SQL.Text := 'UPDATE '+ g_Table_Collect_Receivables +
                         ' SET status=no WHERE BuildCompany="' + szSignName + '" AND status=yes';
             ExecSQL;
             Close;
             SQL.Clear;  //交易记录
             SQL.Text := 'UPDATE '+ g_Table_Income +
                         ' SET status=no WHERE PaymentCompany="' + szSignName + '" AND status=yes';
             ExecSQL;

             Close;
             SQL.Clear; //流水帐记录
             SQL.Text := 'UPDATE '+ g_Table_RuningAccount +
                         ' SET status=no WHERE fromType=0 AND PaymentType="'+ szSignName +'" AND status=yes ';
             ExecSQL;

             Close;
             SQL.Clear; //公司仓库记录
             SQL.Text := 'UPDATE '+ g_Table_Company_StorageBillList +
                         ' SET status=no WHERE AccountsStatus="营收挂账" and DataType=1 AND ReceiveCompany="'+ szSignName +'" AND status=yes ';
             ExecSQL;

             Close;
             SQL.Clear; //公司仓库明细记录
             SQL.Text := 'UPDATE '+ g_Table_Company_StorageDetailed +
                         ' SET status=no WHERE AccountsStatus="营收挂账" and DataType=1 AND ReceiveCompany="'+ szSignName +'" AND status=yes ';
             ExecSQL;

             Close;
             SQL.Clear; //删除营收债务
             SQL.Text := 'delete * from ' + g_Table_pactDebtmoney +' WHERE ParentName="'+ szSignName +'"';  //全部
             ExecSQL;

           end;

            //******************更新状态完毕******************
            with Self.ADOSumLogInfo do
            begin

              if State <> dsInactive then
              begin

                Append;
                  FieldByName(Self.ADOSumInfoInputDate.FieldName).Value  := Self.ADOSumInfoInputDate.AsDateTime;
                  FieldByName(Self.ADOSumInfoSign_Id.FieldName).Value    := GetSignNameId(Self.Client.Caption);  //往来单位ID
                  FieldByName(Self.ADOSumInfoSignName.FieldName).Value   := Self.ADOSumInfoSignName.AsString;
                  FieldByName(Self.ADOSumInfoInduceType.FieldName).Value := Self.ADOSumInfoInduceType.AsString;    //归纳分类
                  FieldByName(Self.ADOSumInfoSumMoney.FieldName).Value   := dwSumMoney ;// Self.ADOSumInfoSumMoney.AsCurrency;    //帐目总价
                  FieldByName('ModuleType').Value := 1;
                Post;
                Requery();

              end else
              begin
                ShowMessage('日志为打开');
              end;

            end;

        end;

       DM.ADOconn.Committrans; //提交事务
    except
      on E:Exception do
      begin
        DM.ADOconn.RollbackTrans;           // 事务回滚
        err:=E.Message;
        ShowMessage(err);
      end;
    end;

    GetSUMMoney(0,Self.ADOSumInfoSignName.AsString);
  end;
end;

procedure TfrmRevenueAccount.SearchPropertiesCloseUp(Sender: TObject);
begin
  inherited;
  Self.RzToolButton4.Click;
end;

procedure TfrmRevenueAccount.TvDblClick(Sender: TObject);
var
  Node : TTreeNode;
  I: Integer;
  szData : PNodeData;
  szCodeId : Integer;

begin
  inherited;
  Node := Self.Tv.Selected;
  if Assigned(Node) then
  begin
    if Node.Level > 0 then
    begin
      szData := PNodeData(Node.Data);
      if Assigned(szData) then
      begin
        szCodeId := szData^.Index;

        with Self.ADOCompany do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from ' + g_Table_CompanyManage + ' WHERE ' + 'Sign_Id' + '=' + IntToStr(szCodeId) ;
          Open;
        end;


      end;

    end;

  end;

end;

procedure TfrmRevenueAccount.tvDetStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
  inherited;
  if ARecord.Values[Self.DetCol4.Index] = '收' then
    AStyle := DM.cxStyle222
  else
    AStyle := DM.cxStyle326;
end;

procedure TfrmRevenueAccount.tvGridColumn13CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
end;

procedure TfrmRevenueAccount.tvGridColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
   AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmRevenueAccount.tvGridColumn3CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
end;

procedure TfrmRevenueAccount.tvGridColumn5CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
  ACanvas.Brush.Color:= clYellow;
  ACanvas.Font.Color := clBlue;
end;

procedure TfrmRevenueAccount.tvSignNameDblClick(Sender: TObject);
var
  szSignName : string;
begin
  inherited;
  //取出往来单
  szSignName := VarToStr( Self.tvSignNameColumn2.EditValue );
  GetSUMMoney(0,szSignName);
  {
  with Self.ADOCompany do
  begin
    if State <> dsInactive then
    begin
      szSignName := FieldByName(Self.tvSignNameColumn2.DataBinding.FieldName).Value;
      GetSUMMoney(szSignName);
    end;
  end;
  }
end;

end.
