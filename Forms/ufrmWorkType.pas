unit ufrmWorkType;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxTextEdit,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, Data.Win.ADODB, RzButton, RzPanel,
  Vcl.ExtCtrls,ufrmBaseController;

type
  TfrmWorkType = class(TfrmBaseController)
    RzPanel7: TRzPanel;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton4: TRzToolButton;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    Grid: TcxGrid;
    tvWorkType: TcxGridDBTableView;
    tvWorkCol1: TcxGridDBColumn;
    tvWorkCol2: TcxGridDBColumn;
    Lv: TcxGridLevel;
    procedure FormActivate(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure tvWorkCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton2Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure tvWorkTypeEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvWorkTypeEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvWorkTypeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvWorkTypeDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    dwIsReadOnly : Boolean;
  end;

var
  frmWorkType: TfrmWorkType;

implementation

uses
   uDataModule,global;

{$R *.dfm}

procedure TfrmWorkType.FormActivate(Sender: TObject);
begin
  with Self.ADOQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_Maintain_WorkType;
    Open;
  end;
end;

procedure TfrmWorkType.RzToolButton1Click(Sender: TObject);
begin
  with Self.ADOQuery1 do
  begin
    if State <> dsInactive then
    begin
      Append;
      Self.tvWorkCol2.FocusWithSelection;
      keybd_event(VK_RETURN,0,0,0);
    end;
  end;
end;

procedure TfrmWorkType.RzToolButton2Click(Sender: TObject);
begin
  IsDeleteEmptyData(Self.ADOQuery1 ,'',Self.tvWorkCol2.DataBinding.FieldName);
end;

procedure TfrmWorkType.RzToolButton3Click(Sender: TObject);
begin
  IsDelete(Self.tvWorkType);
//  IsDeleteData(Self.tvWorkType,'','','');
end;

procedure TfrmWorkType.RzToolButton4Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmWorkType.tvWorkCol1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  AText := IntToStr( ARecord.Index + 1 );
end;

procedure TfrmWorkType.tvWorkTypeDblClick(Sender: TObject);
begin
  inherited;
  if dwIsReadOnly then Close;
end;

procedure TfrmWorkType.tvWorkTypeEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  if dwIsReadOnly then
  begin
    AAllow := False;
  end else
  begin
    if AItem.Index = Self.tvWorkCol1.Index then
    begin
      AAllow := False;
    end;
  end;
end;

procedure TfrmWorkType.tvWorkTypeEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    if AItem.Index = Self.tvWorkCol2.Index then
    begin
      if Self.tvWorkType.Controller.FocusedRow.IsLast then
      begin
          with Self.ADOQuery1 do
          begin
            if State <> dsInactive then
            begin
              Append;
              Self.tvWorkCol2.FocusWithSelection;
              keybd_event(VK_RETURN,0,0,0);
            end;
          end;

      end;
    end;
  end;
end;

procedure TfrmWorkType.tvWorkTypeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    Self.RzToolButton3.Click;
  end;
end;

end.
