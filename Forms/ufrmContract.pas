unit ufrmContract;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls,
  Vcl.ComCtrls, cxGraphics, cxControls,
  ufrmBaseController, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxTextEdit, cxCheckBox, cxDropDownEdit, cxCurrencyEdit, cxDBNavigator,
  Vcl.DBCtrls, Datasnap.DBClient, cxCalendar, cxDBEdit, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxLabel, RzButton, RzPanel,
  cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  cxGroupBox, Vcl.Buttons, Data.Win.ADODB,UnitADO, Vcl.Menus, cxSpinEdit,
  Vcl.StdCtrls, cxMemo, cxButtonEdit , uIStorage, cxPC, dxDockControl,
  dxDockPanel;

type
  TNavgator   =   class(TDBNavigator);
  TfrmContract = class(TfrmBaseController)
    cxGroupBox2: TcxGroupBox;
    Grid: TcxGrid;
    tContractView: TcxGridDBTableView;
    tContractViewColumn1: TcxGridDBColumn;
    tContractViewColumn2: TcxGridDBColumn;
    tContractViewColumn3: TcxGridDBColumn;
    tContractViewColumn4: TcxGridDBColumn;
    tContractViewColumn5: TcxGridDBColumn;
    tContractViewColumn6: TcxGridDBColumn;
    tContractViewColumn14: TcxGridDBColumn;
    tContractViewColumn15: TcxGridDBColumn;
    tContractViewColumn8: TcxGridDBColumn;
    tContractViewColumn9: TcxGridDBColumn;
    tContractViewColumn13: TcxGridDBColumn;
    tContractViewColumn10: TcxGridDBColumn;
    tContractViewColumn7: TcxGridDBColumn;
    tContractViewColumn16: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzToolbar4: TRzToolbar;
    RzSpacer26: TRzSpacer;
    RzToolButton23: TRzToolButton;
    RzSpacer31: TRzSpacer;
    RzToolButton24: TRzToolButton;
    RzSpacer32: TRzSpacer;
    RzSpacer33: TRzSpacer;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzToolButton4: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton5: TRzToolButton;
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    DetailedSource: TDataSource;
    Detailed: TClientDataSet;
    Master: TClientDataSet;
    MasterSource: TDataSource;
    cxDBTextEdit3: TcxDBTextEdit;
    StatusBar1: TStatusBar;
    Bevel1: TBevel;
    Bevel2: TBevel;
    cxDBDateEdit1: TcxDBDateEdit;
    RzToolButton6: TRzToolButton;
    DBNavigator1: TDBNavigator;
    MasterSignName: TWideStringField;
    MasterNoId: TWideStringField;
    cxDBTextEdit4: TcxDBTextEdit;
    ClientMakings: TClientDataSet;
    DataMakings: TDataSource;
    RzSpacer5: TRzSpacer;
    cxDBNavigator1: TcxDBNavigator;
    MasterStorageName: TWideStringField;
    Masterremarks: TWideStringField;
    MasterTreeId: TWideStringField;
    MasterInputDate: TDateTimeField;
    MasterContractName: TWideStringField;
    cxLabel6: TcxLabel;
    Masterstatus: TBooleanField;
    cxDBCheckBox1: TcxDBCheckBox;
    pm: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    MasterModule: TIntegerField;
    RzSpacer6: TRzSpacer;
    Shape2: TShape;
    cxLabel7: TcxLabel;
    RzToolButton7: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzToolButton8: TRzToolButton;
    Timer1: TTimer;
    tContractViewColumn12: TcxGridDBColumn;
    tContractViewColumn11: TcxGridDBColumn;
    cxDBButtonEdit1: TcxDBButtonEdit;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    Company: TClientDataSet;
    CompanySource: TDataSource;
    MasterCompanyName: TWideStringField;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBTextEdit5: TcxDBTextEdit;
    MasterCode: TWideStringField;
    cxDBButtonEdit2: TcxDBButtonEdit;
    N8: TMenuItem;
    N9: TMenuItem;
    cxDBButtonEdit3: TcxDBButtonEdit;
    N10: TMenuItem;
    tContractViewColumn17: TcxGridDBColumn;
    DBCompany: TcxDBButtonEdit;
    procedure RzToolButton3Click(Sender: TObject);
    procedure DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton6Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure DetailedAfterInsert(DataSet: TDataSet);
    procedure tContractViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure DBCol4PropertiesCloseUp(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure tContractViewColumn14GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure cxDBCheckBox1Click(Sender: TObject);
    procedure RzToolButton23Click(Sender: TObject);
    procedure tContractViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure tContractViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton7Click(Sender: TObject);
    procedure tContractViewDblClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tContractViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tViewColumn2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure tViewCol4PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure N8Click(Sender: TObject);
    procedure RzToolButton8Click(Sender: TObject);
    procedure tContractViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure cxDBButtonEdit3PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure N10Click(Sender: TObject);
    procedure DBCompanyPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
    dwMasterSQLText : string;
    dwDetailSQLText : string;
    dwADO : TADO;
    dwContract : string;
    function IsEmpty():Boolean;
    procedure WndProc(var message:TMessage);override; //重载窗口消息过程
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
  public
    { Public declarations }
    dwIsEdit : Boolean;
    dwStorageName : string;
    dwNodeId : Integer;
    dwModule : Integer;  // 0 = 入库 1 = 出库 2 = 编辑
    dwNoId : string;
    dwCompanyName  : string;
    dwCompanyParentId : Integer;
  end;

var
  frmContract: TfrmContract;

implementation

uses
   ufrmCompanyInfo,
   ufrmMaintainProject,
   ufrmIsViewGrid,
   uDataModule,
   global,
   ufrmCalcTool,
   ufrmStorageTreeView,
   ufrmCompanyManage,
   ufrmMakings;

{$R *.dfm}


//英文叫：DockPanel
//中文叫：磁吸面板，磁吸窗体，停靠面板

procedure TfrmContract.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, lpSuffix);
  end;
end;

procedure TfrmContract.WndProc(var Message: TMessage);
var
  pMsg : PMakings;

begin
  case Message.Msg of
    WM_LisView :
    begin
      pMsg := PMakings(message.LParam);
      if Assigned(pMsg) then
      begin
        tContractViewColumn4.EditValue := pMsg.dwMakingCaption;
        tContractViewColumn5.EditValue := pMsg.dwModelIndex;
        tContractViewColumn6.EditValue := pMsg.dwSpec;
        tContractViewColumn7.EditValue := pMsg.dwBrand;
      end;
    end; // 这里加上Exit就不再继续传递了
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

function TfrmContract.IsEmpty():Boolean;
begin
  Result := False;
  if Length( Self.cxDBButtonEdit2.Text ) = 0 then
  begin
    //往来单位为空
    Application.MessageBox( '往来单位不能为空！', '新增记录？', MB_OKCANCEL + MB_ICONWARNING);
    Result := True;
  end else
  if Length(Self.cxDBButtonEdit1.Text) = 0 then
  begin
    //仓库名称为空
    Application.MessageBox( '仓库名称不能为空！', '新增记录？', MB_OKCANCEL + MB_ICONWARNING);
    Result := True;
  end else
  if Length(Self.cxDBButtonEdit3.Text) = 0 then
  begin
    //合同名称为空
    Application.MessageBox( '合同名称不能为空！', '新增记录？', MB_OKCANCEL + MB_ICONWARNING);
    Result := True;
  end else
  if Length(Self.cxDBTextEdit3.Text) = 0 then
  begin
    //备注为空
  end else
  if Length(Self.cxDBDateEdit1.Text) = 0 then
  begin
    //日期为空
    Application.MessageBox( '日期不能为空！', '新增记录？', MB_OKCANCEL + MB_ICONWARNING);
    Result := True;
  end;
end;

function NavClick(Nav : TDBNavigator; lpIndex : Byte):Boolean;
begin
  {
  case lpIndex of
    0: TNavgator(Nav).Buttons[nbPrior].Click;  //上一条
    1: TNavgator(Nav).Buttons[nbNext].Click;   //下一条
    2:
    begin

    end;
  end;
  }
//  TNavgator(DBNavigator1).Buttons[nbNext].Click;
//     TMyNavgator(DBNavigator1).Buttons[nbFirst].Glyph:=nil;   //不显示图标
   {
   TMyNavgator(DBNavigator1).Buttons[nbFirst].Caption  := '第一条 ';
   TMyNavgator(DBNavigator1).Buttons[nbPrior].Caption  := '上一条 ';
   TMyNavgator(DBNavigator1).Buttons[nbNext].Caption   := '下一条 ';
   TMyNavgator(DBNavigator1).Buttons[nbLast].Caption   := '最后一条 ';
   TMyNavgator(DBNavigator1).Buttons[nbInsert].Caption := '添加 ';
   TMyNavgator(DBNavigator1).Buttons[nbDelete].Caption := '删除 ';
   TMyNavgator(DBNavigator1).Buttons[nbEdit].Caption   := '修改 ';
   TMyNavgator(DBNavigator1).Buttons[nbPost].Caption   := '提交 ';
   TMyNavgator(DBNavigator1).Buttons[nbCancel].Caption := '撤销 ';
   TMyNavgator(DBNavigator1).Buttons[nbRefresh].Caption:= '刷新 ';
   }
end;

procedure TfrmContract.tContractViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmContract.tViewCol4PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmMakings;
begin
  inherited;
  Child := TfrmMakings.Create(nil);
  try
    Child.dwParentHandle := Handle;
    Child.dwIsReadonly   := True;
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;

procedure TfrmContract.tViewColumn2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);

begin
  inherited;
  Application.CreateForm(TfrmCalcTool,frmCalcTool);
  try
    frmCalcTool.ShowModal;
    frmCalcTool.Free;
  except on E: Exception do
  end;
end;

procedure TfrmContract.tContractViewColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tContractView,1, dwContract);
end;

procedure TfrmContract.tContractViewColumn14GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;
begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;

  end;

end;

procedure TfrmContract.cxDBButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  szParent : Integer;
begin
  inherited;
  szParent := 0;
  with Self.Company do
  begin
    if Locate('SignName',DBCompany.Text,[]) then
      szParent := FieldByName('parent').Value;
  end;

  if szParent <> 0 then
  begin
    Application.CreateForm(TfrmStorageTreeView,frmStorageTreeView);
    try
      frmStorageTreeView.dwTableName := g_Table_Company_StorageTree;
      frmStorageTreeView.dwParent    := szParent;
      frmStorageTreeView.dwTreeName  := '公司仓库';
      frmStorageTreeView.ShowModal;

      cxDBButtonEdit1.EditValue := frmStorageTreeView.dwStorageName;
      cxDBButtonEdit1.PostEditValue;

      dwNodeId   := frmStorageTreeView.dwNodeId;

      Self.cxDBTextEdit1.EditValue := frmStorageTreeView.dwTreeCode;
      Self.cxDBTextEdit1.PostEditValue;

      Self.cxDBTextEdit5.EditValue := dwNodeId;
      Self.cxDBTextEdit5.PostEditValue;

    finally
      frmStorageTreeView.Free;
    end;
  end else
  begin
    Application.MessageBox( '重新选择公司名称！', 'Error', MB_OKCANCEL + MB_ICONWARNING);
  end;

end;

procedure TfrmContract.cxDBButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child: TfrmCompanyManage;
begin
  inherited;
  Child := TfrmCompanyManage.Create(nil);
  try
    Child.dwIsReadonly := True;
    Child.ShowModal;
    (Sender as TcxDBButtonEdit).EditValue := Child.dwSignName;
    (Sender as TcxDBButtonEdit).PostEditValue;
  finally
    Child.Free;
  end;
end;

procedure TfrmContract.cxDBButtonEdit3PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmMaintainProject;
begin
  inherited;
  Application.CreateForm(TfrmMaintainProject,frmMaintainProject);
  try
    frmMaintainProject.dwIsReadoly := True;
    frmMaintainProject.ShowModal;

    Self.cxDBButtonEdit3.Text := frmMaintainProject.dwSingName;
    Self.cxDBButtonEdit3.PostEditValue;
  finally
    frmMaintainProject.Free;
  end;

end;

procedure TfrmContract.cxDBCheckBox1Click(Sender: TObject);
begin
  inherited;
  if Self.cxDBCheckBox1.EditValue then
  begin
    Self.StatusBar1.Panels[0].Text := '数据解冻';
    Self.cxDBButtonEdit2.Enabled := True;
    Self.cxDBButtonEdit1.Enabled := True;
    Self.cxDBButtonEdit3.Enabled := True;
    Self.cxDBTextEdit3.Enabled := True;
    Self.cxDBTextEdit4.Enabled := True;
    Self.cxDBDateEdit1.Enabled := True;
    Self.Grid.Enabled := True;
  end else
  begin
    Self.StatusBar1.Panels[0].Text := '数据冻结';
    Self.cxDBButtonEdit2.Enabled := False;
    Self.cxDBButtonEdit1.Enabled := False;
    Self.cxDBButtonEdit3.Enabled := False;
    Self.cxDBTextEdit3.Enabled := False;
    Self.cxDBTextEdit4.Enabled := False;
    Self.cxDBDateEdit1.Enabled := False;
    Self.Grid.Enabled := False;
  end;
end;

procedure TfrmContract.DBCol4PropertiesCloseUp(Sender: TObject);
  {
var
  szColContentA : Variant;
  szColContentB : Variant;
  szColContentC : Variant;
  szColContentD : Variant;
  szRowIndex:Integer;
  szColName : string;
  szCount : Variant;
  }
begin
  inherited;
  {
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;

    if szRowIndex >= 0 then
    begin
      szColContentA :=  DataController.Values[szRowIndex,0] ; //名称
      szColContentB :=  DataController.Values[szRowIndex,1] ; //型号
      szColContentC :=  DataController.Values[szRowIndex,2] ; //规格

      if szColContentA <> null then
      begin
        Self.tContractViewColumn4.EditValue := szColContentA;  //名称
      end;
      if szColContentB <> null then
      begin
        Self.tContractViewColumn5.EditValue := szColContentB;  //型号
      end;
      if szColContentC <> null then
      begin
        Self.tContractViewColumn6.EditValue := szColContentC;  //规格
      end;

    end;

  end;
  }
end;

procedure TfrmContract.DBCompanyPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  szCompanyName : string;
begin
  inherited;
  Application.CreateForm(TfrmCompanyInfo,frmCompanyInfo);
  try
    frmCompanyInfo.dwTableName := g_Table_Company_StorageTree;
    frmCompanyInfo.ShowModal;
    szCompanyName := frmCompanyInfo.dwCompanyName;
    if szCompanyName <> '' then
    begin
      (Sender as TcxDBButtonEdit).Text := frmCompanyInfo.dwCompanyName;
      (Sender as TcxDBButtonEdit).PostEditValue;
      //读取公司
      dwADO.OpenSQL(Self.Company, 'Select * from ' + g_Table_Company_StorageTree + ' Where PID=0'); //读取公司
    end;

  finally
    frmCompanyInfo.Free;
  end;
end;

procedure TfrmContract.DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
begin
{
TNavigateBtn   =   (nbFirst,   nbPrior,   nbNext,   nbLast,
                                      nbInsert,   nbDelete,   nbEdit,   nbPost,   nbCancel,   nbRefresh);
  -------------------------------
  procedure TForm1.DBNavigator1Click(Sender : TObject; Button: TNavigateBtn);
  begin
        if  nbInsert=Button   then
          showmessage('点了插入');

  end;
}
end;

procedure TfrmContract.DetailedAfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    FieldByName(Self.MasterNoId.FieldName).Value := Self.cxDBTextEdit4.EditValue;
    FieldByName(Self.MasterTreeId.FieldName).Value := dwNodeId;
    FieldByName(Self.tContractViewColumn17.DataBinding.FieldName).Value := '商品主材';
  //  FieldByName('Code').Value :=
  end;
end;

procedure TfrmContract.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  szNoSave : Boolean;
begin
  inherited;
  szNoSave := False;
  with Master do
  begin
    if ChangeCount > 0 then szNoSave := True;
  end;

  with Detailed do
  begin
    if ChangeCount > 0 then szNoSave := True;
  end;
  
  if szNoSave then
  begin
    ShowMessage('有未保存的数据，是否要退出');
  end;
end;

procedure TfrmContract.MasterAfterInsert(DataSet: TDataSet);
var
  Suffix , szConName , szCode : string;
begin
  inherited;
  Suffix := '000000';
  szConName := Self.MasterNoId.FieldName;
  szCode := GetRowCode(g_Table_Company_Storage_ContractList,szConName,Suffix,1270001);
  with DataSet do
  begin
    FieldByName(Self.MasterNoId.FieldName).Value   := szCode;
    FieldByName(Self.MasterTreeId.FieldName).Value := dwNodeId;
    FieldByName(Self.Masterstatus.FieldName).Value := True;
    FieldByName(Self.MasterModule.FieldName).Value := dwModule;
    FieldByName(Self.DBCompany.DataBinding.DataField).Value  := dwCompanyName;
    //dwCompanyParentId : Integer;
  end;
  Self.cxDBButtonEdit1.EditValue := dwStorageName;
  Self.cxDBButtonEdit1.PostEditValue;
  Self.cxDBDateEdit1.EditValue := Date;
  Self.cxDBDateEdit1.PostEditValue;
  
end;

procedure TfrmContract.N10Click(Sender: TObject);
begin
  inherited;
  Self.Detailed.First;
  Self.Detailed.Insert;
  Self.Grid.SetFocus;
  Self.tContractViewColumn4.Editing := True;
end;

procedure TfrmContract.N1Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton1.Click;
end;

procedure TfrmContract.N2Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton6.Click;
end;

procedure TfrmContract.N3Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton2.Click;
end;

procedure TfrmContract.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton23.Click;
end;

procedure TfrmContract.N6Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton4.Click;
end;

procedure TfrmContract.N7Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton5.Click;
end;

procedure TfrmContract.N8Click(Sender: TObject);
var
  szDir , n : string;
begin
  inherited;
  n := Self.cxDBTextEdit4.EditValue;
  szDir := Concat(g_Resources, '\'+ g_DirCompanyStorageContractList);
  CreateOpenDir(Handle,Concat(szDir,'\' + VarToStr(n)),True);
end;

procedure TfrmContract.RzToolButton1Click(Sender: TObject);
begin
  if dwModule <> 2 then
  begin
    if DBNavigator1.DataSource.State in [ dsEdit , dsInsert] then
    begin
      Self.Detailed.First;
      Self.Detailed.Insert;
      Self.Grid.SetFocus;
      Self.tContractViewColumn4.Editing := True;
    end else
    begin
      TNavgator(DBNavigator1).Buttons[nbInsert].Click;
      Self.Detailed.Append;
      DBCompany.SetFocus;
    end;
  end;
  
end;


procedure TfrmContract.RzToolButton23Click(Sender: TObject);
var
  ADetailDC: TcxGridDataController;
  AView: TcxCustomGridTableView;
  s : string;
  szSQLText : string;
  szRecordCount : Integer;
  szCount , ErrorCount : Integer;

begin
  inherited;
  szCount := Self.tContractView.Controller.SelectedRowCount;
  if szCount > 0 then
  begin
    if Application.MessageBox(PWideChar( '您确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
      //主表删除
      s := GetTableSelectedId(Self.tContractView,tContractViewColumn3.Index);
      DM.ADOconn.BeginTrans; //开始事务
      try
        if dwModule <> 2 then
        begin
          Self.tContractView.Controller.DeleteSelection;
        end else
        begin
        //  szSQLText := 'delete * from ' +  g_Table_Company_Storage_ContractDetailed  +' where '+ 'NoId' +' in("' + s + '")';
        //  ShowMessage(szSQLText);
        //  dwADO.ExecSQL(szSQLText);
          dwADO.ADODeleteSelectionData(Self.tContractView,Self.Detailed,dwDetailSQLText);
          Self.Timer1.Enabled := True;
        end;
        DM.ADOconn.Committrans; //提交事务
      except
        on E:Exception do
        begin
          DM.ADOconn.RollbackTrans;           // 事务回滚
        //  err:=E.Message;
        //  ShowMessage(err);
        end;
      end;
    end;

  end;

end;

procedure TfrmContract.RzToolButton2Click(Sender: TObject);
VAR
  ErrorCount : Integer;
  err:string;
begin
  inherited;
  DM.ADOconn.BeginTrans; //开始事务
  try
    //删除空行
    dwADO.DeleteRowEmpty(Detailed,tContractViewColumn4.DataBinding.FieldName);
    if dwADO.UpdateTableData(g_Table_Company_Storage_ContractList,Self.MasterNoId.FieldName,0,Self.Master) and
       dwADO.UpdateTableData(g_Table_Company_Storage_ContractDetailed,Self.MasterNoId.FieldName,0,Self.Detailed)  then
    begin
      ShowMessage('保存成功!');
      Timer1Timer(Sender);
    end else
    begin
      ShowMessage('保存失败!!');
    end;
    DM.ADOconn.Committrans; //提交事务
  except
    on E:Exception do
    begin
      DM.ADOconn.RollbackTrans;           // 事务回滚
      err:=E.Message;
      ShowMessage(err);
    end;
  end;

end;

procedure TfrmContract.RzToolButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmContract.RzToolButton4Click(Sender: TObject);
begin
  inherited;
  TNavgator(DBNavigator1).Buttons[nbPrior].Click;
end;

procedure TfrmContract.RzToolButton5Click(Sender: TObject);
begin
  inherited;
  TNavgator(DBNavigator1).Buttons[nbNext].Click;
end;

procedure TfrmContract.RzToolButton6Click(Sender: TObject);
begin
  inherited;
  
  if Not IsEmpty then
  begin
    dwADO.DeleteRowEmpty(Detailed,tContractViewColumn4.DataBinding.FieldName);
    with Self.DBNavigator1 do
    begin
      if DataSource.State in [dsEdit , dsInsert] then
      begin
        TNavgator(DBNavigator1).Buttons[nbPost].Click;
      end;
    end;
  end;
end;

procedure TfrmContract.RzToolButton7Click(Sender: TObject);
begin
  inherited;
  if Self.cxDBCheckBox1.EditValue = False then
  begin
    ShowMessage('主合同冻结状态商品明细禁止编辑!');
    Exit;
  end;
  dwADO.ADOIsEdit(Self.Detailed);
end;

procedure TfrmContract.RzToolButton8Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;  //表格设置
begin
  inherited;
  //http://www.a-hospital.com/
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := dwContract;
    Child.ShowModal;
    SetcxGrid(Self.tContractView,0, dwContract);
  finally
    Child.Free;
  end;

end;

procedure TfrmContract.tContractViewDblClick(Sender: TObject);
begin
  inherited;
  if tContractView.Focused then Self.RzToolButton7.Click;
end;

procedure TfrmContract.tContractViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Detailed,AAllow);
end;

procedure TfrmContract.tContractViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = tContractViewColumn15.Index) then
  begin
    Self.Detailed.First;
    Self.Detailed.Insert;
    Self.Grid.SetFocus;
    Self.tContractViewColumn4.FocusWithSelection;
  //  AItem.DataBinding.DataController.Post();
  end;
end;

procedure TfrmContract.tContractViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then Self.RzToolButton23.Click;
end;

procedure TfrmContract.Timer1Timer(Sender: TObject);
var
  szWhere : string;

begin
  inherited;
  Timer1.Enabled := False;

  dwContract := 'Contract';
  SetcxGrid(Self.tContractView,0, dwContract);

  dwADO.OpenSQL(Self.Company, 'Select * from ' + g_Table_Company_StorageTree + ' Where PID=0'); //读取公司

  if dwModule = 2 then
    szWhere := ' WHERE ' + Self.MasterNoId.FieldName + '="' + dwNoId +'"'
  else
    szWhere := ' WHERE 1=2';

  with DM do
  begin
    ADOQuery1.Close;
    ADOQuery1.SQL.Clear;
    ADOQuery1.SQL.Text := 'Select * from ' + g_Table_MakingsList;
    ADOQuery1.Open;
    Self.ClientMakings.Data := DataSetProvider1.Data;
    Self.ClientMakings.Open;  //获取材料列表

    dwMasterSQLText := 'Select * from ' + g_Table_Company_Storage_ContractList + szWhere;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := dwMasterSQLText;
    ADOQuery1.Open;
    Self.Master.Data := DataSetProvider1.Data;
    Self.Master.Open;

    dwDetailSQLText := 'Select * from '+ g_Table_Company_Storage_ContractDetailed + szWhere;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := dwDetailSQLText;
    ADOQuery1.Open;
    Self.Detailed.Data := DataSetProvider1.Data;
    Self.Detailed.Open;

  end;

  case dwModule of
    0: Self.cxLabel7.Caption := '入库单';
    1: Self.cxLabel7.Caption := '出库单';
    2: Self.cxLabel7.Caption := '编辑';
  end;

  if dwModule <> 2 then
  begin
  //  Self.RzToolButton1.Click;
  end else
  begin
    Self.Master.Edit;
    Self.Detailed.Edit;
  end;

end;

end.
