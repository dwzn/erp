unit ufrmContrctList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxTextEdit,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxContainer, cxMaskEdit, cxDropDownEdit,
  cxLabel, RzButton, RzPanel, Vcl.ExtCtrls, Datasnap.DBClient,UnitADO,
  Vcl.StdCtrls;

type
  TfrmContrctList = class(TForm)
    Grid: TcxGrid;
    tView: TcxGridDBTableView;
    tView1: TcxGridDBColumn;
    tView7: TcxGridDBColumn;
    tView8: TcxGridDBColumn;
    tView2: TcxGridDBColumn;
    tView3: TcxGridDBColumn;
    tView4: TcxGridDBColumn;
    tView5: TcxGridDBColumn;
    tView6: TcxGridDBColumn;
    tView9: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzToolbar4: TRzToolbar;
    RzSpacer26: TRzSpacer;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    RzToolButton2: TRzToolButton;
    tmr: TTimer;
    Master: TClientDataSet;
    MasterSource: TDataSource;
    procedure RzToolButton2Click(Sender: TObject);
    procedure tmrTimer(Sender: TObject);
    procedure tView1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tViewDblClick(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    dwADO : TADO;
    function GetSQLtext : string;
  public
    { Public declarations }
    dwModuleIndex : Integer;
    dwCompanyName : Variant;
  end;

var
  frmContrctList: TfrmContrctList;

implementation

uses
   uDataModule,global;

{$R *.dfm}


procedure TfrmContrctList.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    RzToolButton1.Click;
end;

function TfrmContrctList.GetSQLtext : string;
begin
  Result := 'Select * from ' + g_Table_Company_Storage_ContractList +
       ' Where ' + tView7.DataBinding.FieldName +
       '=yes AND ModuleIndex=' + IntToStr(dwModuleIndex);
end;

procedure TfrmContrctList.RzToolButton1Click(Sender: TObject);
var
  lvSQLText : string;

begin
  lvSQLText := GetSQLtext + ' AND ' + tView3.DataBinding.FieldName + '="' + cxTextEdit1.Text +'"';
  dwADO.OpenSQL(Self.Master,lvSQLText);
end;

procedure TfrmContrctList.RzToolButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmContrctList.tmrTimer(Sender: TObject);
var
  s : string;
begin
  tmr.Enabled := False;
  s := GetSQLtext;
  dwADO.OpenSQL(Self.Master,s);
end;

procedure TfrmContrctList.tView1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  AText := IntToStr(ARecord.Index +1);
end;

procedure TfrmContrctList.tViewDblClick(Sender: TObject);
begin
  dwCompanyName := Self.tView3.EditValue;
  Close;
end;

end.


