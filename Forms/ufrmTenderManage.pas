unit ufrmTenderManage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, ComCtrls, RzButton, ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, RzListVw, GridsEh, DBAxisGridsEh, DBGridEh, StdCtrls, RzCmboBx, Mask, RzEdit, RzLabel, RzPanel, PrnDbgeh, TreeFillThrd, TreeUtils, DB, ADODB, ImgList,
  FileCtrl, RzFilSys, Menus, EhLibVCL, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxCore, cxDateUtils,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxLabel, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxDBLookupComboBox,ufrmBaseController;

type
  TfrmTenderManage = class(TfrmBaseController)
    RzPanel1: TRzPanel;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    RzToolbar12: TRzToolbar;
    RzSpacer100: TRzSpacer;
    RzToolButton75: TRzToolButton;
    RzSpacer101: TRzSpacer;
    RzToolButton76: TRzToolButton;
    RzSpacer102: TRzSpacer;
    RzToolButton77: TRzToolButton;
    RzSpacer103: TRzSpacer;
    RzToolButton80: TRzToolButton;
    RzSpacer106: TRzSpacer;
    RzSpacer107: TRzSpacer;
    RzToolButton81: TRzToolButton;
    RzSpacer109: TRzSpacer;
    RzSpacer110: TRzSpacer;
    cxLabel50: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel51: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    Grid: TcxGrid;
    TvTableView: TcxGridDBTableView;
    Lv: TcxGridLevel;
    TvCol1: TcxGridDBColumn;
    TvCol2: TcxGridDBColumn;
    TvCol3: TcxGridDBColumn;
    TvCol4: TcxGridDBColumn;
    TvCol5: TcxGridDBColumn;
    TvTableViewColumn1: TcxGridDBColumn;
    TvTableViewColumn2: TcxGridDBColumn;
    Splitter1: TSplitter;
    RzPanel2: TRzPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton75Click(Sender: TObject);
    procedure ADOQuery1AfterInsert(DataSet: TDataSet);
    procedure RzToolButton76Click(Sender: TObject);
    procedure TvTableViewColumn1GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure RzToolButton80Click(Sender: TObject);
    procedure TvCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure ADOQuery1AfterOpen(DataSet: TDataSet);
    procedure RzToolButton77Click(Sender: TObject);
    procedure RzToolButton81Click(Sender: TObject);
    procedure TvTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TvTableViewDblClick(Sender: TObject);
    procedure TvTableViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
  private
    { Private declarations }
    g_Table_PactTree : string;
    g_ProjectName : string;
    g_PostCode    : string;
    IsSave : Boolean;
    Prefix : string;
    Suffix : string;
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  public
    { Public declarations }
    g_PatTree: TTreeUtils;
    procedure ShowData(lpCodeList: string);
  end;

var
  frmTenderManage: TfrmTenderManage;


implementation

uses
  uProjectFrame,
  ShellAPI,
  uDataModule,
  global,
  ufunctions;

{$R *.dfm}

procedure TfrmTenderManage.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
    begin
      Self.ADOQuery1.Close;
    end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;

      with Self.ADOQuery1 do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'select * from ' + g_Table_Project_Tender + ' where code in(''' + g_PostCode + ''')';
        OutputLog(SQL.Text);
        Open;
      end;

    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;
      szTableList[0].ATableName := g_Table_Project_Tender;
      szTableList[0].ADirectory := g_Dir_Project_Tender;

      DeleteTableFile(szTableList,szCodeList);
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmTenderManage.ADOQuery1AfterInsert(DataSet: TDataSet);
var
  szCode : string;
  szColName : string;
  Aptitude  : string;
  ProjectDepartment : string;

begin
  Aptitude := GetProjectSurvey(g_PostCode,'AptitudeCompany');
  ProjectDepartment := GetProjectSurvey(g_PostCode,'ProjectDepartment');
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      szColName := Self.TvCol2.DataBinding.FieldName;
      szCode := Prefix + GetRowCode( g_Table_Project_Tender ,szColName,Suffix,100000);
      FieldByName(Self.TvCol2.DataBinding.FieldName).Value := szCode;
      FieldByName(Self.TvCol5.DataBinding.FieldName).Value := Date;
      FieldByName(Self.TvCol3.DataBinding.FieldName).Value := g_ProjectName;
      FieldByName('Code').Value := g_PostCode;
      FieldByName(Self.TvCol4.DataBinding.FieldName).Value := Aptitude;
       FieldByName(Self.TvTableViewColumn2.DataBinding.FieldName).Value := ProjectDepartment;
    end;

  end;

end;

procedure TfrmTenderManage.ADOQuery1AfterOpen(DataSet: TDataSet);
begin
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmTenderManage.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmTenderManage.FormCreate(Sender: TObject);
var
  i : Integer;
  List : TStringList;
  ChildFrame : TProjectFrame;
begin
  inherited;
  Prefix := 'ZB-';
  Suffix := '00010';

  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.Parent := Self.RzPanel2;
  ChildFrame.IsSystem := True;
  ChildFrame.FrameClick(Self);

  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
end;

procedure TfrmTenderManage.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then  Close;
  
end;

procedure TfrmTenderManage.RzToolButton75Click(Sender: TObject);
begin
  with Self.ADOQuery1 do
  begin
    if State = dsInactive then
    begin
      Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin
      First;
      Insert;
      Self.TvCol3.FocusWithSelection;
      Self.Grid.SetFocus;
      keybd_event(VK_RETURN,0,0,0);
    end;
  end;
end;

procedure TfrmTenderManage.RzToolButton76Click(Sender: TObject);
begin
  IsDeleteEmptyData(Self.ADOQuery1,
                    g_Table_Project_Tender ,
                    Self.TvCol4.DataBinding.FieldName);
  IsSave := False;
end;

procedure TfrmTenderManage.RzToolButton77Click(Sender: TObject);
begin
  DeleteSelection(Self.TvTableView,'',g_Table_Project_Tender,
  Self.TvCol2.DataBinding.FieldName);
  Self.ADOQuery1.Requery();
end;

procedure TfrmTenderManage.RzToolButton80Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmTenderManage.RzToolButton81Click(Sender: TObject);
var
  sqltext : string;
  szColName : string;
begin
  inherited;
  szColName   := Self.TvCol5.DataBinding.FieldName;
  sqltext := 'select *from '  + g_Table_Project_Tender +
             ' where Code= "' + g_PostCode +
             ' " and ' + szColName + ' between :t1 and :t2';

  SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.ADOQuery1);
end;

procedure TfrmTenderManage.ShowData(lpCodeList: string);
var
  szCode: string;
begin
  with Self.ADOQuery1 do
  begin
    Close;
    SQL.Text := 'select * from ' + g_Table_Project_Tender + ' where code in(''' + lpCodeList + ''')';
    Open;
  end;
end;

procedure TfrmTenderManage.TvCol1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmTenderManage.TvTableViewColumn1GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add('资质');
    Items.Add('报价');
    Items.Add('中标书');
    Items.Add('合同');
  end;
end;

procedure TfrmTenderManage.TvTableViewDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir :=  Concat(g_Resources,'\' + g_Dir_Project_Tender);
  CreateEnclosure(Self.TvTableView.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmTenderManage.TvTableViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  if (AItem.Index = Self.TvCol1.Index) or (AItem.Index = Self.TvCol2.Index) then
  begin
    AAllow := False;
  end;
end;

procedure TfrmTenderManage.TvTableViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    Self.RzToolButton77.Click;
  end;
end;

end.


