unit ufrmAddConcrete;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RzTabs, ExtCtrls, RzPanel, StdCtrls, Mask, RzEdit,
  ComCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, cxTextEdit, cxCurrencyEdit, cxLabel, cxMaskEdit,
  cxSpinEdit, cxTimeEdit, cxDropDownEdit, cxCalendar, cxMemo, Menus, cxButtons,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, dxCore,
  cxDateUtils, RzButton;

type
  TfrmAddConcrete = class(TForm)
    GroupBox1: TGroupBox;
    cxTextEdit1: TcxTextEdit;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    cxLabel4: TcxLabel;
    cxTextEdit4: TcxTextEdit;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxTimeEdit1: TcxTimeEdit;
    cxTimeEdit2: TcxTimeEdit;
    cxLabel7: TcxLabel;
    cxTextEdit5: TcxTextEdit;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxTextEdit6: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxLabel1: TcxLabel;
    cxLabel10: TcxLabel;
    cxComboBox1: TcxComboBox;
    cxLabel11: TcxLabel;
    cxComboBox2: TcxComboBox;
    cxLabel12: TcxLabel;
    cxComboBox3: TcxComboBox;
    cxLabel13: TcxLabel;
    cxComboBox4: TcxComboBox;
    cxLabel14: TcxLabel;
    cxComboBox5: TcxComboBox;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxLabel15: TcxLabel;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxLabel16: TcxLabel;
    cxCurrencyEdit3: TcxCurrencyEdit;
    cxLabel17: TcxLabel;
    cxCurrencyEdit4: TcxCurrencyEdit;
    cxLabel18: TcxLabel;
    cxCurrencyEdit5: TcxCurrencyEdit;
    cxLabel19: TcxLabel;
    cxLabel20: TcxLabel;
    cxLabel21: TcxLabel;
    cxLabel22: TcxLabel;
    cxLabel23: TcxLabel;
    cxLabel24: TcxLabel;
    cxMemo1: TcxMemo;
    cxLabel25: TcxLabel;
    cxCurrencyEdit6: TcxCurrencyEdit;
    cxCurrencyEdit7: TcxCurrencyEdit;
    cxLabel26: TcxLabel;
    cxLabel27: TcxLabel;
    cxLabel28: TcxLabel;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn8: TRzBitBtn;
    RzBitBtn9: TRzBitBtn;
    cxLabel29: TcxLabel;
    cxLabel30: TcxLabel;
    cxLabel31: TcxLabel;
    cxLabel32: TcxLabel;
    cxLabel33: TcxLabel;
    cxLabel34: TcxLabel;
    cxButton1: TcxButton;
    cxLabel35: TcxLabel;
    cxTextEdit7: TcxTextEdit;
    procedure FormCreate(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn4Click(Sender: TObject);
    procedure RzBitBtn5Click(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit2PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit3PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit4PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit5PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit7PropertiesChange(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    g_ProjectDir  : string;
    g_PostCode    : string;
    g_PostNumbers : string;
    g_IsModify  : Boolean;
    g_TableName : string; // = 'concrete';
    g_Handte : THandle;
    procedure GetUnitList(lpComBox : TcxComboBox ; chType : Integer);
  end;

var
  frmAddConcrete: TfrmAddConcrete;

implementation

uses
   global, uDataModule,ShellAPI;

{$R *.dfm}

procedure TfrmAddConcrete.cxButton1Click(Sender: TObject);
var
  DD1 , DD2 , DD3 , DD4 , DD5 , DD6 : Currency;

begin
  DD1 := Self.cxCurrencyEdit1.Value ;
  DD2 := Self.cxCurrencyEdit2.Value ;
  DD3 := Self.cxCurrencyEdit3.Value ;
  DD4 := Self.cxCurrencyEdit4.Value ;
  DD5 := Self.cxCurrencyEdit5.Value ;

  Self.cxCurrencyEdit7.Value := DD1 + dd2 + DD3 + DD4 +DD5;
end;

procedure TfrmAddConcrete.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.cxLabel29.Caption := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmAddConcrete.cxCurrencyEdit2PropertiesChange(Sender: TObject);
begin
  Self.cxLabel30.Caption := MoneyConvert(Self.cxCurrencyEdit2.Value);
end;

procedure TfrmAddConcrete.cxCurrencyEdit3PropertiesChange(Sender: TObject);
begin
  Self.cxLabel31.Caption := MoneyConvert(Self.cxCurrencyEdit3.Value);
end;

procedure TfrmAddConcrete.cxCurrencyEdit4PropertiesChange(Sender: TObject);
begin
  Self.cxLabel32.Caption := MoneyConvert(Self.cxCurrencyEdit4.Value);
end;

procedure TfrmAddConcrete.cxCurrencyEdit5PropertiesChange(Sender: TObject);
begin
  Self.cxLabel33.Caption := MoneyConvert(Self.cxCurrencyEdit5.Value);
end;

procedure TfrmAddConcrete.cxCurrencyEdit7PropertiesChange(Sender: TObject);
begin
  Self.cxLabel34.Caption := MoneyConvert(Self.cxCurrencyEdit7.Value);
end;

procedure TfrmAddConcrete.FormCreate(Sender: TObject);
begin
  Self.cxLabel29.Caption := '';
  Self.cxLabel30.Caption := '';
  Self.cxLabel31.Caption := '';
  Self.cxLabel32.Caption := '';
  Self.cxLabel33.Caption := '';
  Self.cxLabel34.Caption := '';

  GetUnitList(Self.cxComboBox1,5); //输送方式
  GetUnitList(Self.cxComboBox2,1); //砼强度
  GetUnitList(Self.cxComboBox3,2); //抗渗等级
  GetUnitList(Self.cxComboBox4,3); //附加
  GetUnitList(Self.cxComboBox5,4); //外加剂

end;

procedure TfrmAddConcrete.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13:
    begin
      Self.RzBitBtn3.Click;
    end;
    27:
    begin
      Close;
    end;
  end;
end;

procedure TfrmAddConcrete.GetUnitList(lpComBox : TcxComboBox ; chType : Integer);
var
  s : string;
  
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from unit where chType=' + IntToStr(chType);
    Open;

    if 0 <> RecordCount then
     begin
       lpComBox.Clear;
       while not Eof do
       begin
         s := FieldByName('name').AsString;
         lpComBox.Properties.Items.Add(s);
         Next;
       end;
       lpComBox.ItemIndex := 0;
     end;
  end;
end;  

procedure TfrmAddConcrete.RzBitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmAddConcrete.RzBitBtn4Click(Sender: TObject);
var
  s : string;
  szCode : string;
  szParent : string;
  szDeliver: string;  //传送方式
  szDeliver_UnitPrice : Currency; //传送单价
  szConcrete:string;  //砼强度
  szConcrete_UnitPrice: Currency; //砼单价
  szPermeability : string; //抗渗等级
  szPermeability_Unitprice:Currency; //抗渗单价
  szTechnical : string;
  szStere     : string; //立方米
  szPosition  : string; //浇筑部位
  szUnitPrice : Currency;
  szWagonNumber : string; //车号
  szSign  : string; //签字
  szFile  : string;
  sqlstr : string;
  szRemarks  : string;  //备注
  szPouringTime : string;
  szleaveTime   : string;
  szchAttach    : string;
  szchAttach_UnitPrice : Currency;
  szchAdditive  : string;
  szchAdditive_UnitPrice : Currency;
  szSupplyUnit  : string;
  szProjectName : string;
  szPurchaseCompany : string;

  i : Integer;
  
begin
  //混凝土添加
  szCode := g_PostCode; //g_PostCode
  if Length(szCode) = 0 then
  begin
    Application.MessageBox('合同编号不能为零!',m_title,MB_OK + MB_ICONQUESTION) ;
  end
  else
  begin
      szPurchaseCompany := Self.cxTextEdit7.Text;//购买单位
      szSupplyUnit := Self.cxTextEdit1.Text;  //供应单位
      szProjectName:= Self.cxTextEdit2.Text;  //工程名称
      szDeliver := Self.cxComboBox1.Text; //输送方式
      szDeliver_UnitPrice := Self.cxCurrencyEdit1.Value;

      szConcrete:= Self.cxComboBox2.Text;  //砼强度
      szConcrete_UnitPrice:= Self.cxCurrencyEdit2.Value; //砼单价

      szPermeability := Self.cxComboBox3.Text; //抗渗等级
      szPermeability_Unitprice:= Self.cxCurrencyEdit3.Value; //抗渗单价

      szTechnical   := Self.cxTextEdit3.Text; //其他技术要求
      szStere       := Self.cxCurrencyEdit6.Text; //立方米
      szPosition    := Self.cxTextEdit4.Text; //浇筑部位
      szUnitPrice   := Self.cxCurrencyEdit7.Value; //单价
      szWagonNumber := Self.cxTextEdit5.Text; //车号
      szSign        := Self.cxTextEdit6.Text; //签字
      szRemarks     := Self.cxMemo1.Text; //备注

      szPouringTime := FormatDateTime('HH:mm:ss',Self.cxTimeEdit1.Time);
      szleaveTime   := FormatDateTime('HH:mm:ss',Self.cxTimeEdit2.Time);

      szchAttach    := Self.cxComboBox4.Text; //附加
      szchAttach_UnitPrice := Self.cxCurrencyEdit4.Value;
      szchAdditive  := Self.cxComboBox5.Text; //外加剂
      szchAdditive_UnitPrice := Self.cxCurrencyEdit5.Value;

      if g_IsModify then
      begin //编辑
        sqlstr := Format('Update '+ g_TableName +' set '   +
                                              'Deliver="%s",'   +
                                              'Deliver_UnitPrice=''%m'','   +
                                              'Concrete="%s",'            +
                                              'Concrete_UnitPrice=''%m'','  +
                                              'Permeability="%s",'        +
                                              'Permeability_Unitprice=''%m'','+
                                              'Technical="%s",'     +
                                              'Stere="%s",'         +
                                              'chPosition="%s",'       +
                                              'UnitPrice="%m",'     +
                                              'WagonNumber="%s",'   +
                                              'Sign="%s",'       +
                                              'remarks="%s",'    +
                                              'pouringTime="%s",'+
                                              'leaveTime="%s",'  +
                                              'chDate="%s",'     +
                                              'chAttach="%s",'   +
                                              'chAttach_UnitPrice=''%m'','+
                                              'chAdditive="%s",'+
                                              'chAdditive_UnitPrice=''%m'','+
                                              'chSupplyUnit="%s",' +
                                              'chProjectName="%s",'+
                                              'PurchaseCompany="%s"'+
                                              ' where Numbers="%s"',
                                              [
                                                szDeliver,
                                                szDeliver_UnitPrice,
                                                szConcrete,
                                                szConcrete_UnitPrice,
                                                szPermeability,
                                                szPermeability_Unitprice,
                                                szTechnical,
                                                szStere,
                                                szPosition,
                                                szUnitPrice,
                                                szWagonNumber,
                                                szSign,
                                                szRemarks,
                                                szPouringTime,
                                                szleaveTime,
                                                FormatDateTime('yyyy-MM-dd',Self.cxDateEdit1.Date),
                                                szchAttach,
                                                szchAttach_UnitPrice,
                                                szchAdditive,
                                                szchAdditive_UnitPrice,
                                                szSupplyUnit,
                                                szProjectName,
                                                szPurchaseCompany,
                                                g_PostNumbers
                                              ]);

        OutputLog(sqlstr);
        with DM.Qry do
        begin
           Close;
           SQL.Clear;
           SQL.Text := sqlstr;
           if ExecSQL = 0 then
           begin

             Application.MessageBox('执行失败!',m_title,MB_OK + MB_ICONQUESTION) ;
           end
           else
           begin
             m_IsModify := True;
             SendMessage(g_Handte,WM_LisView,0,0);
             Application.MessageBox('执行成功!',m_title,MB_OK + MB_ICONQUESTION);
           end;

        end;

      end else
      begin //新增
        sqlstr := Format( 'Insert into '+ g_TableName +' (code,'   +
                                              'parent,'    +
                                              'Numbers,'   +
                                              'chSupplyUnit,' +
                                              'chProjectName,'+
                                              'Deliver,'   +
                                              'Deliver_UnitPrice,'   +
                                              'Concrete,'            +
                                              'Concrete_UnitPrice,'  +
                                              'Permeability,'        +
                                              'Permeability_Unitprice,'+
                                              'Technical,'     +
                                              'Stere,'         +
                                              'chPosition,'       +
                                              'UnitPrice,'     +
                                              'WagonNumber,'   +
                                              'Sign,'       +
                                              'remarks,'    +
                                              'pouringTime,'+
                                              'leaveTime,'  +
                                              'chDate,'     +
                                              'chAttach,'   +
                                              'chAttach_UnitPrice,' +
                                              'chAdditive,' +
                                              'chAdditive_UnitPrice,'+
                                              'PurchaseCompany'      +
                                              ') values ("%s","%s","%s","%s","%s","%s",''%m'',"%s",''%m'',"%s",''%m'',"%s","%s","%s",''%m'',"%s","%s","%s","%s","%s","%s","%s","%m","%s","%m","%s")',[
                                                                      szCode,
                                                                      szParent,
                                                                      g_PostNumbers,
                                                                      szSupplyUnit,
                                                                      szProjectName,
                                                                      szDeliver,
                                                                      szDeliver_UnitPrice,
                                                                      szConcrete,
                                                                      szConcrete_UnitPrice,
                                                                      szPermeability,
                                                                      szPermeability_Unitprice,
                                                                      szTechnical,
                                                                      szStere,
                                                                      szPosition,
                                                                      szUnitPrice,
                                                                      szWagonNumber,
                                                                      szSign,
                                                                      szRemarks,
                                                                      szPouringTime,
                                                                      szleaveTime,
                                                                      FormatDateTime('yyyy-MM-dd',Self.cxDateEdit1.Date),
                                                                      szchAttach,
                                                                      szchAttach_UnitPrice,
                                                                      szchAdditive,
                                                                      szchAdditive_UnitPrice,
                                                                      szPurchaseCompany
                                                                      ]
                                                                      );

         OutputLog(sqlstr);
         with DM.Qry do
         begin
           Close;
           SQL.Clear;
           SQL.Text := sqlstr;
           if ExecSQL = 0 then
           begin
             SendMessage(g_Handte,WM_LisView,0,0);
             Application.MessageBox('执行失败!',m_title,MB_OK + MB_ICONQUESTION) ;
           end
           else
           begin
             m_IsModify := True;
             g_PostNumbers := DM.getDataMaxDate(g_TableName);
             Application.MessageBox('执行成功!',m_title,MB_OK + MB_ICONQUESTION);
           end;

         end;

      end;

  end;

end;

procedure TfrmAddConcrete.RzBitBtn5Click(Sender: TObject);
begin
  //附件目录
  CreateOpenDir(Handle,Concat(g_ProjectDir,'\' + g_PostNumbers),True);
end;

end.
