unit ufrmFlowingWaterAccount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, RzPanel,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxTextEdit, cxDropDownEdit, cxMemo,
  cxCurrencyEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, RzButton, cxContainer,
  Vcl.Menus, Vcl.ComCtrls, dxCore, cxDateUtils, cxMaskEdit, cxCalendar,
  cxButtons, cxGroupBox, Data.Win.ADODB,ufrmBaseController, cxSpinEdit,
  cxCheckBox, dxSkinsdxStatusBarPainter, dxStatusBar, cxLabel,
  cxDBLookupComboBox, cxLookupEdit, cxDBLookupEdit, RzTabs,UtilsTree,FillThrdTree,
  cxRadioGroup, Datasnap.DBClient, frxClass, frxDBSet;

type
  TfrmFlowingWaterAccount = class(TfrmBaseController)
    FlowingGrid: TcxGrid;
    tvGrid: TcxGridDBTableView;
    Col1: TcxGridDBColumn;
    Col3: TcxGridDBColumn;
    Col4: TcxGridDBColumn;
    Col5: TcxGridDBColumn;
    Col6: TcxGridDBColumn;
    Col7: TcxGridDBColumn;
    Col10: TcxGridDBColumn;
    Col8: TcxGridDBColumn;
    Col11: TcxGridDBColumn;
    lv: TcxGridLevel;
    DataMaster: TDataSource;
    Col2: TcxGridDBColumn;
    Col9: TcxGridDBColumn;
    PmRepot: TPopupMenu;
    Execl1: TMenuItem;
    N6: TMenuItem;
    Col0: TcxGridDBColumn;
    Col12: TcxGridDBColumn;
    Col13: TcxGridDBColumn;
    Col14: TcxGridDBColumn;
    dxStatusBar1: TdxStatusBar;
    RzToolbar2: TRzToolbar;
    RzToolButton2: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton14: TRzToolButton;
    RzSpacer5: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzToolButton3: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzSpacer2: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzSpacer4: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzToolButton6: TRzToolButton;
    RzToolButton7: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzPanel1: TRzPanel;
    Lv2: TcxGridLevel;
    tvDetailed: TcxGridDBTableView;
    RzSpacer8: TRzSpacer;
    RzToolButton8: TRzToolButton;
    ADOView: TADOQuery;
    DataView: TDataSource;
    RzSpacer9: TRzSpacer;
    Splitter1: TSplitter;
    RzPanel3: TRzPanel;
    RzSpacer11: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzPanel4: TRzPanel;
    cxComboBox1: TcxComboBox;
    cxLabel15: TcxLabel;
    Label10: TLabel;
    cxDateEdit1: TcxDateEdit;
    Label4: TLabel;
    cxDateEdit2: TcxDateEdit;
    cxLabel4: TcxLabel;
    cxLookupComboBox3: TcxLookupComboBox;
    cxButton1: TcxButton;
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cLeft: TRzPanel;
    cxLabel1: TcxLabel;
    cxComboBox2: TcxComboBox;
    Master: TClientDataSet;
    tvDetailedColumn1: TcxGridDBColumn;
    tvDetailedColumn2: TcxGridDBColumn;
    tvDetailedColumn3: TcxGridDBColumn;
    tvDetailedColumn4: TcxGridDBColumn;
    tvDetailedColumn5: TcxGridDBColumn;
    tvDetailedColumn6: TcxGridDBColumn;
    tvDetailedColumn7: TcxGridDBColumn;
    tvDetailedColumn8: TcxGridDBColumn;
    tvDetailedColumn9: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    frxDBDataset: TfrxDBDataset;
    Fast1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure tvGridEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure qryAfterClose(DataSet: TDataSet);
    procedure tvGridColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Execl1Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure Col13GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure tvGridStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure RzToolButton3Click(Sender: TObject);
    procedure qryAfterOpen(DataSet: TDataSet);
    procedure Col10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure cxLookupComboBox3PropertiesCloseUp(Sender: TObject);
    procedure cxComboBox2PropertiesCloseUp(Sender: TObject);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure tvDetailedColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure N2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure RzToolButton7Click(Sender: TObject);
    procedure Fast1Click(Sender: TObject);
  private
    { Private declarations }
    g_TreeView : TNewUtilsTree;
    dwSQLText  : string;
    g_PostCode : string;
    g_ProjectName : string;
    Prefix : string;
    Suffix : string;
    g_DirFlowingWater : string;
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
    procedure GetRuningAccountData(lpName : string ; lpIndex : Byte);
    function FunSearch(lpSignName : string):Integer;
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  public
    { Public declarations }
    function Requery(lpSQL : string) : string;
    function ComGroupSQL():string;
    function GetViewData():Boolean;
    function SetDetailed(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string):BOOL;
  end;

var
  frmFlowingWaterAccount: TfrmFlowingWaterAccount;
  g_Dir_RuningAccount : string = 'RuningAccount';

implementation

uses
  uDataModule,global,ufrmIsViewGrid,ufrmDetailed,ufrmFlowingFrame,ufunctions,ufrmReport;

{$R *.dfm}

function TfrmFlowingWaterAccount.SetDetailed(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string):BOOL;
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, 'frmBaseRunningAccount' + lpSuffix);
  end;
end;

function TfrmFlowingWaterAccount.GetViewData():Boolean;
begin
  with Self.ADOView do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_DetailTable;
    Open;
    if RecordCount <> 0 then
    begin
      SetDetailed(Self.tvDetailed,0,g_dir_DetailTable);
    end;

  end;

end;

function TfrmFlowingWaterAccount.Requery(lpSQL : string) : string;
begin
  with DM do
  begin
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := lpSQL;
    ADOQuery1.Open;
    Self.Master.Data := DataSetProvider1.Data;
    Self.Master.Open;
  end;
end;

function TfrmFlowingWaterAccount.ComGroupSQL():string;
var
  i : Integer;
  DD1 , DD2 , DD3 , DD4  : string;
  s : string;

begin
  s := '';
  DD1 := cxLookupComboBox3.Text; //项目名称
  DD2 := Self.cxComboBox1.Text;  //制表类型
  DD3 := Self.cxComboBox2.Text;  //数据状态
  if g_ProjectName <> '' then
  begin

    if Length(DD1) <> 0 then  DD1 := ' AND ' + Self.Col6.DataBinding.FieldName + '="' + DD1 + '"';
    if Length(DD2) <> 0 then  DD2 := ' AND ' + Self.Col2.DataBinding.FieldName + '="' + DD2 + '"';

    i := Self.cxComboBox1.ItemIndex;
    if i = -1 then
    begin
       DD4 := '(' + Self.Col3.DataBinding.FieldName + '="' + g_ProjectName +
          '" or ' + Self.Col5.DataBinding.FieldName + '="' + g_ProjectName + '") '
    end else
    begin
      if (i = 0) or (i = 2) then
        DD4 := Self.Col5.DataBinding.FieldName + '="' + g_ProjectName + '"'
      else
        DD4 := Self.Col3.DataBinding.FieldName + '="' + g_ProjectName + '"';
    end;

    if Length(DD3) <> 0 then  //冻结状态
    begin
      case Self.cxComboBox2.ItemIndex of
        0: DD3 := ' AND ' + Self.Col14.DataBinding.FieldName + '=no';
        1: DD3 := ' AND ' + Self.Col14.DataBinding.FieldName + '=yes';
      end;
    end;

    if (DD4 <> '') or (DD1 <> '') or (DD2 <> '') or (DD3<>'') then s := ' WHERE ' + DD4 + DD1 + DD2 + DD3 ;
  end;
  dwSQLText := 'Select * from ' + g_Table_RuningAccount + s;
//  ShowMessage(dwSQLText);
  Requery(dwSQLText);
end;

procedure TfrmFlowingWaterAccount.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
  i : Integer;
  DD1 : string;

begin
  case Message.Msg of
    WM_FrameClose:
    begin

    end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      Self.cxLookupComboBox3.Text := '';
      Self.cxComboBox2.Text := '';
      Self.cxDateEdit1.Date := Date;
      Self.cxDateEdit2.Date := Date;
      Self.cxComboBox1.Text := '';
    //  g_PostCode := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;
      ComGroupSQL;

      GetViewData  ;

    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;
    end;
    WM_Refresh :
    begin
      GetMaintainInfo;
      GetViewData;
      ShowMessage('信息刷新完成!');
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmFlowingWaterAccount.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmFlowingWaterAccount.qryAfterClose(DataSet: TDataSet);
begin
  Self.RzToolButton2.Enabled := False;
  Self.cxLookupComboBox3.Enabled := False;
  Self.cxComboBox1.Enabled := False;
  Self.cxComboBox2.Enabled := False;
  Self.cxDateEdit1.Enabled := False;
  Self.cxDateEdit2.Enabled := False;
  Self.cxRadioButton1.Enabled := False;
  Self.cxRadioButton2.Enabled := False;
  Self.cxButton1.Enabled := False;
end;

procedure TfrmFlowingWaterAccount.qryAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
  Self.RzToolButton2.Enabled := True;
  Self.cxLookupComboBox3.Enabled := True;
  Self.cxComboBox1.Enabled := True;
  Self.cxComboBox2.Enabled := True;
  Self.cxDateEdit1.Enabled := True;
  Self.cxDateEdit2.Enabled := True;
  Self.cxRadioButton1.Enabled := True;
  Self.cxRadioButton2.Enabled := True;
  Self.cxButton1.Enabled := True;
end;

procedure TfrmFlowingWaterAccount.tvDetailedColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetDetailed(Self.tvDetailed,1,g_dir_DetailTable);
end;

procedure TfrmFlowingWaterAccount.tvGridColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvGrid,1,g_Dir_RuningAccount);
end;

procedure TfrmFlowingWaterAccount.tvGridEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
var
  s : string;
  szColName : string;

begin
  if (Key = 13) and (AItem.Index = Self.Col10.Index) then
  begin
    if Self.tvGrid.Controller.FocusedRow.IsLast and IsNewRow then
    begin
      //在最后一行新增
      Self.RzToolButton1.Click;

    end;

  end;

end;

procedure TfrmFlowingWaterAccount.tvGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton2.Click;
  end;

end;

procedure TfrmFlowingWaterAccount.tvGridStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.Col14.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmFlowingWaterAccount.tvGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;
  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.Col12.Summary.FooterFormat := szCapital;
  except
  end;

end;

procedure TfrmFlowingWaterAccount.GetRuningAccountData(lpName : string ; lpIndex : Byte);
begin

  with DM do
  begin
    ADOQuery1.Close;
    if ( Length(Self.cxDateEdit1.Text) <> 0) and (Length( Self.cxDateEdit2.Text ) <> 0) then
    begin
      ADOQuery1.SQL.Text := dwSQLText + lpName  +' and cxDate between :t1 and :t2';
      ADOQuery1.Parameters.ParamByName('t1').Value := Self.cxDateEdit1.Date;  // StrToDate('2016-4-17 00:00:00');
      ADOQuery1.Parameters.ParamByName('t2').Value := Self.cxDateEdit2.Date;
    end;
    ADOQuery1.Open;
    Self.Master.Data := DataSetProvider1.Data;
    Self.Master.Open;
  end;
  GetViewData  ;
end;

procedure TfrmFlowingWaterAccount.N1Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton2.Click;
end;

procedure TfrmFlowingWaterAccount.N2Click(Sender: TObject);
var
  szNumbers : string;
begin
  inherited;
  szNumbers := VarToStr( Self.Col0.EditValue );
  CreateOpenDir(Handle,Concat(g_DirFlowingWater,'\' + szNumbers),True);
end;

procedure TfrmFlowingWaterAccount.N6Click(Sender: TObject);
begin
  inherited;

  DM.BasePrinterLink1.Component := Self.FlowingGrid;
  DM.BasePrinter.Preview(True,nil);

end;

procedure TfrmFlowingWaterAccount.Col10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szColName : string;

begin
  inherited;
  if DisplayValue <> null then
  begin
    szColName := Self.Col10.DataBinding.FieldName;
    Self.Col10.EditValue := DisplayValue;
    Self.Col12.EditValue := MoneyConvert(DisplayValue);
  end;
end;

procedure TfrmFlowingWaterAccount.Col13GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmFlowingWaterAccount.cxButton1Click(Sender: TObject);
var
  szColName : string;
  sqltext : string;

begin
  inherited;
  if Self.cxRadioButton1.Checked then
  begin
    //按名称查询
    ComGroupSQL;
  end else
  if Self.cxRadioButton2.Checked then
  begin
    //按日期组合查询
    GetRuningAccountData('',1);
  end;
end;

procedure TfrmFlowingWaterAccount.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  s : string;
begin
  inherited;
  ComGroupSQL;
  {
  s := dwSQL + ' Where ' + Self.Col2.DataBinding.FieldName + '="' + Self.cxComboBox1.Text + '"';
  Requery(s);
  }
end;

procedure TfrmFlowingWaterAccount.cxComboBox2PropertiesCloseUp(Sender: TObject);
var
  s : string;
begin
  inherited;
  ComGroupSQL;
  {
  case Self.cxComboBox2.ItemIndex of
    0: s := dwSQL + ' Where ' + Self.Col14.DataBinding.FieldName + '=no';
    1: s := dwSQL + ' Where ' + Self.Col14.DataBinding.FieldName + '=yes';
  end;
  Requery(s);
  }
end;

procedure TfrmFlowingWaterAccount.cxLookupComboBox3PropertiesCloseUp(
  Sender: TObject);
var
  szRowIndex : Integer;
  szColName , s : string;
  szAND : string;

begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szColName := DataController.Values[szRowIndex,0];
      if szColName <> null then ComGroupSQL;
      {
      //工程名称
      if g_ProjectName <> '' then
      begin
        szAND := ' AND ';
      end else
      begin
        szAND := ' Where ';
      end;
      s := dwSQLText + szAND + Self.Col6.DataBinding.FieldName + '="' + szColName + '"';
      ShowMessage(s);
      Requery(s);
      }
    end;
  end;
end;

procedure TfrmFlowingWaterAccount.Execl1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.FlowingGrid,'流水帐明细表') ;
end;

procedure TfrmFlowingWaterAccount.Fast1Click(Sender: TObject);
  procedure RePinert(Sender: TObject;lpReportName : string ; lpIndex : Byte = 0);
  var
    lvReport : TfrmReport;
  begin
    inherited;
    lvReport := TfrmReport.Create(Application);
    try
      {
      with lvReport.dwDictionary do
      begin
        Clear;
        Add('甲方', cxDBTextEdit1.Text);
        Add('甲方电话', cxDBSpinEdit2.Text);
        Add('设计师',cxDBButtonEdit1.Text);
        Add('工程地址',Self.cxDBTextEdit6.Text);
        Add('工程面积',cxDBSpinEdit1.Text);
        Add('户型',cxDBTextEdit4.Text);
        Add('合同备注',cxDBMemo2.Text);
        Add('客户要求',cxDBMemo1.Text);
        Add('公司名称',cxDBButtonEdit5.Text);
        Add('公司电话',cxDBSpinEdit8.Text);
        Add('工程类型',cxDBComboBox1.Text);
        Add('Title',lpReportName);
      end;
      }
      lvReport.frxDBDataset.DataSource := DataMaster;
      lvReport.frxDBDataset.UserName   := 'DataMaster';
      lvReport.dwReportName := lpReportName;  //主材报价
      lvReport.dwReportTitle:= lpReportName; //报表标题
      if lpIndex = 1 then
      begin
        lvReport.Show;
        lvReport.Hide;
        lvReport.dwIsReportTitle := False;
        lvReport.cxButton1.Click;
        lvReport.Close;
      end else
      begin
        lvReport.ShowModal;
      end;
    finally
    end;
  end;
begin
  inherited;
  RePinert( Sender , '流水帐明细表',0);
end;

procedure TfrmFlowingWaterAccount.FormActivate(Sender: TObject);
var
  ChildFrame : TfrmFlowingFrame;
  s : string;
  i : Integer;
begin
  inherited;
  Prefix := 'LS-';
  Suffix := '10010';
  ChildFrame := TfrmFlowingFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.dwTableName:= g_Table_CompanyTree;
  //ChildFrame.IsSystem := True;
  ChildFrame.Parent := Self.cLeft;
  ChildFrame.FrameClick(Self);

  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;

  SetcxGrid(Self.tvGrid,0,g_Dir_RuningAccount);
end;

procedure TfrmFlowingWaterAccount.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmFlowingWaterAccount.RzToolButton2Click(Sender: TObject);
var
  i : Integer;
  szName : string;
  szIndex: Integer;
  Id : string;
  dir , str: string;
  s : string;

begin

  inherited;
  if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin

      with Self.tvGrid.DataController.DataSource.DataSet do
      begin
        if not IsEmpty then
        begin

          for I := 0 to Self.tvGrid.Controller.SelectedRowCount -1 do
          begin
            szName  := Self.Col0.DataBinding.FieldName;
            szIndex := Self.tvGrid.GetColumnByFieldName(szName).Index;
            Id := Self.tvGrid.Controller.SelectedRows[i].Values[szIndex];

            dir := ConcatEnclosure( Concat(g_Resources ,  '\' + g_dir_BranchMoney) , id);
            if DirectoryExists(dir) then  DeleteDirectory(dir);

            if Length(str) = 0 then
              str := Id
            else
              str := str + '","' + Id;
          end;

          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'delete * from ' +  g_Table_RuningAccount + ' where ' + 'numbers' +' in("' + str + '")';
            ExecSQL;
            SQL.Clear;
            SQL.Text := 'delete * from ' +  g_Table_DetailTable + ' where ' + 'parent' +' in("' + str + '")';
            ExecSQL;
          end;
        //  Requery( dwSQLText );
          ComGroupSQL;
        end;

      end;

  end;


end;

procedure TfrmFlowingWaterAccount.RzToolButton3Click(Sender: TObject);
begin
  inherited;
//  Self.qry.UpdateBatch(arAll);
//  Self.qry.Requery();
//  ShowMessage('保存完成!!');

end;

procedure TfrmFlowingWaterAccount.RzToolButton4Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
//表格设置
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_Dir_RuningAccount;
    Child.ShowModal;
    SetcxGrid(Self.tvGrid,0,g_Dir_RuningAccount);
  finally
    Child.Free;
  end;
end;

procedure TfrmFlowingWaterAccount.RzToolButton5Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmFlowingWaterAccount.RzToolButton7Click(Sender: TObject);
begin
  inherited;
  g_ProjectName := '';  //往来单位
  cxLookupComboBox3.Text := ''; //项目名称
  Self.cxComboBox1.Text  := '';  //制表类型
  Self.cxComboBox2.Text  := '';  //数据状态
//  Requery( 'Select * from ' + g_Table_RuningAccount );
  ComGroupSQL;
end;

function TfrmFlowingWaterAccount.FunSearch(lpSignName : string):Integer;
var
  DD1 , DD2 , DD3 , DD4 : string;

begin
  inherited;
  Self.RzPanel6.Caption := lpSignName;
  if 0 <> Length(lpSignName) then
  begin
    DD1 := Self.cxComboBox1.Text; //制表类型
    if DD1 = '' then
    begin
      Self.cxComboBox1.DroppedDown := True;
      ShowMessage('可以选择一个制表类别进行精准查询');
    end;

  end else
  begin
    Application.MessageBox('查询条件往来单位为空无法正常查询','查询',mb_ok);

  //  ShowMessage('可以选择一个制表类别进行精准查询');
  end;


end;

end.
