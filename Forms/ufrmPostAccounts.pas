unit ufrmPostAccounts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, RzEdit, ComCtrls, RzButton, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,ufrmBaseController;

type
  TfrmPostAccounts = class(TfrmBaseController)
    GroupBox1: TGroupBox;
    Label12: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    RzMemo1: TRzMemo;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    dtp1: TDateTimePicker;
    cxLookupComboBox1: TcxLookupComboBox;
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_ProjectDir : string;
    g_IsModify : Boolean;
    g_PostCode : string;
    g_TableName: string;
    g_TableType: Byte;
    g_Prefix   : string;
    g_Suffix   : string;
  end;

var
  frmPostAccounts: TfrmPostAccounts;

implementation

uses
   uDataModule,global;

{$R *.dfm}

procedure TfrmPostAccounts.FormActivate(Sender: TObject);
begin
  if not g_IsModify then g_PostCode := g_Prefix + g_PostCode + g_Suffix;
end;

procedure TfrmPostAccounts.FormCreate(Sender: TObject);
begin
inherited;
//
end;

procedure TfrmPostAccounts.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13:
    begin
      if not Self.cxLookupComboBox1.DroppedDown then
      begin
        Self.RzBitBtn1.Click;
      end;

    end;
    27:
    begin
      Close;
    end;
  end;
end;

procedure TfrmPostAccounts.FormShow(Sender: TObject);
begin
  if not g_IsModify then
  begin
    Self.dtp1.Date := Date;
  end;
end;

procedure TfrmPostAccounts.RzBitBtn1Click(Sender: TObject);
var
  szName : string;
  Input_time : string;
  End_time  : string;
  sqlstr    : string;
  szRemarks : string;
  parent : Integer;
  i : Integer;
begin
  szName := Self.cxLookupComboBox1.Text;
  if Length(szName) = 0 then
  begin
    Application.MessageBox('请输入帐目名称!',m_title,MB_OK + MB_ICONQUESTION)
  end
  else
  begin
    szRemarks  := Self.RzMemo1.Text;
    Input_time := FormatDateTime('yyyy-MM-dd',Self.dtp1.Date);
    End_time   := FormatDateTime('yyyy-MM-dd',Now);

    if g_IsModify then
    begin
       //编辑
      sqlstr := 'Update '+ g_TableName + ' set Name="' + szName +'",Input_time="'+
                                               Input_time +'",End_time="'+
                                               End_time + '",Remarks="'+
                                               szRemarks +'" where code ="' + g_PostCode +'"';

      OutputLog(sqlstr);
      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := sqlstr;
        if ExecSQL <> 0 then
        begin
           Application.MessageBox('帐目编辑成功!',m_title,MB_OK + MB_ICONQUESTION);
           Close;
        end
        else
           Application.MessageBox('帐目编辑失败!',m_title,MB_OK + MB_ICONQUESTION)

      end;

    end else
    begin
      //新增
      i := DM.SelectCode(g_TableName,g_PostCode);
      if i = 0 then
      begin
          parent := DM.getLastId(g_TableName);
          if parent > 0 then
          begin
             case g_TableType of
               0:
               begin
                 sqlstr := 'Insert into ' + g_TableName
                                       + '(parent,Code,Remarks,Input_time,End_time,PID,Name) values('
                                       + IntToStr(parent)
                                       + ',"' + g_PostCode
                                       + '","'+ szRemarks
                                       +'","' + Input_time
                                       +'","' + End_time + '",0,"'+ szName +'")';

               end;
               1:
               begin
                 sqlstr := 'Insert into ' + g_TableName
                                       + '(parent,Code,Remarks,Input_time,End_time,PID,Name,PatType) values('
                                       + IntToStr(parent)
                                       + ',"' + g_PostCode
                                       + '","'+ szRemarks
                                       +'","' + Input_time
                                       +'","' + End_time + '",0,"'+ szName +'",'+ IntToStr(g_pacttype) +' )';

               end;

             end;
             
             with DM.Qry do
             begin
               Close;
               SQL.Clear;
               SQL.Text := sqlstr;
               if ExecSQL <> 0 then
               begin
                  g_PostCode := DM.getDataMaxDate(g_TableName);
                  Application.MessageBox('帐目新增成功!',m_title,MB_OK + MB_ICONQUESTION);
               end
               else
                  Application.MessageBox('帐目新增失败!',m_title,MB_OK + MB_ICONQUESTION)

             end;

          end;
      end else
      begin
        Application.MessageBox('编号重复新增失败!',m_title,MB_OK + MB_ICONQUESTION);
      end;  

    end;

  end;
end;

procedure TfrmPostAccounts.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

end.
