unit ufrmReport;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxDesgn, frxDBSet,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxContainer, cxEdit, cxListBox,
  Vcl.ExtCtrls, RzPanel, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxLabel, cxTextEdit,
  frxBarcode,Generics.Collections, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxGroupBox, frxChart, dxmdaset, RzButton,UnitADO,
  Datasnap.DBClient, frxOLE, frxGaugePanel, frxRich, frxGZip, frxGradient,
  frxChBox, frxCross, frxCellularTextObject, frxMap, frxTableObject;

type
  TfrmReport = class(TForm)
    frxDBDataset: TfrxDBDataset;
    frxDesigner: TfrxDesigner;
    frxReport: TfrxReport;
    RzPanel1: TRzPanel;
    RzPanel2: TRzPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    RzPanel3: TRzPanel;
    cxLabel1: TcxLabel;
    ReportFilePath: TcxTextEdit;
    cxButton6: TcxButton;
    frxBarCode: TfrxBarCodeObject;
    cxGroupBox1: TcxGroupBox;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    lv: TcxGridLevel;
    frxChart: TfrxChartObject;
    ds: TDataSource;
    RzToolbar4: TRzToolbar;
    RzSpacer26: TRzSpacer;
    RzToolButton24: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    dsReport: TClientDataSet;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    frxRichObject1: TfrxRichObject;
    frxOLEObject1: TfrxOLEObject;
    frxMapObject1: TfrxMapObject;
    frxReportCellularTextObject1: TfrxReportCellularTextObject;
    frxCrossObject1: TfrxCrossObject;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    frxGradientObject1: TfrxGradientObject;
    frxGZipCompressor1: TfrxGZipCompressor;
    frxReportTableObject1: TfrxReportTableObject;
    procedure frxReportGetValue(const VarName: string; var Value: Variant);
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    function frxReportUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure tvViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvViewDblClick(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure cxButton6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    dwADO : TADO;
  public
    { Public declarations }
    dwReportPath : string;
    dwReportFile : string;
    dwReportName : string;
    dwReportTitle: string;
    dwDictionary : TDictionary<String,String>;
    dwIsReportTitle : Boolean;
    dwIsAutoHeight    : Boolean;
    dwRecordCount : Integer;
  end;

var
  frmReport: TfrmReport;

implementation

uses
   uDataModule,global,Winapi.ShellAPI,Vcl.Printers , ufrmStyleManage;

{$R *.dfm}

{
//自定义纸张
  for i := 0 to fr.Pages.Count - 1 do
  begin
    if 横向 then
      fr.Pages[i].ChangePaper(256, Width, Height, -1, poLandscape)
    else
      fr.Pages[i].ChangePaper(256, Width, Height, -1, poPortrait);
    //Landscape --横向打印，Portrait -- 纵向打印
  end;
}

function IsFileExists(lpFilePath : string):Boolean;
begin
  Result := True;
  if not FileExists(  lpFilePath ) then
  begin
    Result := False;
    Application.MessageBox('无法找到报表文件....','提示',MB_OK + MB_ICONERROR);
  end;
end;

procedure TfrmReport.cxButton1Click(Sender: TObject);
var
  lvPath : string;
  lvPage: TfrxReportPage;
begin
  with frxReport do
  begin
    Clear;
    lvPath := dwReportPath + dwReportFile + '.fr3';
    if IsFileExists(lvPath) then
    begin
      LoadFromFile(lvPath);
      if Sender = Self.cxButton1 then
      begin
        //直接打印
      //  PrintOptions.Printer   := Edit1.Text; //指定打印机
      //  PrintOptions.ShowDialog:= false;
        PrepareReport;
        Print;
      end else
      if Sender = Self.cxButton2 then
      begin
        //报表设计
        DesignReport();
      end else
      if Sender = Self.cxButton3 then
      begin
        //打印预览
        ShowReport(True);
      end;

    end;

  end;
end;

procedure TfrmReport.cxButton6Click(Sender: TObject);
var
  strFileName : string;
begin
  strFileName := Self.ReportFilePath.Text;
  if IsFileExists(strFileName) then
    ShellExecute(0, nil, PChar('explorer.exe'),PChar('/e, ' + '/select,' + strFileName), nil, SW_NORMAL)
end;

procedure TfrmReport.FormCreate(Sender: TObject);
begin
  dwDictionary := TDictionary<String,String>.Create();
  //声明函数
  frxReport.AddFunction('function MoneyConvert(mmje:real):WideString;','人民币转换','人民币中文大写转换'); //Myfunction1为名


  frxReport.AddFunction('function ADD(ParamsA , ParamsB : Real ):Real;','聚会函数','两个值进行相加');
  dwReportPath := ExtractFilePath(ParamStr(0)) + 'Report\';

end;

procedure TfrmReport.FormShow(Sender: TObject);
var
  i: Integer;
  s: string;

begin
//标准单据     折扣单据     统计数量单据  租赁单据
//标准模板=进销管理-标准模板.fr3
//折扣单据=进销管理-折扣单据.fr3
//统计数量单据=进销管理-统计数量单据.fr3
  {
  with Self.ReportData do
  begin
    Active := False;
    Active := True;
    Append;
    FieldByName(Self.ReportDataTemplet.FieldName).Value    := '标准模板';
    FieldByName(Self.ReportDataTempletName.FieldName).Value:= '进销管理-标准模板';
    Post;
    //----
    Append;
    FieldByName(Self.ReportDataTemplet.FieldName).Value    := '折扣单据';
    FieldByName(Self.ReportDataTempletName.FieldName).Value:= '进销管理-折扣单据';
    Post;
    //----
    Append;
    FieldByName(Self.ReportDataTemplet.FieldName).Value    := '统计数量单据';
    FieldByName(Self.ReportDataTempletName.FieldName).Value:= '进销管理-统计数量单据';
    Post;
    //----
    Append;
    FieldByName(Self.ReportDataTemplet.FieldName).Value    := '租赁单据';
    FieldByName(Self.ReportDataTempletName.FieldName).Value:= '进销管理-租赁单据';
    Post;
    //----
    Append;
    FieldByName(Self.ReportDataTemplet.FieldName).Value    := '条码';
    FieldByName(Self.ReportDataTempletName.FieldName).Value:= 'BarCode';
    Post;
    //----
    Append;
    FieldByName(Self.ReportDataTemplet.FieldName).Value     := '小票';
    FieldByName(Self.ReportDataTempletName.FieldName).Value := 'SmallTicket';
    Post;
  end;
  }
  s := 'Select * from sk_SysReportList';
  dwADO.OpenSQL(Self.dsReport,s);

  Self.ReportFilePath.Text := dwReportPath;
  with Self.tvView do
  begin
    Controller.FocusedRowIndex := 0;
    for i := 0 to ViewData.RowCount-1 do
    begin
      s := ViewData.Rows[i].Values[tvViewColumn3.Index];
      if s =  dwReportName then
      begin
         Controller.FocusedRowIndex := i;
         dwReportFile := Self.tvViewColumn2.EditValue;
         Self.ReportFilePath.Text := dwReportPath + dwReportFile + '.fr3';
         Break;
      end;
    end;
  end;

end;

procedure TfrmReport.frxReportGetValue(const VarName: string;
  var Value: Variant);
begin
  if dwDictionary.ContainsKey(VarName) then Value := dwDictionary[VarName];

// ShowMessage(VarName + ' ' + Value);
end;

function TfrmReport.frxReportUserFunction(const MethodName: string;
  var Params: Variant): Variant;
  function Sum(ParamsA , ParamsB : Real ):Real;
  begin
    Result := ParamsA + ParamsB;
  end;
begin
  //报表使用函数
  if UpperCase(MethodName) = UpperCase('MoneyConvert') then Result := MoneyConvert(Params[0]);
  if UpperCase(MethodName) = UpperCase('ADD') then  Result := Sum(Params[0],Params[1]);
  
end;

procedure TfrmReport.RzToolButton1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmStyleManage,frmStyleManage);
  try
    frmStyleManage.ShowModal;
  finally
    frmStyleManage.Free;
  end;
end;

procedure TfrmReport.tvViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmReport.tvViewDblClick(Sender: TObject);
begin
  dwReportFile := Self.tvViewColumn2.EditValue;
  Self.ReportFilePath.Text := dwReportPath + dwReportFile +  '.fr3';
end;

procedure TfrmReport.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  AAllow := False;
end;

end.
