unit ufrmCustomerSummaryU;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,ufrmBaseController, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxClasses, cxCustomData,
  cxStyles, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  cxCustomPivotGrid, cxDBPivotGrid, Vcl.Menus, cxScheduler, cxSchedulerStorage,
  cxSchedulerCustomControls, cxSchedulerCustomResourceView, cxSchedulerDayView,
  cxSchedulerDateNavigator, cxSchedulerHolidays, cxSchedulerTimeGridView,
  cxSchedulerUtils, cxSchedulerWeekView, cxSchedulerYearView,
  cxSchedulerGanttView, cxSchedulerRecurrence, cxSchedulerTreeListBrowser,
  cxSchedulerRibbonStyleEventEditor, dxSkinscxSchedulerPainter,
  dxBreadcrumbEdit, dxDBBreadcrumbEdit, cxInplaceContainer, cxVGrid, cxDBVGrid,
  Data.DB, dxmdaset, cxCurrencyEdit, cxTextEdit;

type
  TfrmCustomerSummaryU = class(TfrmBaseController)
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow2: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow3: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow4: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow8: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow9: TcxDBEditorRow;
    dxGroupname: TdxMemData;
    ds: TDataSource;
    dxGroupnameASmall: TCurrencyField;
    dxGroupnameALarge: TStringField;
    dxGroupnameBSmall: TCurrencyField;
    dxGroupnameBLarge: TStringField;
    dxGroupnameCSmall: TCurrencyField;
    dxGroupnameCLarge: TStringField;
    dxGroupnameManpowerSmall: TCurrencyField;
    dxGroupnameColligate: TStringField;
    dxGroupnameExtras: TStringField;
    cxDBVerticalGrid1DBEditorRow10: TcxDBEditorRow;
    dxGroupnameTaxGoldFront: TCurrencyField;
    cxDBVerticalGrid1DBEditorRow11: TcxDBEditorRow;
    dxGroupnameDirectTotal: TCurrencyField;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCustomerSummaryU: TfrmCustomerSummaryU;

implementation

{$R *.dfm}

procedure TfrmCustomerSummaryU.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
end;

end.
