unit ufrmPayAddAccounts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RzEdit, ComCtrls, RzButton, Mask, RzBckgnd, ExtCtrls,
  RzTabs, RzPanel;

type
  TfrmAdditive = class(TForm)
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    Panel9: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label10: TLabel;
    Label21: TLabel;
    Label9: TLabel;
    Label25: TLabel;
    ComboBox2: TComboBox;
    RzEdit12: TRzEdit;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn17: TRzBitBtn;
    RzBitBtn6: TRzBitBtn;
    RzEdit3: TRzEdit;
    RzEdit2: TRzEdit;
    RzEdit5: TRzEdit;
    RzEdit4: TRzEdit;
    AddEndDate: TDateTimePicker;
    AddStartDate: TDateTimePicker;
    RzMemo2: TRzMemo;
    TabSheet2: TRzTabSheet;
    Panel2: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label26: TLabel;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn4: TRzBitBtn;
    RzEdit8: TRzEdit;
    RzEdit10: TRzEdit;
    RzEdit13: TRzEdit;
    RzEdit14: TRzEdit;
    ComboBox1: TComboBox;
    RzEdit11: TRzEdit;
    ModifyStartDate: TDateTimePicker;
    ModifyEndDate: TDateTimePicker;
    RzMemo1: TRzMemo;
    RzPanel3: TRzPanel;
    procedure FormCreate(Sender: TObject);
    procedure RzBitBtn4Click(Sender: TObject);
    procedure RzBitBtn17Click(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RzEdit13Change(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    g_PostCode : string;
    g_ProjectDir  : string;
    g_PostNumbers : string;
    g_TableName   : string;
  end;

var
  frmAdditive: TfrmAdditive;
//  g_TableName : string = 'expenditure';

implementation

uses
  global,ufrmBranch,uDataModule;

{$R *.dfm}

procedure TfrmAdditive.FormActivate(Sender: TObject);
var
  i : Integer;
begin

  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin
      g_PostNumbers := DM.getDataMaxDate(g_TableName);
      Self.RzEdit2.Text := g_PostNumbers;
    end;
    1:
    begin
      g_PostCode := g_global_code;

      Self.RzEdit14.Text := g_numbers ; //单号
      g_PostNumbers := g_Numbers;
      Self.RzEdit13.Text := FloatToStr(g_Price);//单价

      Self.RzEdit11.Text := FloatToStr( g_num ) ;//数量

      Self.RzEdit10.Text := g_caption; //名称
      Self.RzEdit8.Text  := g_content;

      ModifyStartDate.DateTime  := g_pay_time;
      ModifyEndDate.DateTime := g_End_time;

      Self.RzMemo1.Text  := g_remarks;
      i := Self.ComboBox1.Items.IndexOf( g_Units);
      if i >= 0 then Self.ComboBox1.ItemIndex := i;

    end;    
  end;

end;

procedure TfrmAdditive.FormCreate(Sender: TObject);
var
  i : Integer;
  
begin
  Self.Position := poScreenCenter;
  for i := 0 to Self.RzPageControl1.PageCount - 1 do Self.RzPageControl1.Pages[i].TabVisible := False;//隐藏
  AddStartDate.Date := Date;
  AddEndDate.Date   := Date;
  
  ModifyStartDate.Date := Date;
  ModifyEndDate.Date   := Date;
  
end;

procedure TfrmAdditive.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
end;

procedure TfrmAdditive.FormShow(Sender: TObject);
var
  s : string;
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from unit where chType=0';
    Open;

    if 0 <> RecordCount then
     begin
       Self.ComboBox1.Clear;
       Self.ComboBox2.Clear;

       while not Eof do
       begin
         s := FieldByName('name').AsString;
         Self.ComboBox1.Items.Add( s );
         Self.ComboBox2.Items.Add( s );
         Next;
       end;
       
     end;

  end;
end;

procedure TfrmAdditive.RzBitBtn17Click(Sender: TObject);
var
  s : string;
  szCode : string;
  szPayee: string;

  szCaption: string;
  szContent : string;
  szPrice: Currency;
  szNum  : Single;
  szRemarks: string;
  szUnit   : string;
  szPic    : string;
  szPhoto  : string;
  szPayType: Integer;
  szTruck  : Integer;
  szDate   : TDateTime;
  i : Integer;
  pData : PPaytype;
  parent: string;
  
begin
  szCode := g_PostCode;
  if Length(szCode) = 0 then
  begin
    Application.MessageBox('请选择合同',m_title,MB_OK + MB_ICONHAND);
    Close;
  end
  else
  begin
    if DM.IsNumbers(g_TableName,g_PostNumbers) then
    begin
        Application.MessageBox('编号已存在',m_title,MB_OK + MB_ICONHAND);
        Exit;
    end else
    begin
      szUnit := Self.ComboBox2.Text;
      if Length(szUnit) = 0 then
      begin
        Application.MessageBox('请选择单位',m_title,MB_OK + MB_ICONHAND);
        Self.ComboBox2.DroppedDown := True;
      end
      else
      begin
        szPrice := StrToFloatDef( Self.RzEdit5.Text ,-0) ;
        
        if szPrice = 0 then
        begin
          Application.MessageBox('请输入单价',m_title,MB_OK + MB_ICONHAND);
        end
        else
        begin
           szNum   := StrToFloatDef(Self.RzEdit4.Text,0);
           if szNum = 0 then
           begin
             Application.MessageBox('请输入数量',m_title,MB_OK + MB_ICONHAND);
           end
           else
           begin

               szCaption := Self.RzEdit3.Text;  //名称
               szContent := Self.RzEdit12.Text; //内容
               szRemarks := Self.RzMemo2.Text;  //备注
               sztruck   := 1;
               szPic     := '';
               szDate    := AddStartDate.DateTime;
               parent    := ''; //主合同

              if DM.AddPayInfo(g_TableName,
                                    szCode,
                                    parent,
                                    szCaption,
                                    szContent,
                                    szUnit,
                                    szPhoto,
                                    szRemarks,
                                    szDate,
                                    AddEndDate.DateTime,
                                    szPrice,
                                    szTruck,
                                    g_PostNumbers,
                                    szNum,
                                    g_pacttype) <> 0 then
              begin
                Self.FormActivate(Sender);
                Application.MessageBox('添加成功',m_title,MB_OK + MB_ICONQUESTION );
              end
              else
              begin
                Application.MessageBox('添加失败',m_title,MB_OK + MB_ICONQUESTION );
              end;
           end;

        end;

      end;

    end;

  end;

end;


procedure TfrmAdditive.RzBitBtn1Click(Sender: TObject);
begin
  CreateOpenDir(Handle,Concat(g_ProjectDir,'\' + g_PostNumbers),True);
end;

procedure TfrmAdditive.RzBitBtn3Click(Sender: TObject);
var
  s : string;
  szCode : string;
  szPayee: string;

  szNumber : string;
  szCaption: string;
  szContent : string;
  szPrice: Currency;
  szNum  : Single;
  szRemarks: string;
  szUnit   : string;
  szPic    : string;
  szPhoto  : string;
  szPayType: Integer;
  sztruck  : Integer;
  i : Integer;
  pData : PPaytype;
  szDate : TDateTime;
  
begin
  szCode := g_PostCode;
  if Length(szCode) = 0 then
  begin
    Application.MessageBox('请选择合同',m_title,MB_OK + MB_ICONHAND);
    Close;
  end
  else
  begin
    szNumber := Self.RzEdit14.Text;
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'Select *from '+ g_TableName +' where numbers="' + szNumber +'"';
      Open;
      if RecordCount = 0 then
      begin
        Application.MessageBox('修改编号不存在',m_title,MB_OK + MB_ICONHAND);
        Exit;
      end;
    end;
    
    if szNumber = '' then
    begin
      Self.RzEdit2.SetFocus;
      Application.MessageBox('请输入编号',m_title,MB_OK + MB_ICONHAND);
    end
    else
    begin

      if Self.ComboBox1.ItemIndex < 0 then
      begin
        Application.MessageBox('请选择单位',m_title,MB_OK + MB_ICONHAND);
        Self.ComboBox1.DroppedDown := True;
      end
      else
      begin
        szUnit := Self.ComboBox1.Text;
        
        szPrice := StrToFloatDef( Self.RzEdit13.Text ,-0);
        if szPrice = 0 then
        begin
          Application.MessageBox('请输入单价',m_title,MB_OK + MB_ICONHAND);
        end
        else
        begin
           szNum := StrToFloatDef(Self.RzEdit11.Text,0);
           if szNum = 0 then
           begin
             Application.MessageBox('请输入数量',m_title,MB_OK + MB_ICONHAND);
           end
           else
           begin
             sztruck   := 1;
             szCaption := Self.RzEdit10.Text;  //名称
             szContent := Self.RzEdit8.Text; //内容
             szRemarks := Self.RzMemo1.Text;  //备注
             szDate    := ModifyStartDate.DateTime;
             szPic := '';

              if DM.UpdatePayInfo(g_TableName,
                                  szCaption,
                                  szContent,
                                  szUnit,
                                  szPhoto,
                                  szRemarks,
                                  szDate,
                                  ModifyStartDate.DateTime,
                                  szPrice,
                                  sztruck,
                                  szNumber,
                                  szNum,
                                  g_code
                                  ) <> 0 then
              begin
                 m_IsModify := True;
                  Application.MessageBox('修改成功',m_title,MB_OK + MB_ICONQUESTION);
                  Close;
              end
              else
              begin
                Application.MessageBox('修改失败',m_title,MB_OK + MB_ICONQUESTION);
              end;
              
           end;

        end;

      end;

    end;
       
  end;

  
end;


procedure TfrmAdditive.RzBitBtn4Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmAdditive.RzEdit13Change(Sender: TObject);
begin
  if Sender = RzEdit5 then
  begin
    Self.Label9.Caption := MoneyConvert( StrToFloatDef( Self.RzEdit5.Text,0 ) );
  end;

  if Sender = RzEdit13 then
  begin
    Self.Label12.Caption := MoneyConvert( StrToFloatDef( Self.RzEdit13.Text,0 ) );
  end;
end;

end.
