object frmCalc: TfrmCalc
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #22810#21151#33021#35745#31639#22120
  ClientHeight = 363
  ClientWidth = 645
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object cxMemo1: TcxMemo
    Left = 8
    Top = 8
    Lines.Strings = (
      #35745#31639#34920#36798#24335)
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 0
    Height = 113
    Width = 629
  end
  object cxTextEdit1: TcxTextEdit
    Left = 64
    Top = 127
    TabOrder = 1
    Width = 321
  end
  object cxButton1: TcxButton
    Left = 545
    Top = 127
    Width = 45
    Height = 24
    Caption = #35745#31639
    TabOrder = 2
    OnClick = cxButton1Click
  end
  object cxButton2: TcxButton
    Left = 593
    Top = 127
    Width = 45
    Height = 24
    Caption = #28165#38500
    TabOrder = 3
  end
  object cxLabel1: TcxLabel
    Left = 8
    Top = 127
    Caption = #35745#31639#32467#26524':'
  end
  object cb_decimal: TComboBox
    Left = 460
    Top = 127
    Width = 79
    Height = 24
    Style = csDropDownList
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Monospac821 BT'
    Font.Style = []
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    ParentFont = False
    TabOrder = 5
    Items.Strings = (
      #26080#23567#25968
      '1'#20301#23567#25968
      '2'#20301#23567#25968
      '3'#20301#23567#25968
      '4'#20301#23567#25968
      '5'#20301#23567#25968
      '6'#20301#23567#25968
      '7'#20301#23567#25968
      '8'#20301#23567#25968
      #20934#30830#20540)
  end
  object cxLabel2: TcxLabel
    Left = 398
    Top = 127
    Caption = #20445#30041#23567#25968':'
  end
  object cxListBox1: TcxListBox
    Left = 8
    Top = 154
    Width = 629
    Height = 199
    ItemHeight = 16
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 7
  end
end
