object frmWorkType: TfrmWorkType
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #24037#31181
  ClientHeight = 457
  ClientWidth = 304
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object RzPanel7: TRzPanel
    Left = 0
    Top = 0
    Width = 304
    Height = 23
    Align = alTop
    Alignment = taLeftJustify
    BorderOuter = fsNone
    Caption = #24037#31181#31649#29702
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object RzToolbar1: TRzToolbar
    Left = 0
    Top = 23
    Width = 304
    Height = 29
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsGroove
    BorderSides = [sdTop]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 1
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer1
      RzToolButton1
      RzSpacer2
      RzToolButton2
      RzSpacer3
      RzToolButton3
      RzSpacer4
      RzToolButton4)
    object RzSpacer1: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 12
      Top = 2
      ImageIndex = 37
      OnClick = RzToolButton1Click
    end
    object RzSpacer2: TRzSpacer
      Left = 37
      Top = 2
    end
    object RzToolButton2: TRzToolButton
      Left = 45
      Top = 2
      ImageIndex = 3
      OnClick = RzToolButton2Click
    end
    object RzSpacer3: TRzSpacer
      Left = 70
      Top = 2
    end
    object RzToolButton3: TRzToolButton
      Left = 78
      Top = 2
      ImageIndex = 2
      OnClick = RzToolButton3Click
    end
    object RzSpacer4: TRzSpacer
      Left = 103
      Top = 2
    end
    object RzToolButton4: TRzToolButton
      Left = 111
      Top = 2
      ImageIndex = 8
      OnClick = RzToolButton4Click
    end
  end
  object Grid: TcxGrid
    Left = 0
    Top = 52
    Width = 304
    Height = 405
    Align = alClient
    TabOrder = 2
    object tvWorkType: TcxGridDBTableView
      OnDblClick = tvWorkTypeDblClick
      OnKeyDown = tvWorkTypeKeyDown
      Navigator.Buttons.CustomButtons = <>
      OnEditing = tvWorkTypeEditing
      OnEditKeyDown = tvWorkTypeEditKeyDown
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.CellMultiSelect = True
      OptionsView.DataRowHeight = 26
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 23
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 20
      object tvWorkCol1: TcxGridDBColumn
        Caption = #24207#21495
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        OnGetDisplayText = tvWorkCol1GetDisplayText
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Sorting = False
        Width = 54
      end
      object tvWorkCol2: TcxGridDBColumn
        Caption = #24037#31181#31867#22411
        DataBinding.FieldName = 'WorkType'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Sorting = False
        Width = 222
      end
    end
    object Lv: TcxGridLevel
      GridView = tvWorkType
    end
  end
  object ADOQuery1: TADOQuery
    Connection = DM.ADOconn
    CursorType = ctStatic
    Parameters = <>
    Left = 160
    Top = 121
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 160
    Top = 168
  end
end
