object frmFlowingWaterAccount: TfrmFlowingWaterAccount
  Left = 0
  Top = 0
  Caption = #27969#27700#36134#34920
  ClientHeight = 598
  ClientWidth = 1056
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 14
  object Splitter1: TSplitter
    Left = 280
    Top = 28
    Width = 10
    Height = 550
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitLeft = 256
    ExplicitTop = 0
    ExplicitHeight = 605
  end
  object dxStatusBar1: TdxStatusBar
    Left = 0
    Top = 578
    Width = 1056
    Height = 20
    Panels = <>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object RzPanel1: TRzPanel
    Left = 0
    Top = 0
    Width = 1056
    Height = 28
    Align = alTop
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    Caption = #24448#26469#21333#20301
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object RzPanel5: TRzPanel
      Left = 0
      Top = 0
      Width = 271
      Height = 27
      Align = alLeft
      BorderOuter = fsNone
      BorderSides = []
      Caption = #27969#27700#36134#34920#32479#35745
      TabOrder = 0
    end
    object RzPanel6: TRzPanel
      Left = 271
      Top = 0
      Width = 785
      Height = 27
      Align = alClient
      BorderOuter = fsNone
      Caption = #24448#26469#21333#20301
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object RzPanel3: TRzPanel
    Left = 290
    Top = 28
    Width = 766
    Height = 550
    Align = alClient
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft]
    TabOrder = 2
    object FlowingGrid: TcxGrid
      Left = 1
      Top = 145
      Width = 765
      Height = 405
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 6
      ExplicitTop = 139
      object tvGrid: TcxGridDBTableView
        PopupMenu = PopupMenu1
        OnKeyDown = tvGridKeyDown
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Append.Enabled = False
        OnEditKeyDown = tvGridEditKeyDown
        DataController.DataSource = DataMaster
        DataController.KeyFieldNames = 'Numbers'
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = #21512#35745
            Position = spFooter
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Position = spFooter
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Position = spFooter
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Position = spFooter
            Column = Col10
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Column = Col10
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #24635#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            OnGetText = tvGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
            Column = Col10
          end
          item
            Format = #22823#20889
            Kind = skCount
            Column = Col12
          end
          item
            Kind = skCount
            Column = Col13
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.CancelOnExit = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CellMultiSelect = True
        OptionsView.CellEndEllipsis = True
        OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
        OptionsView.CellAutoHeight = True
        OptionsView.DataRowHeight = 23
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.GroupRowHeight = 30
        OptionsView.HeaderFilterButtonShowMode = fbmButton
        OptionsView.HeaderHeight = 23
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 16
        Styles.OnGetContentStyle = tvGridStylesGetContentStyle
        OnColumnSizeChanged = tvGridColumnSizeChanged
        object Col13: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDisplayText = Col13GetDisplayText
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Width = 40
        end
        object Col14: TcxGridDBColumn
          Caption = #29366#24577
          DataBinding.FieldName = 'status'
          PropertiesClassName = 'TcxCheckBoxProperties'
          HeaderAlignmentHorz = taCenter
          Width = 50
        end
        object Col0: TcxGridDBColumn
          Caption = #32534#21495
          DataBinding.FieldName = 'numbers'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Filtering = False
          Options.Sorting = False
          Width = 91
        end
        object Col1: TcxGridDBColumn
          Caption = #26085#26399
          DataBinding.FieldName = 'cxDate'
          PropertiesClassName = 'TcxDateEditProperties'
          Width = 80
        end
        object Col2: TcxGridDBColumn
          Caption = #21046#34920#31867#21035
          DataBinding.FieldName = 'TabType'
          PropertiesClassName = 'TcxComboBoxProperties'
          Width = 62
        end
        object Col3: TcxGridDBColumn
          Caption = #25910#27454#21333#20301
          DataBinding.FieldName = 'Receivables'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.DropDownWidth = 180
          Properties.ImmediateDropDownWhenActivated = True
          Properties.KeyFieldNames = 'SignName'
          Properties.ListColumns = <
            item
              Caption = #24448#26469#21333#20301
              Width = 120
              FieldName = 'SignName'
            end
            item
              Caption = #31616#30721
              Width = 60
              FieldName = 'PYCode'
            end>
          Properties.ListOptions.ShowHeader = False
          RepositoryItem = DM.SignNameBox
          Width = 109
        end
        object Col4: TcxGridDBColumn
          Caption = #20132#26131#26041#24335
          DataBinding.FieldName = 'TradeType'
          PropertiesClassName = 'TcxComboBoxProperties'
          Width = 84
        end
        object Col5: TcxGridDBColumn
          Caption = #20184#27454#21333#20301
          DataBinding.FieldName = 'PaymentType'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.DropDownWidth = 180
          Properties.ImmediateDropDownWhenActivated = True
          Properties.KeyFieldNames = 'SignName'
          Properties.ListColumns = <
            item
              Width = 120
              FieldName = 'SignName'
            end
            item
              Width = 60
              FieldName = 'PYCode'
            end>
          RepositoryItem = DM.SignNameBox
          Width = 124
        end
        object Col6: TcxGridDBColumn
          Caption = #24037#31243'/'#21517#31216
          DataBinding.FieldName = 'ProjectName'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.DropDownWidth = 200
          Properties.ImmediateDropDownWhenActivated = True
          Properties.KeyFieldNames = 'ProjectName'
          Properties.ListColumns = <
            item
              Caption = #24037#31243#21517
              Width = 140
              FieldName = 'ProjectName'
            end
            item
              Caption = #31616#30721
              Width = 60
              FieldName = 'ProjectPYCode'
            end>
          RepositoryItem = DM.ProjectNameBox
          Width = 72
        end
        object Col7: TcxGridDBColumn
          Caption = #36153#29992#31185#30446
          DataBinding.FieldName = 'CategoryName'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.ImmediateDropDownWhenActivated = True
          Properties.KeyFieldNames = 'CostName'
          Properties.ListColumns = <
            item
              Caption = 'CostName'
              FieldName = 'CostName'
            end>
          Properties.ListOptions.ShowHeader = False
          RepositoryItem = DM.CostComboBox
          Width = 86
        end
        object Col8: TcxGridDBColumn
          Caption = #25910#27454#20154#21517#31216
          DataBinding.FieldName = 'Payee'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 77
        end
        object Col9: TcxGridDBColumn
          Caption = #20184#27454#20154#21517#31216
          DataBinding.FieldName = 'Drawee'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 102
        end
        object Col10: TcxGridDBColumn
          Caption = #37329#39069
          DataBinding.FieldName = 'SumMoney'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taRightJustify
          Properties.OnValidate = Col10PropertiesValidate
          HeaderAlignmentHorz = taRightJustify
          Width = 102
        end
        object Col12: TcxGridDBColumn
          Caption = #22823#20889#37329#39069
          DataBinding.FieldName = 'CapitalMoney'
          Width = 230
        end
        object Col11: TcxGridDBColumn
          Caption = #22791#27880
          DataBinding.FieldName = 'Remarks'
          PropertiesClassName = 'TcxMemoProperties'
          Properties.Alignment = taLeftJustify
          Properties.ScrollBars = ssVertical
          Width = 230
        end
      end
      object tvDetailed: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Visible = True
        DataController.DataSource = DataView
        DataController.DetailKeyFieldNames = 'parent'
        DataController.MasterKeyFieldNames = 'Numbers'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Appending = True
        OptionsView.DataRowHeight = 23
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 20
        OnColumnSizeChanged = tvDetailedColumnSizeChanged
        object tvDetailedColumn1: TcxGridDBColumn
          Caption = #24207#21495
          Width = 40
        end
        object tvDetailedColumn2: TcxGridDBColumn
          Caption = #32534#21495
          DataBinding.FieldName = 'Parent'
          Width = 70
        end
        object tvDetailedColumn3: TcxGridDBColumn
          Caption = #39033#30446#21517#31216
          DataBinding.FieldName = 'appropriation'
          Width = 70
        end
        object tvDetailedColumn4: TcxGridDBColumn
          Caption = #36153#29992#31185#30446
          DataBinding.FieldName = 'ProjectName'
          Width = 70
        end
        object tvDetailedColumn5: TcxGridDBColumn
          Caption = #25968#37327
          DataBinding.FieldName = 'fine'
          Width = 70
        end
        object tvDetailedColumn6: TcxGridDBColumn
          Caption = #21333#20215
          DataBinding.FieldName = 'Armourforwood'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DecimalPlaces = 4
          Properties.DisplayFormat = #165',0.00##;'#165'-,0.00##'
          Width = 70
        end
        object tvDetailedColumn7: TcxGridDBColumn
          Caption = #35745#31639#20844#24335
          DataBinding.FieldName = 'DesignFormulas'
          Width = 70
        end
        object tvDetailedColumn8: TcxGridDBColumn
          Caption = #37329#39069
          DataBinding.FieldName = 'receiptsMoney'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DecimalPlaces = 4
          Properties.DisplayFormat = #165',0.00##;'#165'-,0.00##'
          Width = 70
        end
        object tvDetailedColumn9: TcxGridDBColumn
          Caption = #22791#27880
          DataBinding.FieldName = 'Remarks'
          Width = 70
        end
      end
      object lv: TcxGridLevel
        GridView = tvGrid
        object Lv2: TcxGridLevel
          GridView = tvDetailed
        end
      end
    end
    object RzToolbar2: TRzToolbar
      Left = 1
      Top = 0
      Width = 765
      AutoStyle = False
      Images = DM.cxImageList1
      RowHeight = 28
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = []
      BorderWidth = 0
      Caption = #28165#31639
      GradientColorStyle = gcsCustom
      TabOrder = 1
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer5
        RzToolButton1
        RzSpacer2
        RzToolButton10
        RzSpacer11
        RzToolButton3
        RzSpacer1
        RzToolButton2
        RzSpacer14
        RzToolButton8
        RzSpacer8
        RzToolButton4
        RzSpacer3
        RzToolButton6
        RzSpacer4
        RzToolButton7
        RzSpacer9
        RzToolButton14
        RzSpacer7
        RzToolButton5)
      object RzToolButton2: TRzToolButton
        Left = 216
        Top = 4
        Width = 60
        SelectionColorStop = 16119543
        ImageIndex = 51
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton2Click
      end
      object RzSpacer14: TRzSpacer
        Left = 276
        Top = 4
      end
      object RzToolButton14: TRzToolButton
        Left = 617
        Top = 4
        Width = 60
        SelectionColorStop = 16119543
        DropDownMenu = PmRepot
        ImageIndex = 5
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        ToolStyle = tsDropDown
        Caption = #25253#34920
      end
      object RzSpacer5: TRzSpacer
        Left = 4
        Top = 4
      end
      object RzToolButton1: TRzToolButton
        Left = 12
        Top = 4
        Width = 60
        SelectionColorStop = 16119543
        ImageIndex = 37
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #26032#22686
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object RzToolButton3: TRzToolButton
        Left = 148
        Top = 4
        Width = 60
        SelectionColorStop = 16119543
        ImageIndex = 3
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20445#23384
        Visible = False
        OnClick = RzToolButton3Click
      end
      object RzSpacer1: TRzSpacer
        Left = 208
        Top = 4
      end
      object RzSpacer2: TRzSpacer
        Left = 72
        Top = 4
        Visible = False
      end
      object RzToolButton4: TRzToolButton
        Left = 372
        Top = 4
        Width = 80
        SelectionColorStop = 16119543
        ImageIndex = 4
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #34920#26684#35774#32622
        OnClick = RzToolButton4Click
      end
      object RzSpacer3: TRzSpacer
        Left = 452
        Top = 4
        Visible = False
      end
      object RzSpacer4: TRzSpacer
        Left = 520
        Top = 4
      end
      object RzToolButton5: TRzToolButton
        Left = 685
        Top = 4
        Width = 56
        SelectionColorStop = 16119543
        ImageIndex = 8
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #36864#20986
        OnClick = RzToolButton5Click
      end
      object RzToolButton6: TRzToolButton
        Left = 460
        Top = 4
        Width = 60
        SelectionColorStop = 16119543
        ImageIndex = 10
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #26597#35810
        Visible = False
      end
      object RzToolButton7: TRzToolButton
        Left = 528
        Top = 4
        Width = 81
        Height = 24
        SelectionColorStop = 16119543
        ImageIndex = 43
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20840#37096#25968#25454
        OnClick = RzToolButton7Click
      end
      object RzSpacer7: TRzSpacer
        Left = 677
        Top = 4
      end
      object RzSpacer8: TRzSpacer
        Left = 364
        Top = 4
      end
      object RzToolButton8: TRzToolButton
        Left = 284
        Top = 4
        Width = 80
        SelectionColorStop = 16119543
        ImageIndex = 16
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #26126#32454#34920
        Visible = False
      end
      object RzSpacer9: TRzSpacer
        Left = 609
        Top = 4
      end
      object RzSpacer11: TRzSpacer
        Left = 140
        Top = 4
        Visible = False
      end
      object RzToolButton10: TRzToolButton
        Left = 80
        Top = 4
        Width = 60
        SelectionColorStop = 16119543
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
        Visible = False
      end
    end
    object RzPanel4: TRzPanel
      Left = 1
      Top = 32
      Width = 765
      Height = 113
      Align = alTop
      BorderOuter = fsFlat
      TabOrder = 2
      object Label10: TLabel
        Left = 224
        Top = 23
        Width = 60
        Height = 14
        Caption = #36215#22987#26085#26399#65306
      end
      object Label4: TLabel
        Left = 224
        Top = 47
        Width = 60
        Height = 14
        Caption = #32467#26463#26085#26399#65306
      end
      object cxComboBox1: TcxComboBox
        Left = 80
        Top = 47
        Enabled = False
        Properties.DropDownListStyle = lsEditFixedList
        Properties.Items.Strings = (
          #24080#30446#25910#27454
          #24080#30446#25903#27454
          #29616#37329#25910#27454
          #29616#37329#25903#27454)
        Properties.OnCloseUp = cxComboBox1PropertiesCloseUp
        TabOrder = 1
        Width = 126
      end
      object cxLabel15: TcxLabel
        Left = 18
        Top = 47
        Caption = #21046#34920#31867#21035#65306
        Transparent = True
      end
      object cxDateEdit1: TcxDateEdit
        Left = 284
        Top = 19
        Enabled = False
        TabOrder = 3
        Width = 110
      end
      object cxDateEdit2: TcxDateEdit
        Left = 284
        Top = 47
        Enabled = False
        TabOrder = 4
        Width = 110
      end
      object cxLabel4: TcxLabel
        Left = 18
        Top = 20
        Caption = #39033#30446#21517#31216#65306
      end
      object cxLookupComboBox3: TcxLookupComboBox
        Left = 80
        Top = 20
        Enabled = False
        Properties.DropDownWidth = 200
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'ProjectName'
        Properties.ListColumns = <
          item
            Caption = #39033#30446#21517#31216
            FieldName = 'ProjectName'
          end
          item
            Caption = #31616#30721
            FieldName = 'ProjectPYCode'
          end>
        Properties.ListSource = DM.DataProjectName
        Properties.OnCloseUp = cxLookupComboBox3PropertiesCloseUp
        TabOrder = 0
        Width = 126
      end
      object cxButton1: TcxButton
        Left = 511
        Top = 19
        Width = 89
        Height = 46
        Caption = #26597#35810
        Enabled = False
        LookAndFeel.NativeStyle = False
        LookAndFeel.SkinName = 'Office2013White'
        OptionsImage.Glyph.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0002000000040000000500000004000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000040000
          000A000000110000001400000011000000090000000300000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000050000000D0307
          10380F2455C01D448BFA152F63BD040810310000000900000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000001000000050000000D0408133D1D45
          83EC5294CBFF63AEE5FF8AB5DAFF203E70C20000001100000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000040000000D040A143C214E8DEC5AA8
          DEFF4598E0FF93D4F6FFEAF8FEFF2C5696F00000001400000005000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000001000000040000000C050C163B265695EC5CA9DFFF3E94
          DDFF92D3F6FFEAF9FFFF76B2DDFF254878C20000001100000004000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0002000000030000000400000004000000040000000400000004000000030000
          000100000001000000040000000C0D0B0B392B5E9CEC5BABE0FF4096DFFF92D3
          F6FFEAF9FFFF86C8EEFF32619FEC070D16380000000A00000002000000000000
          0000000000000000000000000000000000000000000100000003000000050000
          00080000000B0000000E0000000F00000010000000100000000E0000000B0000
          0008000000070000000C110A08397B4C3EF58792A6FF4398E0FF93D4F6FFEAF9
          FFFF87C7EEFF3468A4EC070E183D0000000D0000000400000001000000000000
          00000000000000000000000000010000000200000004000000090C0807263B28
          21835C3D34C4775045F77A5145FF794F42FF794F43FF5C3B33CA3B26208C110B
          093700000014110A083B784C40EBAC8071FF835548FFA7BAC7FFEAF9FFFF88C8
          F0FF376CA9EC080F193C0000000D000000050000000100000000000000000000
          0000000000000000000100000002000000060604041749332C96856154F7B294
          89FFCFB8ADFFEBD9CFFFECDCD0FFECDBD0FFEBDBD0FFCFB8ACFFAE9084FF815A
          4EFF4E332BB76A4438EEA77D6FFF8F6354FFC3A79FFFC8B2AAFFB2CAD9FF3A71
          ADEC08101A3B0000000D00000005000000010000000000000000000000000000
          0000000000000000000200000006140E0C2F725246D9B59A8FFFE6D4CAFFF0E3
          DAFFF9F2EBFFFDF9F3FFFFFCF7FFFFFCF7FFFDFAF3FFFAF3EDFFF3E7DEFFE6D4
          CAFFAE9084FF7B5447FF745045FFBDA29AFFFDFCFBFFDDCEC8FF895F53F5110F
          0F390000000C0000000400000001000000000000000000000000000000000000
          00000000000100000005110C0B29886459EDD4C2B9FFEFE0D7FFF9F2EAFFF3EA
          DEFFD4BB9CFFC09D73FFB38855FFB48956FFC49F75FFD7BE9FFFF4EBDFFFFBF5
          F0FFF0E2D9FFCDB6ABFF825A4FFFB2A29DFFD4C6C1FF825C51EB140D0B390000
          000C000000040000000100000000000000000000000000000000000000000000
          000100000003050403127A594FD5D6C5BEFFEEE2D8FFFCF6EFFFDBC5ACFFB489
          59FFCDA872FFDDC18AFFEBD49DFFECD7A2FFE2C995FFD2B481FFBC9362FFDFCA
          B0FFFDF9F3FFF1E5DCFFCCB4A9FF815D52FF6F4D41EE110B09390000000C0000
          0004000000010000000000000000000000000000000000000000000000000000
          0001000000054836307FC1ACA3FFF0E4DDFFF8F0E8FFC4A17EFFBC915EFFDCB7
          7AFFE3BF7FFFE3C07FFFE4C585FFE7CC8FFFEBD39BFFEFDCABFFE7D4A4FFC7A2
          71FFCBAC86FFFAF5EDFFF0E3DAFFB5998EFF5A3E35BD00000015000000070000
          0001000000000000000000000000000000000000000000000000000000010000
          0003090706179B786DF5F2EBE7FFF3E8E1FFCEB094FFBD8F5CFFDFB371FFDFB7
          74FFE2BA79FFE3BE7FFFE5C385FFE6C78AFFE9CB91FFEBD198FFEED9A8FFF0DE
          B2FFCAA578FFD7BD9FFFF7EFE8FFE8D7CEFF8E695CFC150E0C3A000000080000
          0002000000000000000000000000000000000000000000000000000000010000
          000443332E6FC8B2AAFFF5EDE8FFECDDD0FFAE7A4FFFDAA867FFDEAE6DFFDFB2
          73FFE2B77AFFE4BB7FFFE4BF85FFE6C38BFFE9C790FFEAC994FFEBCD98FFEED6
          A9FFEDD6AEFFBF9065FFF1E4D8FFF1E7DFFFBAA095FF4A342E940000000B0000
          0003000000000000000000000000000000000000000000000000000000010000
          0005735950B5E5D9D6FFF3EAE3FFCCAA91FFC28A55FFDDA768FFDFAC6DFFDFAF
          73FFE2B379FFE4B77FFFE5BA83FFE7BF89FFE9C38FFFEAC493FFEBC696FFEBC9
          99FFF0D4AEFFD5AF87FFD8BAA0FFF5EBE4FFDAC6BDFF74544AD50000000E0000
          0004000000000000000000000000000000000000000000000000000000010000
          0005947369E0F8F4F3FFF3E9E2FFB17F5EFFD2955EFFDDA368FFE1AC76FFEAC4
          A0FFEDCBADFFEDC8A7FFE9C39CFFE7B98AFFE8BC8BFFE9BF90FFE9C092FFEAC2
          95FFECC9A0FFE5C29DFFC59877FFF6EEE8FFEADCD4FF8F6A5EFA020101130000
          0004000000000000000000000000000000000000000000000000000000010000
          0006A58379F1FFFEFEFFF3EBE5FFA46946FFD89961FFE1AA78FFF0D5C3FFF0D4
          C2FFEFCFBBFFEECDB4FFEECAAEFFECC7A7FFE8B98DFFE8B88CFFE8BA90FFE8BB
          91FFEABE95FFECC7A5FFBD8966FFF8F0EBFFF1E4DDFF9C7669FF0B0807220000
          0005000000010000000000000000000000000000000000000000000000010000
          0005AA897EF1FFFFFFFFF5EDE9FFA16544FFDB9861FFEECBB6FFF4DCD3FFF2D8
          CBFFF1D4C3FFEFCFBCFFEFCBB5FFEDC8AFFFEABF9FFFE7B189FFE7B38BFFE7B4
          8CFFE7B68EFFEAC1A2FFBF8669FFF7F1ECFFF2E7E1FF9F7A6EFF0907061E0000
          0004000000010000000000000000000000000000000000000000000000010000
          000498796FD7FAF7F6FFF8F2EFFFAB7158FFCF8B59FFF6E2DDFFF6E1DCFFF4DC
          D3FFF3D8CCFFF1D2C3FFEFCEBCFFEFCAB6FFEDC5ADFFE6AC85FFE6AE86FFE6AE
          89FFE7B08BFFE3B495FFC69278FFF6EFEAFFF0E6E2FF987568F4020101120000
          0004000000000000000000000000000000000000000000000000000000010000
          0003776058A8EAE0DCFFFBF8F7FFC39A88FFBC764EFFF7E6E4FFF8E6E5FFF6E1
          DDFFF4DCD4FFF3D7CCFFF1D1C3FFF0CDBCFFEEC7B3FFE5A67FFFE6A782FFE6A8
          83FFE7AC89FFD39C7EFFD6B2A0FFF5EDE8FFE4D7D2FF7F6358CC0000000C0000
          0003000000000000000000000000000000000000000000000000000000010000
          000244383361D6C2BBFFFFFEFEFFE7D6CEFF9F593DFFECCBBCFFF9EBECFFF8E6
          E5FFF6E1DDFFF4DBD4FFF3D6CCFFF1D1C3FFEBBDA5FFE3A27AFFE4A37CFFE4A3
          7CFFE5AA89FFBC7D65FFEDDED7FFF4ECE7FFCCB6AFFF53423B89000000090000
          0002000000000000000000000000000000000000000000000000000000000000
          000109070711AF9187E9F8F4F3FFFBF9F8FFBD9381FFAD6A4AFFF8E9E9FFF9EB
          ECFFF7E6E5FFF6E1DDFFF4DBD3FFF2D1C5FFE4A57FFFE19D74FFE29E75FFE29F
          77FFC6896CFFD4AD9DFFF5F0EAFFF6F0EDFFAE8C80FA120E0D28000000050000
          0001000000000000000000000000000000000000000000000000000000000000
          00010000000350413D6DDAC4BEFFFFFFFFFFF8F2F1FFB17F6AFFA86444FFE5BF
          AFFFF6E1DEFFF5DDD7FFEEC7B5FFE19F76FFE0996DFFE19A6EFFDA946BFFBF7F
          62FFC99A87FFF6EEEAFFF9F4F1FFD0BBB3FF5E4B449000000009000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010202020791776EBFE8DAD6FFFFFFFFFFFBF8F6FFCDAC9EFF9F5D
          43FFBC744EFFCE855BFFDC9265FFDC9266FFD28A62FFC6825FFFB5765CFFDABB
          AEFFF7F2EEFFF9F5F2FFE5D9D4FF95776DD60B08081A00000004000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000020E0C0B16A98E83DAE9DDD7FFFFFFFFFFFCFAFAFFEBDC
          D5FFC69F90FFB27C66FFA46248FFA7664BFFBA856FFFD0A999FFEEE0D9FFF8F2
          EFFFFCF9F9FFE6D9D5FFAC8C82EA1A15132F0000000600000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000001000000020E0C0B17957D74C2DCC8C1FFFAF6F5FFFFFF
          FFFFFEFBFBFFFAF8F6FFF9F5F2FFF8F3F0FFF9F4F1FFFBF6F5FFFDFBFBFFFAF8
          F7FFD8C4BCFF9B8075D516121129000000060000000200000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000100000002020202075345406DB99B90E9DDCA
          C2FFEEE5E1FFFAF8F7FFFFFFFFFFFFFFFFFFFAF7F6FFEEE5E1FFDBC7BFFFBA9C
          91F25B4B457F0706051200000005000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000003090807104036
          3256826D65A8A1877DCEB7998EEBBE9E92F49F847BCF877068B2463935610E0C
          0B19000000060000000300000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          0002000000030000000400000005000000060000000500000005000000040000
          0002000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000100000001000000010000000100000001000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        TabOrder = 7
        OnClick = cxButton1Click
      end
      object cxRadioButton1: TcxRadioButton
        Left = 412
        Top = 21
        Width = 93
        Height = 17
        Caption = #25353#21517#31216#26597#35810
        Checked = True
        Enabled = False
        TabOrder = 5
        TabStop = True
      end
      object cxRadioButton2: TcxRadioButton
        Left = 412
        Top = 48
        Width = 93
        Height = 17
        Caption = #25353#26085#26399#26597#35810
        Enabled = False
        TabOrder = 6
      end
      object cxLabel1: TcxLabel
        Left = 18
        Top = 74
        Caption = #25968#25454#29366#24577#65306
        Transparent = True
      end
      object cxComboBox2: TcxComboBox
        Left = 80
        Top = 74
        Enabled = False
        Properties.DropDownListStyle = lsEditFixedList
        Properties.Items.Strings = (
          #24050#20923#32467
          #26410#20923#32467)
        Properties.OnCloseUp = cxComboBox2PropertiesCloseUp
        TabOrder = 2
        Width = 126
      end
    end
  end
  object cLeft: TRzPanel
    Left = 0
    Top = 28
    Width = 280
    Height = 550
    Align = alLeft
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdRight]
    TabOrder = 3
  end
  object DataMaster: TDataSource
    DataSet = Master
    Left = 616
    Top = 376
  end
  object PmRepot: TPopupMenu
    Images = DM.cxImageList1
    Left = 123
    Top = 234
    object Execl1: TMenuItem
      Caption = #36755#20986'Excel'
      OnClick = Execl1Click
    end
    object N6: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
      OnClick = N6Click
    end
    object Fast1: TMenuItem
      Caption = 'Fast'#25171#21360#25253#34920
      OnClick = Fast1Click
    end
  end
  object ADOView: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = qryAfterOpen
    Parameters = <>
    Left = 808
    Top = 400
  end
  object DataView: TDataSource
    DataSet = ADOView
    Left = 808
    Top = 456
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 616
    Top = 312
  end
  object PopupMenu1: TPopupMenu
    Images = DM.cxImageList1
    Left = 424
    Top = 336
    object N1: TMenuItem
      Caption = #21024#38500
      ImageIndex = 51
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #38468#20214
      ImageIndex = 55
      OnClick = N2Click
    end
  end
  object frxDBDataset: TfrxDBDataset
    UserName = 'Detailed'
    CloseDataSource = False
    DataSource = DataView
    BCDToCurrency = False
    Left = 460
    Top = 416
  end
end
