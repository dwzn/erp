object frmMakings: TfrmMakings
  Left = 0
  Top = 0
  Caption = #21830#21697#20449#24687
  ClientHeight = 632
  ClientWidth = 1259
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RzPanel1: TRzPanel
    Left = 0
    Top = 0
    Width = 1259
    Height = 613
    Align = alClient
    BorderOuter = fsNone
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 321
      Top = 0
      Width = 10
      Height = 613
      Color = 16250613
      ParentColor = False
      ResizeStyle = rsUpdate
      ExplicitLeft = 178
      ExplicitHeight = 335
    end
    object RzPanel10: TRzPanel
      Left = 0
      Top = 0
      Width = 321
      Height = 613
      Align = alLeft
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdLeft, sdRight]
      TabOrder = 0
      object tv: TTreeView
        Left = 1
        Top = 32
        Width = 319
        Height = 581
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        DragMode = dmAutomatic
        HideSelection = False
        Images = DM.TreeImageList
        Indent = 19
        RowSelect = True
        SortType = stBoth
        TabOrder = 0
        OnClick = tvClick
        OnDragDrop = tvDragDrop
        OnDragOver = tvDragOver
        OnEdited = tvEdited
        OnKeyDown = tvKeyDown
        Items.NodeData = {
          0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
          000500000001045067996506527B7C220000000000000000000000FFFFFFFFFF
          FFFFFF0000000000000000000000000102A29450672400000000000000000000
          00FFFFFFFFFFFFFFFF0000000000000000050000000103F76DDD511F57220000
          000000000000000000FFFFFFFFFFFFFFFF000000000000000000000000010297
          62176E2A0000000000000000000000FFFFFFFFFFFFFFFF000000000000000000
          00000001067651D64E80622F678189426C260000000000000000000000FFFFFF
          FFFFFFFFFF0000000000000000000000000104938F0190B9650F5F2400000000
          00000000000000FFFFFFFFFFFFFFFF00000000000000000000000001033C783A
          5FA65E240000000000000000000000FFFFFFFFFFFFFFFF000000000000000000
          00000001031659A0524252220000000000000000000000FFFFFFFFFFFFFFFF00
          0000000000000000000000010228675067220000000000000000000000FFFFFF
          FFFFFFFFFF0000000000000000000000000102F3775067220000000000000000
          000000FFFFFFFFFFFFFFFF0000000000000000000000000102346CE56C}
      end
      object RzToolbar6: TRzToolbar
        Left = 1
        Top = 0
        Width = 319
        AutoStyle = False
        Images = DM.cxImageList1
        RowHeight = 28
        BorderInner = fsNone
        BorderOuter = fsFlat
        BorderSides = [sdTop]
        BorderWidth = 0
        GradientColorStyle = gcsCustom
        TabOrder = 1
        VisualStyle = vsGradient
        ToolbarControls = (
          RzSpacer16
          RzToolButton18
          RzSpacer26
          RzToolButton19
          RzSpacer17
          RzToolButton20
          RzSpacer21
          RzToolButton23
          RzSpacer22
          RzToolButton24)
        object RzSpacer16: TRzSpacer
          Left = 4
          Top = 4
        end
        object RzToolButton18: TRzToolButton
          Left = 12
          Top = 4
          Width = 53
          ImageIndex = 37
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #26032#22686
          OnClick = RzToolButton18Click
        end
        object RzSpacer17: TRzSpacer
          Left = 126
          Top = 4
        end
        object RzToolButton20: TRzToolButton
          Left = 134
          Top = 4
          Width = 53
          ImageIndex = 2
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #21024#38500
          OnClick = RzToolButton18Click
        end
        object RzSpacer21: TRzSpacer
          Left = 187
          Top = 4
        end
        object RzToolButton23: TRzToolButton
          Left = 195
          Top = 4
          Width = 53
          ImageIndex = 29
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #19978#31227
          OnClick = RzToolButton23Click
        end
        object RzSpacer22: TRzSpacer
          Left = 248
          Top = 4
        end
        object RzToolButton24: TRzToolButton
          Left = 256
          Top = 4
          Width = 53
          ImageIndex = 31
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #19979#31227
          OnClick = RzToolButton24Click
        end
        object RzSpacer26: TRzSpacer
          Left = 65
          Top = 4
        end
        object RzToolButton19: TRzToolButton
          Left = 73
          Top = 4
          Width = 53
          ImageIndex = 0
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #32534#36753
          OnClick = RzToolButton18Click
        end
      end
    end
    object RzPanel9: TRzPanel
      Left = 331
      Top = 0
      Width = 928
      Height = 613
      Align = alClient
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdLeft, sdRight]
      TabOrder = 1
      object RzToolbar5: TRzToolbar
        Left = 1
        Top = 0
        Width = 926
        AutoStyle = False
        Images = DM.cxImageList1
        RowHeight = 28
        BorderInner = fsNone
        BorderOuter = fsFlat
        BorderSides = [sdTop]
        BorderWidth = 0
        GradientColorStyle = gcsCustom
        TabOrder = 0
        VisualStyle = vsGradient
        ToolbarControls = (
          RzSpacer13
          RzToolButton15
          RzSpacer3
          RzToolButton2
          RzSpacer2
          RzToolButton16
          RzSpacer14
          RzToolButton17
          RzSpacer5
          RzToolButton4
          cxLabel2
          cxTextEdit2
          RzSpacer18
          RzToolButton21
          RzSpacer1
          RzToolButton5
          RzSpacer6
          RzToolButton6
          RzSpacer7
          RzToolButton3
          RzSpacer4
          RzToolButton1)
        object RzSpacer13: TRzSpacer
          Left = 4
          Top = 4
        end
        object RzToolButton15: TRzToolButton
          Left = 12
          Top = 4
          Width = 50
          ImageIndex = 37
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #26032#22686
          OnClick = RzToolButton15Click
        end
        object RzToolButton16: TRzToolButton
          Left = 128
          Top = 4
          Width = 50
          ImageIndex = 3
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #20445#23384
          OnClick = RzToolButton15Click
        end
        object RzSpacer14: TRzSpacer
          Left = 178
          Top = 4
        end
        object RzToolButton17: TRzToolButton
          Left = 186
          Top = 4
          Width = 50
          ImageIndex = 2
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #21024#38500
          OnClick = RzToolButton15Click
        end
        object RzSpacer18: TRzSpacer
          Left = 557
          Top = 4
        end
        object RzToolButton21: TRzToolButton
          Left = 565
          Top = 4
          ImageIndex = 10
          Caption = #26597#35810
          OnClick = RzToolButton21Click
        end
        object RzToolButton1: TRzToolButton
          Left = 791
          Top = 4
          Width = 65
          ImageIndex = 8
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #36864#20986
          OnClick = RzToolButton1Click
        end
        object RzSpacer1: TRzSpacer
          Left = 590
          Top = 4
        end
        object RzSpacer2: TRzSpacer
          Left = 120
          Top = 4
        end
        object RzSpacer3: TRzSpacer
          Left = 62
          Top = 4
        end
        object RzToolButton2: TRzToolButton
          Left = 70
          Top = 4
          Width = 50
          ImageIndex = 0
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #32534#36753
          OnClick = RzToolButton2Click
        end
        object RzSpacer4: TRzSpacer
          Left = 783
          Top = 4
        end
        object RzToolButton3: TRzToolButton
          Left = 718
          Top = 4
          Width = 65
          DropDownMenu = Print
          ImageIndex = 5
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          ToolStyle = tsDropDown
          Caption = #25253#34920
        end
        object RzSpacer5: TRzSpacer
          Left = 236
          Top = 4
        end
        object RzToolButton4: TRzToolButton
          Left = 244
          Top = 4
          Width = 76
          ImageIndex = 4
          ShowCaption = True
          UseToolbarShowCaption = False
          Caption = #34920#26684#35774#32622
          OnClick = RzToolButton4Click
        end
        object RzSpacer6: TRzSpacer
          Left = 650
          Top = 4
        end
        object RzToolButton5: TRzToolButton
          Left = 598
          Top = 4
          Width = 52
          ImageIndex = 71
          ShowCaption = True
          UseToolbarShowCaption = False
          Caption = #23548#20837
          OnClick = RzToolButton5Click
        end
        object RzSpacer7: TRzSpacer
          Left = 710
          Top = 4
        end
        object RzToolButton6: TRzToolButton
          Left = 658
          Top = 4
          Width = 52
          ImageIndex = 73
          ShowCaption = True
          UseToolbarShowCaption = False
          Caption = #23548#20986
          OnClick = RzToolButton6Click
        end
        object cxLabel2: TcxLabel
          Left = 320
          Top = 8
          Caption = #20851#38190#23383':'
          Transparent = True
        end
        object cxTextEdit2: TcxTextEdit
          Left = 364
          Top = 6
          TabOrder = 1
          TextHint = #26448#26009#21517#31216
          OnKeyDown = cxTextEdit2KeyDown
          Width = 193
        end
      end
      object Grid: TcxGrid
        Left = 1
        Top = 32
        Width = 926
        Height = 581
        Align = alClient
        TabOrder = 1
        object tvView: TcxGridDBTableView
          PopupMenu = pm1
          OnDblClick = tvViewDblClick
          OnKeyDown = tvViewKeyDown
          Navigator.Buttons.CustomButtons = <>
          Navigator.Visible = True
          OnEditing = tvViewEditing
          OnEditKeyDown = tvViewEditKeyDown
          OnEditValueChanged = tvViewEditValueChanged
          DataController.DataModeController.GridModeBufferCount = 30
          DataController.DataSource = ds1
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.DragDropText = True
          OptionsBehavior.DragFocusing = dfDragDrop
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.NavigatorHints = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsBehavior.PullFocusing = True
          OptionsCustomize.ColumnMoving = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsView.DataRowHeight = 22
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 16
          OnColumnSizeChanged = tvViewColumnSizeChanged
          object tvViewColumn1: TcxGridDBColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            OnGetDisplayText = tvViewColumn1GetDisplayText
            HeaderAlignmentHorz = taCenter
            Options.Filtering = False
            Options.Sorting = False
            Width = 35
          end
          object tvViewColumn6: TcxGridDBColumn
            Caption = #32534#21495'/'#36135#21495
            DataBinding.FieldName = 'GoodsNo'
            SortIndex = 0
            SortOrder = soAscending
            Width = 88
          end
          object tvViewColumn7: TcxGridDBColumn
            Caption = #26465#30721
            DataBinding.FieldName = 'BarCode'
            Width = 139
          end
          object tvViewColumn2: TcxGridDBColumn
            Caption = #21517#31216
            DataBinding.FieldName = 'MakingCaption'
            PropertiesClassName = 'TcxTextEditProperties'
            HeaderAlignmentHorz = taCenter
            Options.Filtering = False
            Options.Moving = False
            Options.Sorting = False
            Width = 183
          end
          object tvViewColumn8: TcxGridDBColumn
            Caption = #26085#26399
            DataBinding.FieldName = 'InputDate'
            Width = 70
          end
          object tvViewColumn4: TcxGridDBColumn
            Caption = #22411#21495
            DataBinding.FieldName = 'ModelIndex'
            PropertiesClassName = 'TcxTextEditProperties'
            HeaderAlignmentHorz = taCenter
            Options.Filtering = False
            Options.Moving = False
            Options.Sorting = False
            Width = 70
          end
          object tvViewColumn3: TcxGridDBColumn
            Caption = #35268#26684
            DataBinding.FieldName = 'spec'
            PropertiesClassName = 'TcxTextEditProperties'
            HeaderAlignmentHorz = taCenter
            Options.Filtering = False
            Options.Moving = False
            Options.Sorting = False
            Width = 70
          end
          object tvViewColumn5: TcxGridDBColumn
            Caption = #21697#29260
            DataBinding.FieldName = 'Brand'
            PropertiesClassName = 'TcxTextEditProperties'
            HeaderAlignmentHorz = taCenter
            Options.Filtering = False
            Options.Moving = False
            Options.Sorting = False
            Width = 155
          end
          object tvViewColumn9: TcxGridDBColumn
            Caption = #21378#23478
            DataBinding.FieldName = 'Manufactor'
            Width = 80
          end
          object tvViewColumn10: TcxGridDBColumn
            Caption = #39068#33394
            DataBinding.FieldName = 'NoColour'
            Width = 60
          end
          object tvViewColumn12: TcxGridDBColumn
            Caption = #36827#20215
            DataBinding.FieldName = 'BuyingPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 60
          end
          object tvViewColumn11: TcxGridDBColumn
            Caption = #21806#20215
            DataBinding.FieldName = 'UnitPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 60
          end
        end
        object Lv: TcxGridLevel
          GridView = tvView
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 613
    Width = 1259
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Print: TPopupMenu
    Images = DM.cxImageList1
    Left = 344
    Top = 120
    object N2: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
      OnClick = N2Click
    end
    object N1: TMenuItem
      Caption = #25171#21360#26465#30721'('#20840#37096')'
      OnClick = N1Click
    end
    object N3: TMenuItem
      Caption = #25171#21360#26465#30721'('#36873#25321#37096#20998')'
      OnClick = N3Click
    end
  end
  object dsMakings: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = dsMakingsAfterInsert
    AfterEdit = dsMakingsAfterEdit
    AfterScroll = dsMakingsAfterScroll
    Left = 400
    Top = 216
  end
  object ds1: TDataSource
    DataSet = dsMakings
    Left = 400
    Top = 280
  end
  object dxSelectPrint: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 616
    Top = 328
    object dxSelectPrintMakingCaption: TStringField
      FieldName = 'MakingCaption'
      Size = 50
    end
    object dxSelectPrintModelIndex: TStringField
      FieldName = 'ModelIndex'
      Size = 255
    end
    object dxSelectPrintBrand: TStringField
      FieldKind = fkCalculated
      FieldName = 'Brand'
      Size = 50
      Calculated = True
    end
    object dxSelectPrintspec: TStringField
      FieldName = 'spec'
      Size = 50
    end
    object dxSelectPrintGoodsNo: TStringField
      FieldName = 'GoodsNo'
    end
    object dxSelectPrintBarCode: TStringField
      FieldName = 'BarCode'
      Size = 50
    end
    object dxSelectPrintManufactor: TStringField
      FieldName = 'Manufactor'
      Size = 255
    end
    object dxSelectPrintNoColour: TStringField
      FieldName = 'NoColour'
      Size = 50
    end
    object dxSelectPrintBuyingPrice: TCurrencyField
      FieldName = 'BuyingPrice'
    end
    object dxSelectPrintUnitPrice: TCurrencyField
      FieldName = 'UnitPrice'
    end
    object dxSelectPrintInputDate: TDateField
      FieldName = 'InputDate'
    end
    object dxSelectPrintProjectName: TStringField
      FieldName = 'ProjectName'
      Size = 255
    end
    object dxSelectPrintProjectCode: TStringField
      FieldName = 'ProjectCode'
      Size = 255
    end
    object dxSelectPrintSign_Id: TIntegerField
      FieldName = 'Sign_Id'
    end
    object dxSelectPrintId: TIntegerField
      FieldName = 'Id'
    end
  end
  object dsSelectPrint: TDataSource
    DataSet = dxSelectPrint
    Left = 512
    Top = 328
  end
  object pm1: TPopupMenu
    Images = DM.cxImageList1
    Left = 536
    Top = 184
    object N4: TMenuItem
      Caption = #26032#12288#12288#22686
      ImageIndex = 37
      OnClick = N4Click
    end
    object N5: TMenuItem
      Caption = #32534#12288#12288#36753
      ImageIndex = 0
      OnClick = N5Click
    end
    object N6: TMenuItem
      Caption = #20445#12288#12288#23384
      ImageIndex = 3
      OnClick = N6Click
    end
    object N7: TMenuItem
      Caption = #21024#12288#12288#38500
      ImageIndex = 51
      OnClick = N7Click
    end
    object N11: TMenuItem
      Caption = #21047#12288#12288#26032
      ImageIndex = 11
      OnClick = N11Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N9: TMenuItem
      Caption = #25171#21360#26465#30721'('#20840#37096')'
      OnClick = N1Click
    end
    object N10: TMenuItem
      Caption = #25171#21360#26465#30721'('#36873#20013#37096#20998')'
      OnClick = N3Click
    end
    object N12: TMenuItem
      Caption = #22797#21046
      OnClick = N12Click
    end
    object N13: TMenuItem
      Caption = #31896#36148
      OnClick = N13Click
    end
  end
end
