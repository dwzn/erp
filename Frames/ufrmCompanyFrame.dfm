object frmCompanyFrame: TfrmCompanyFrame
  Left = 0
  Top = 0
  Width = 451
  Height = 670
  Align = alClient
  TabOrder = 0
  OnClick = FrameClick
  ExplicitHeight = 304
  object RzPanel2: TRzPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 670
    Align = alClient
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdRight]
    TabOrder = 0
    ExplicitHeight = 304
    object Splitter3: TSplitter
      Left = 0
      Top = 202
      Width = 451
      Height = 6
      Cursor = crVSplit
      Align = alBottom
      Color = 16250613
      ParentColor = False
      ResizeStyle = rsUpdate
      ExplicitLeft = 1
      ExplicitTop = 377
      ExplicitWidth = 295
    end
    object PeopleGrid: TcxGrid
      Left = 0
      Top = 58
      Width = 451
      Height = 115
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 231
      object tvpeopleView: TcxGridDBTableView
        OnDblClick = tvpeopleViewDblClick
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ds
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.DataRowHeight = 26
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 23
        OptionsView.Indicator = True
        object tvpeopleViewCol1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.ReadOnly = True
          OnGetDisplayText = tvCompanyColumn1GetDisplayText
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Sorting = False
          Width = 28
        end
        object tvpeopleViewCol2: TcxGridDBColumn
          Caption = #22995#21517
          DataBinding.FieldName = 'Contacts'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Sorting = False
          Width = 113
        end
        object tvpeopleViewCol4: TcxGridDBColumn
          Caption = #32844#21153
          DataBinding.FieldName = 'duties'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Sorting = False
          Width = 55
        end
        object tvpeopleViewColumn1: TcxGridDBColumn
          Caption = #21333#20215
          DataBinding.FieldName = 'UnitPrice'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Visible = False
        end
        object tvpeopleViewColumn2: TcxGridDBColumn
          Caption = #21152#29677#21333#20215
          DataBinding.FieldName = 'OverPrice'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Visible = False
        end
        object tvpeopleViewColumn3: TcxGridDBColumn
          Caption = #24037#36164#31867#22411
          DataBinding.FieldName = 'wagestype'
          Visible = False
        end
        object tvpeopleViewColumn4: TcxGridDBColumn
          Caption = #26368#23569#20986#21220
          DataBinding.FieldName = 'LeastWork'
          Visible = False
        end
        object tvpeopleViewColumn5: TcxGridDBColumn
          Caption = #24615#21035
          DataBinding.FieldName = 'Sex'
          Visible = False
          Width = 50
        end
        object tvpeopleViewColumn6: TcxGridDBColumn
          Caption = #37096#38376
          DataBinding.FieldName = 'sectione'
          Visible = False
        end
        object tvpeopleViewColumn7: TcxGridDBColumn
          Caption = #32534#21495
          DataBinding.FieldName = 'Numbers'
          Visible = False
        end
      end
      object peopleLv: TcxGridLevel
        GridView = tvpeopleView
      end
    end
    object RzToolbar3: TRzToolbar
      Left = 0
      Top = 173
      Width = 451
      Height = 29
      Align = alBottom
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdBottom]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 1
      VisualStyle = vsGradient
      ExplicitTop = 0
      ToolbarControls = (
        RzSpacer12
        cxLabel2
        cxLookupComboBox1
        RzSpacer13
        RzToolButton13)
      object RzSpacer12: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzSpacer13: TRzSpacer
        Left = 254
        Top = 2
      end
      object RzToolButton13: TRzToolButton
        Left = 262
        Top = 2
        ImageIndex = 10
        OnClick = RzToolButton13Click
      end
      object cxLabel2: TcxLabel
        Left = 12
        Top = 6
        Caption = #20844#21496#21517#31216#65306
        Transparent = True
      end
      object cxLookupComboBox1: TcxLookupComboBox
        Left = 76
        Top = 4
        RepositoryItem = DM.CompanyList
        Properties.ImmediateDropDownWhenActivated = True
        Properties.ListColumns = <>
        Properties.OnCloseUp = cxLookupComboBox1PropertiesCloseUp
        TabOrder = 1
        Width = 178
      end
    end
    object Grid: TcxGrid
      Left = 0
      Top = 208
      Width = 451
      Height = 462
      Align = alBottom
      TabOrder = 2
      object tvCompany: TcxGridDBTableView
        OnDblClick = tvCompanyDblClick
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DataCompany
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.DataRowHeight = 23
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 23
        OptionsView.Indicator = True
        object tvCompanyColumn1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDisplayText = tvCompanyColumn1GetDisplayText
          HeaderAlignmentHorz = taCenter
          HeaderAlignmentVert = vaTop
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Width = 35
        end
        object tvCompanyColumn2: TcxGridDBColumn
          Caption = #20844#21496#21517#31216
          DataBinding.FieldName = 'SignName'
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Width = 211
        end
      end
      object lv: TcxGridLevel
        GridView = tvCompany
      end
    end
    object RzToolbar1: TRzToolbar
      Left = 0
      Top = 0
      Width = 451
      Height = 29
      AutoStyle = False
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdBottom]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 3
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer24
        cxLabel13
        Section
        RzSpacer25
        cxLabel14
        duties
        RzSpacer23)
      object RzSpacer23: TRzSpacer
        Left = 280
        Top = 2
      end
      object RzSpacer24: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzSpacer25: TRzSpacer
        Left = 142
        Top = 2
      end
      object cxLabel13: TcxLabel
        Left = 12
        Top = 6
        Caption = #37096#38376#65306
        Transparent = True
      end
      object cxLabel14: TcxLabel
        Left = 150
        Top = 6
        Caption = #32844#21153#65306
        Transparent = True
      end
      object duties: TcxLookupComboBox
        Left = 190
        Top = 4
        Enabled = False
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'duties'
        Properties.ListColumns = <
          item
            Caption = #32844#21153
            Width = 100
            FieldName = 'duties'
          end>
        Properties.ListSource = DataSource1
        Properties.OnCloseUp = dutiesPropertiesCloseUp
        TabOrder = 2
        Width = 90
      end
      object Section: TcxLookupComboBox
        Left = 52
        Top = 4
        RepositoryItem = DM.section
        Enabled = False
        Properties.ListColumns = <>
        Properties.OnCloseUp = SectionPropertiesCloseUp
        TabOrder = 3
        Width = 90
      end
    end
    object RzToolbar5: TRzToolbar
      Left = 0
      Top = 29
      Width = 451
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdBottom]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 4
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer26
        cxLabel15
        cxTextEdit1
        RzSpacer27
        RzToolButton5
        RzSpacer28
        RzToolButton6)
      object RzSpacer26: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzSpacer27: TRzSpacer
        Left = 219
        Top = 2
      end
      object RzToolButton5: TRzToolButton
        Left = 227
        Top = 2
        ImageIndex = 10
        Enabled = False
        OnClick = RzToolButton5Click
      end
      object RzSpacer28: TRzSpacer
        Left = 252
        Top = 2
      end
      object RzToolButton6: TRzToolButton
        Left = 260
        Top = 2
        ImageIndex = 11
        Enabled = False
        OnClick = RzToolButton6Click
      end
      object cxLabel15: TcxLabel
        Left = 12
        Top = 6
        Caption = #22995#21517#65306
        Transparent = True
      end
      object cxTextEdit1: TcxTextEdit
        Left = 52
        Top = 4
        Enabled = False
        TabOrder = 1
        OnKeyDown = cxTextEdit1KeyDown
        Width = 167
      end
    end
  end
  object ADOQuery1: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 40
    Top = 104
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 112
    Top = 104
  end
  object qry: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 192
    Top = 192
  end
  object ds: TDataSource
    DataSet = qry
    Left = 232
    Top = 192
  end
  object ADOCompany: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 56
    Top = 416
  end
  object DataCompany: TDataSource
    DataSet = ADOCompany
    Left = 56
    Top = 472
  end
end
