unit ufrmProjectPurchase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,ufrmBasePurchase, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxCalendar, cxDBLookupComboBox, cxDropDownEdit, cxMemo, cxCurrencyEdit,
  cxSpinEdit, Vcl.Menus, cxDBEdit, Vcl.StdCtrls, cxGroupBox,
  Vcl.ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMaskEdit, cxLabel,
  RzButton, RzPanel, Data.Win.ADODB, Datasnap.DBClient, ufrmBaseStorage,
  cxButtons, Datasnap.Provider, cxLookupEdit, cxDBLookupEdit;

type
  TfrmProjectPurchase = class(TfrmBasePurchase)
    RzSpacer2: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzSpacer4: TRzSpacer;
    RzToolButton2: TRzToolButton;
    procedure RzToolButton3Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure StorageAfterInsert(DataSet: TDataSet);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvViewColumn8PropertiesPopup(Sender: TObject);
    procedure StorageAfterOpen(DataSet: TDataSet);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProjectPurchase: TfrmProjectPurchase;

implementation

uses
   ufrmPurchaseDetailed,
   System.Math,
   uProjectFrame,
   global,
   ufunctions,
   uDataModule,
   ufrmIsViewGrid;


{$R *.dfm}

procedure TfrmProjectPurchase.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
//
end;

procedure TfrmProjectPurchase.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  Self.Storage.Append;
end;

procedure TfrmProjectPurchase.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  with Self.Storage do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsEdit) or (State <> dsInsert) then
        Edit;
    end;
  end;
end;

procedure TfrmProjectPurchase.RzToolButton3Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_DirBuildStorage;
    Child.ShowModal;
    SetcxGrid(Self.tvView,0, g_DirBuildStorage);
  finally
    Child.Free;
  end;
end;

procedure TfrmProjectPurchase.StorageAfterInsert(DataSet: TDataSet);
begin
  inherited;
//
end;

procedure TfrmProjectPurchase.StorageAfterOpen(DataSet: TDataSet);
begin
  inherited;
//
end;

procedure TfrmProjectPurchase.tvViewColumn8PropertiesPopup(Sender: TObject);
begin
  inherited;
//
end;

procedure TfrmProjectPurchase.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  with Self.Storage do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsEdit) AND (State <> dsInsert) then
        AAllow := False
      else
        AAllow := True;
    end;
  end;
end;

end.
