unit ufrmBasePurchases;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, RzPanel, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxTextEdit, cxCheckBox, cxCalendar, cxDBLookupComboBox,
  cxDropDownEdit, cxMemo, cxCurrencyEdit, cxSpinEdit, cxContainer, Vcl.Menus,
  Vcl.StdCtrls, cxDBEdit, cxButtons, cxLabel, cxMaskEdit, cxGroupBox,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, RzButton, Datasnap.DBClient,
  Data.Win.ADODB, Datasnap.Provider, dxmdaset;

type
  TForm3 = class(TForm)
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    RzPanel1: TRzPanel;
    RzToolbar13: TRzToolbar;
    RzSpacer30: TRzSpacer;
    RzToolButton85: TRzToolButton;
    RzSpacer113: TRzSpacer;
    RzToolButton87: TRzToolButton;
    RzToolButton88: TRzToolButton;
    RzToolButton90: TRzToolButton;
    RzSpacer120: TRzSpacer;
    RzSpacer1: TRzSpacer;
    RzSpacer10: TRzSpacer;
    btnGridSet: TRzToolButton;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewColumn24: TcxGridDBColumn;
    tvViewColumn28: TcxGridDBColumn;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    Lv: TcxGridLevel;
    Storage: TClientDataSet;
    ds: TDataSource;
    RzToolButton1: TRzToolButton;
    RzToolButton2: TRzToolButton;
    procedure RzToolButton1Click(Sender: TObject);
    procedure StorageAfterInsert(DataSet: TDataSet);
    procedure RzToolButton90Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure StorageReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure StorageBeforePost(DataSet: TDataSet);
    procedure StorageAfterPost(DataSet: TDataSet);
    procedure RzToolButton88Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    g_ProjectName : string;
    g_PostCode    : string;
    g_sqlText : string;
    IsSave : Boolean;
    IsEidt : Boolean;
    Prefix : string;
    Suffix : string;
  public
    { Public declarations }
  //  procedure WndProc(var Message: TMessage); override;  // 第一优先权
  end;

var
  Form3: TForm3;

implementation

uses
   global,ufunctions,uDataModule;

{$R *.dfm}

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TForm3.RzToolButton1Click(Sender: TObject);
begin
  with Self.Storage do
  begin
    if State <> dsInactive then
    begin
      Append;
    //  Self.tvViewColumn15.FocusWithSelection;
    //  Self.Grid.SetFocus;
    //  keybd_event(VK_RETURN,0,0,0);
    end;
  end;
end;

procedure TForm3.RzToolButton2Click(Sender: TObject);
var
  i: integer;
begin
  DM.DataSetProvider1.DataSet := DM.ADOQuery1;
  with DM.ADOQuery1 do
  begin
    Connection := DM.ADOconn;
    Close;
    SQL.Text := 'Select *from sk_Project_Storage';
    Open;
    for i:=0 to FieldCount-1 do
      Fields[i].ReadOnly:=false;//将字段的只读属性去掉
  end;
  Self.Storage.Data := DM.DataSetProvider1.Data; //cds获取数据
  Self.Storage.Open;
end;
{
begin
  g_sqlText := 'Select *from sk_Project_Storage';
  with DM.ADOQuery1 do
  begin
    Close;
    SQL.Text := g_sqlText;
    Open;
    Self.Storage.Data := DM.DataSetProvider1.Data;
    Self.Storage.Open;
  //  Self.Storage.Active := True;
  //  Self.Storage.EmptyDataSet;
  end;

end;
}
procedure TForm3.RzToolButton88Click(Sender: TObject);
begin
  Close;
end;

procedure TForm3.RzToolButton90Click(Sender: TObject);
var
  ErrCount : Integer;
  err : string;
begin
  //数据保存锁定

    if Self.tvView.DataController.IsEditing then  Self.tvView.DataController.Post();
    {
    DM.ADOQuery1.Close;
    DM.ADOQuery1.SQL.Text := 'Select *from sk_Project_Storage';
    }
      showmessage((DM.DataSetProvider1.DataSet As TADOQuery).SQL.Text);//加多一句，看看sql语句是否空的？

    //  if ClientDataSet1.ChangeCount > 0 then
    //    DataSetProvider1.ApplyUpdates(ClientDataSet1.Delta, 0, errcount);
    with Self.Storage do
    begin

      if ChangeCount > 0 then
      begin

        ErrCount := ApplyUpdates(0);
        DM.DataSetProvider1.ApplyUpdates(Delta,0,ErrCount);
        if ErrCount = 0 then
        begin
          ShowMessage('保存成功！');
        end else
        begin
          ShowMessage('保存失败:' + IntToStr(ErrCount));
        end;

      end else
      begin
        ShowMessage('没有新增与修改，无需保存！');
      end;

    end;
  {
  DM.ADOconn.BeginTrans; //开始事务
  try
    DM.ADOconn.Committrans; //提交事务
  except
    on E:Exception do
    begin
      DM.ADOconn.RollbackTrans;           // 事务回滚
      err:=E.Message;
      ShowMessage('Error:' + err);
    end;
  end;
  }
end;

procedure TForm3.StorageAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  with DataSet do
  begin
    if State <> dsInactive then
    begin

      szColName := Self.tvViewColumn28.DataBinding.FieldName;
      FieldByName(szColName).Value := True;

      szColName := Self.tvViewColumn1.DataBinding.FieldName; //编号
      szCode := Prefix + GetRowCode(g_Table_Project_BuildStorage,szColName,Suffix,20000 + Self.tvView.DataController.RecordCount);
      FieldByName(szColName).Value := szCode;

    end;

  end;

end;
procedure TForm3.StorageAfterPost(DataSet: TDataSet);
begin
  if not DM.ADOconn.InTransaction then
  begin
  //  DM.ADOconn.CommitTrans;
  end;
end;

procedure TForm3.StorageBeforePost(DataSet: TDataSet);
begin
  if not DM.ADOconn.InTransaction then
  begin
  //  DM.ADOconn.BeginTrans;
  end;
end;

procedure TForm3.StorageReconcileError(DataSet: TCustomClientDataSet;
  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
  ShowMessage(e.Message);
//  Action := HandleReconcileError(DataSet, UpdateKind, E);
end;

end.
